<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><?php echo e(trans('users.create')); ?></div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="<?php echo e(url('/users')); ?>">
                        <?php echo csrf_field(); ?>

                        <div class="form-group<?php echo e($errors->has('name') ? ' has-error' : ''); ?>">
                            <label class="col-md-4 control-label"><?php echo e(trans('users.name')); ?></label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="name" value="<?php echo e(old('name')); ?>">
                                <?php if($errors->has('name')): ?>
                                <span class="help-block">
                                    <strong><?php echo e($errors->first('name')); ?></strong>
                                </span>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="form-group<?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
                            <label class="col-md-4 control-label"><?php echo e(trans('users.email')); ?></label>
                            <div class="col-md-6">
                                <input type="email" class="form-control" name="email" value="<?php echo e(old('email')); ?>">
                                <?php if($errors->has('email')): ?>
                                <span class="help-block">
                                    <strong><?php echo e($errors->first('email')); ?></strong>
                                </span>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="form-group<?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
                            <label class="col-md-4 control-label"><?php echo e(trans('users.cwid')); ?></label>
                            <div class="col-md-6">
                                <input type="cwid" class="form-control" name="cwid" value="<?php echo e(old('cwid')); ?>">
                            </div>
                            </div>
                             <div class="form-group<?php echo e($errors->has('phone') ? ' has-error' : ''); ?>">
                            <label class="col-md-4 control-label"><?php echo e(trans('users.phone')); ?></label>
                            <div class="col-md-6">
                                <input type="phone" class="form-control" name="phone" value="<?php echo e(old('phone')); ?>">
                            </div>
                            </div>
                            <div class="form-group<?php echo e($errors->has('role_name') ? ' has-error' : ''); ?>">
                                <label class="col-md-4 control-label"><?php echo e(trans('users.role')); ?></label>
                                <div class="col-md-4">
                                    <select name="role_name" class="form-control">
                                        
                                        <?php foreach($roles as $role): ?>
                                        <option value="<?php echo e($role->name); ?>"><?php echo e($role->label); ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    
                                    
                                </div>
                            </div>
                            <div class="form-group<?php echo e($errors->has('password') ? ' has-error' : ''); ?>">
                                <label class="col-md-4 control-label"><?php echo e(trans('users.password')); ?></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="password" name="password"> <a class="btn btn-primary" id='password-generate'><i class="fa fa-key"></i>
                                
                                <?php echo e(trans('users.generate-password')); ?></a>
                                <?php if($errors->has('password')): ?>
                                <span class="help-block">
                                    <strong><?php echo e($errors->first('password')); ?></strong>
                                </span>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="form-group<?php echo e($errors->has('password_confirmation') ? ' has-error' : ''); ?>">
                            <label class="col-md-4 control-label"><?php echo e(trans('users.confirm-password')); ?></label>
                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password_confirmation">
                                <?php if($errors->has('password_confirmation')): ?>
                                <span class="help-block">
                                    <strong><?php echo e($errors->first('password_confirmation')); ?></strong>
                                </span>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                <i class="fa fa-btn fa-user"></i><?php echo e(trans('users.create')); ?>

                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
<script type="text/javascript">
    $(document).ready(function() {
        var pass_field=$("#password");
            $('#password-generate').click(function(e){ //on add input button click
            e.preventDefault();
            pass_field.val(randomPassword(8));
            });
                
            function randomPassword(length) {
                var chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
                var pass = "";
                for (var x = 0; x < length; x++) {
                var i = Math.floor(Math.random() * chars.length);
                pass += chars.charAt(i);
                }
                return pass;
            }
    } );
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>