<!DOCTYPE html>
<html>
<head>
	<title>Поръчки</title>
	<font face="arial"> 
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>

<?php if(!empty($groupedOrders['totalsPromoForSinglePharms'])): ?>
	<div>
	<p>Заявка за единчни промоции/ продукти за аптеки</p>
		<table>
		
			<tr style="background-color: #ccc;">
				<td>Дата</td>
				<td>Производител</td>
				<td>Град</td>
				<td>Име на клиент</td>
				<td>адрес</td>
				<td>булстат</td>
				<td>вид</td>
				<td>продукт ид</td>
				<td>продукт</td>
				<td>количество</td>
				<td>рабат</td>
				<td>коментар</td>
				<td>представител</td>
				<td>телефон</td>
			</tr>
		<?php foreach($groupedOrders['totalsPromoForSinglePharms'] as $total): ?>
			<?php if($total->quantity != 0 || $total->rabat!=0): ?>

				<tr>
					<td><?php echo e($total->created_at); ?></td>
					<td><?php echo e($total->manufacturer); ?></td>
					<td><?php echo e($total->city); ?></td>
					<td><?php echo e($total->client); ?></td>
					<td><?php echo e($total->address); ?></td>
					<td><?php echo e($total->bulstat); ?></td>
					<td><?php echo e(trans('orders.'.$total->type)); ?></td>
					<td><?php echo e($total->item); ?></td>
					<td><?php echo e(productName($total->item_type,$total->item)); ?></td>
					<td><?php echo e($total->quantity); ?></td>
					<td><?php echo e($total->rabat); ?></td>
					<td><?php echo e($total->comment); ?></td>
					<td><?php echo e($total->order->user->name); ?></td>
					<td><?php echo e($total->order->user->phone); ?></td>
				</tr>

			<?php endif; ?>
		<?php endforeach; ?>
			<tr style="background-color: #ffff00;">
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td><?php echo e($groupedOrders['totalsPromoForSinglePharms']->sum('quantity')); ?></td>
				<td><?php echo e($groupedOrders['totalsPromoForSinglePharms']->sum('rabat')); ?></td>
				<td></td>
				<td></td>
				<td></td>

			</tr>
		</table>
	</div>
<?php endif; ?>
<?php if(!empty($groupedOrders['totalsPromoForChains'])): ?>
	<div>
	<p>Заявка за единични пакети/ продукти за вериги ( ако е към един и същ дистрибутор)</p>
		<table>
		
			<tr style="background-color: #ccc;">
				<td>Дата</td>
				<td>Производител</td>
				<td>Град</td>
				<td>Име на клиент</td>
				<td>адрес</td>
				<td>булстат</td>
				<td>вид</td>
				<td>продукт ид</td>
				<td>продукт</td>
				<td>количество</td>
				<td>рабат</td>
				<td>коментар</td>
				<td>представител</td>
				<td>телефон</td>
			</tr>
		<?php foreach($groupedOrders['totalsPromoForChains'] as $total): ?>
			<?php if($total->quantity != 0 || $total->rabat!=0): ?>
				<tr>
					<td><?php echo e($total->created_at); ?></td>
					<td><?php echo e($total->manufacturer); ?></td>
					<td><?php echo e($total->city); ?></td>
					<td><?php echo e($total->client); ?></td>
					<td><?php echo e($total->address); ?></td>
					<td><?php echo e($total->bulstat); ?></td>
					<td><?php echo e(trans('orders.'.$total->type)); ?></td>
					<td><?php echo e($total->item); ?></td>
					<td><?php echo e(productName($total->item_type,$total->item)); ?></td>
					<td><?php echo e($total->quantity); ?></td>
					<td><?php echo e($total->rabat); ?></td>
					<td><?php echo e($total->comment); ?></td>
					<td><?php echo e($total->order->user->name); ?></td>
					<td><?php echo e($total->order->user->phone); ?></td>
				</tr>
			<?php endif; ?>
		<?php endforeach; ?>
			<tr style="background-color: #ffff00;">
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td><?php echo e($groupedOrders['totalsPromoForChains']->sum('quantity')); ?></td>
				<td><?php echo e($groupedOrders['totalsPromoForChains']->sum('rabat')); ?></td>
				<td></td>
				<td></td>
				<td></td>

			</tr>
		</table>
	</div>
<?php endif; ?>


<?php if( !empty($packagesArray)): ?>

	<div>
		<?php foreach($packagesArray as $package): ?>
			<p><?php echo e(packageName($package->id)); ?></p>
			<table>
				<tr style="background-color: #ccc;">
					<td>Продукт</td>
					<td>Количество</td>
					<td>Натурален рабат</td>
				</tr>
				<?php foreach($package->products as $product): ?>
				<tr>
					<td><?php echo e(productName('product',$product->product_id)); ?></td>
					<td><?php echo e($product->quantity); ?></td>
					<td><?php echo e($product->rabat); ?></td>
				</tr>
				<?php endforeach; ?>
				<tr style="background-color: #ffff00;">
					<td></td>
					<td><?php echo e($package->products->sum('quantity')); ?></td>
					<td><?php echo e($package->products->sum('rabat')); ?></td>
				</tr>
			</table>
		<?php endforeach; ?>
	</div>
<?php endif; ?>
<?php if(!empty($groupedOrders['totalsPackageForSinglePharms'])): ?>
	<div>
	<p>Заявка за ПАКЕТ / единична аптека</p>
		
		<table>
			<tr style="background-color: #ccc;">
				<td>Дата</td>
				<td>Производител</td>
				<td>Град</td>
				<td>Име на клиент</td>
				<td>адрес</td>
				<td>булстат</td>
				<td>вид</td>
				<td>продукт ид</td>
				<td>продукт</td>
				<td>количество</td>
				<td>рабат</td>
				<td>коментар</td>
				<td>представител</td>
				<td>телефон</td>
			</tr>
		<?php foreach($groupedOrders['totalsPackageForSinglePharms'] as $total): ?>
			<?php if($total->quantity != 0 || $total->rabat!=0): ?>
				<tr>
					<td><?php echo e($total->created_at); ?></td>
					<td><?php echo e($total->manufacturer); ?></td>
					<td><?php echo e($total->city); ?></td>
					<td><?php echo e($total->client); ?></td>
					<td><?php echo e($total->address); ?></td>
					<td><?php echo e($total->bulstat); ?></td>
					<td><?php echo e(trans('orders.'.$total->type)); ?></td>
					<td><?php echo e($total->item); ?></td>
					<td><?php echo e(productName($total->item_type,$total->item)); ?></td>
					<td><?php echo e($total->quantity); ?></td>
					<td><?php echo e($total->rabat); ?></td>
					<td><?php echo e($total->comment); ?></td>
					<td><?php echo e($total->order->user->name); ?></td>
					<td><?php echo e($total->order->user->phone); ?></td>
				</tr>
			<?php endif; ?>
		<?php endforeach; ?>
			<tr style="background-color: #ffff00;">
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td><?php echo e($groupedOrders['totalsPackageForSinglePharms']->sum('quantity')); ?></td>
				<td><?php echo e($groupedOrders['totalsPackageForSinglePharms']->sum('rabat')); ?></td>
				<td></td>
				<td></td>
				<td></td>

			</tr>
		</table>
	</div>
<?php endif; ?>
<?php if(!empty($groupedOrders['totalsPackageForChains'])): ?>
	<div>
	<p>Заявка за ПАКЕТ към вериги</p>
		<table>
		
			<tr style="background-color: #ccc;">
				<td>Дата</td>
				<td>Производител</td>
				<td>Град</td>
				<td>Име на клиент</td>
				<td>адрес</td>
				<td>булстат</td>
				<td>вид</td>
				<td>продукт ид</td>
				<td>продукт</td>
				<td>количество</td>
				<td>рабат</td>
				<td>коментар</td>
				<td>представител</td>
				<td>телефон</td>
			</tr>
		<?php foreach($groupedOrders['totalsPackageForChains'] as $total): ?>
			<?php if($total->quantity != 0 || $total->rabat!=0): ?>
				<tr>
					<td><?php echo e($total->created_at); ?></td>
					<td><?php echo e($total->manufacturer); ?></td>
					<td><?php echo e($total->city); ?></td>
					<td><?php echo e($total->client); ?></td>
					<td><?php echo e($total->address); ?></td>
					<td><?php echo e($total->bulstat); ?></td>
					<td><?php echo e(trans('orders.'.$total->type)); ?></td>
					<td><?php echo e($total->item); ?></td>
					<td><?php echo e(productName($total->item_type,$total->item)); ?></td>
					<td><?php echo e($total->quantity); ?></td>
					<td><?php echo e($total->rabat); ?></td>
					<td><?php echo e($total->comment); ?></td>
					<td><?php echo e($total->order->user->name); ?></td>
					<td><?php echo e($total->order->user->phone); ?></td>
				</tr>
			<?php endif; ?>
		<?php endforeach; ?>
			<tr style="background-color: #ffff00;">
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td><?php echo e($groupedOrders['totalsPackageForChains']->sum('quantity')); ?></td>
				<td><?php echo e($groupedOrders['totalsPackageForChains']->sum('rabat')); ?></td>
				<td></td>
				<td></td>
				<td></td>

			</tr>
		</table>
	</div>
<?php endif; ?>
</body>
</html>