<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-8">
                            <i class="fa fa-bars"></i>
                            <?php echo e(trans('promotions.promotions')); ?>

                        </div>
                        <div class="col-md-4">
                            <div class="btn-group pull-right">
                                <?php if (app('Illuminate\Contracts\Auth\Access\Gate')->check('create_promotions')): ?>
                                <a href="<?php echo e(url('promotions').'/'.$promo['id'].'/edit'); ?>" type="button"  id='info' class="btn btn-info" aria-label="Left Align" data-product-name="1">
                                    <i class="fa fa-cog" aria-hidden="true"></i>
                                    <?php echo e(trans('promotions.edit')); ?>

                                </a>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="panel-body">
                    
                    <?php if (app('Illuminate\Contracts\Auth\Access\Gate')->check('create_promotions')): ?>
                    <a class="btn btn-primary" href="<?php echo e(url('/promotions/create')); ?>"><i class="fa fa-plus-circle"></i>
                    <?php echo e(trans('promotions.create-new-promo')); ?></a>
                    <hr>
                    <?php endif; ?>
                    <div class="row">
                        <div class="col-md-6">
                            <dl class="dl-horizontal">
                                <dt><?php echo e(trans('promotions.name')); ?></dt>
                                <dd><?php echo e($promo['name']); ?></dd>
                                <dt><?php echo e(trans('promotions.date-start')); ?></dt>
                                <dd><?php echo e($promo['start_at']); ?></dd>
                                <dt><?php echo e(trans('promotions.date-end')); ?></dt>
                                <dd><?php echo e($promo['end_at']); ?></dd>
                                <dt><?php echo e(trans('promotions.type')); ?></dt>
                                <dd><?php echo e(isPromoPrivate($promo['is_private'])); ?></dd>
                            </dl>
                            <?php if($promo['is_private']): ?>
                            <dl class="dl-horizontal">
                                <dt><?php echo e(trans('promotions.private-description')); ?></dt>
                                
                                <?php foreach($promo['users'] as $user): ?>
                                <dd><?php echo e($user['name']); ?></dd>
                                <?php endforeach; ?>
                                
                            </dl>
                            <?php endif; ?>
                        </div>
                        <div class="col-md-6">
                            <div class="list-group">
                                <a href="#" class="list-group-item active">
                                    <h4 class="list-group-item-heading"><?php echo e(trans('promotions.products')); ?></h4>
                                </a>
                                <table id="promotions-detail-table" class="display" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th><?php echo e(trans('promotions.product-name')); ?></th>
                                            <th><?php echo e(trans('promotions.quantity')); ?></th>
                                            <th><?php echo e(trans('promotions.rabat')); ?></th>
                                        </tr>
                                    </thead>
                                    
                                    <tbody>
                                        <?php foreach($promo['products'] as $product): ?>
                                        <tr>
                                            <td><?php echo e($product['name']); ?></td>
                                            <td><?php echo e($product['product_quantity']); ?></td>
                                            <td><span class="label label-success"><?php echo e($product['rabat']); ?></span></td>
                                        </tr>
                                        <?php endforeach; ?>
                                        
                                    </tbody>
                                </table>
                                
                            </div>
                            
                            
                            
                            
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
<script type="text/javascript">
$(document).ready(function() {
$('#promotions-detail-table').dataTable({
"language": {
"lengthMenu": "_MENU_ "+"<?php echo e(trans('general.table-display-tail')); ?>",
"zeroRecords": "<?php echo e(trans('general.table-zero-records')); ?>",
"info": "<?php echo e(trans('general.table-page')); ?>"+" _PAGE_ "+"<?php echo e(trans('general.table-from')); ?>"+" _PAGES_",
"infoEmpty": "<?php echo e(trans('general.table-zero-records')); ?>",
"infoFiltered": "(<?php echo e(trans('general.table-filtered-from')); ?> _MAX_ <?php echo e(trans('general.table-filtered-records')); ?>)",
"search": "<?php echo e(trans('general.search-label')); ?>:",
"paginate": {
"first":      "<?php echo e(trans('general.table-first')); ?>",
"last":       "<?php echo e(trans('general.table-last')); ?>",
"next":       "<?php echo e(trans('general.table-next')); ?>",
"previous":   "<?php echo e(trans('general.table-previous')); ?>"
},
},
"paging":   false,
"ordering": false,
"searching": false,
"info":     false
});
} );
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>