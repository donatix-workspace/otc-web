<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><?php echo e(trans('resellers.reseller')); ?></div>
                <div class="panel-body">
                    <form enctype="multipart/form-data" id="re_update_form" class="form-horizontal" role="form" method="POST" files='true' action="<?php echo e(url('/resellers/'.$reseller->id)); ?>">
                        <input name="_method" id="re-edit-method" type="hidden" value="PATCH"></input>
                        <input name="is_deleted" id="re-edit-is-deleted" type="hidden" value="0"></input>
                        <?php echo csrf_field(); ?>

                        <div class="row">
                            <div class="col-md-9">
                                
                                <div class="media">
                                    <div class="media-left">
                                        <a href="#">
                                            <img width='64' class="media-object" src="/uploaded/images/<?php echo e($reseller->image); ?>" >
                                        </a>
                                    </div>
                                </div>
                                <div class="form-group<?php echo e($errors->has('name') ? ' has-error' : ''); ?>">
                                    <label class="col-md-4 control-label"><?php echo e(trans('resellers.name')); ?></label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="name" value="<?php echo e($reseller->name); ?>">
                                        <?php if($errors->has('name')): ?>
                                        <span class="help-block">
                                            <strong><?php echo e($errors->first('name')); ?></strong>
                                        </span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="form-group<?php echo e($errors->has('group_id') ? ' has-error' : ''); ?>">
                                    <label class="col-md-4 control-label"><?php echo e(trans('resellers.group')); ?></label>
                                    <div class="col-md-6">

                                        <select name="group_id" class="form-control" id="sel1">
                                            
                                            <?php foreach($groups as $group): ?>

                                            <option <?php echo e($reseller->group['id']== $group->id ? 'selected' : ''); ?> value="<?php echo e($group->id); ?>"><?php echo e($group->name); ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        
                                    </div>
                                </div>
                                <div class="form-group<?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
                                    <label class="col-md-4 control-label"><?php echo e(trans('resellers.email')); ?></label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="email" value="<?php echo e($reseller->email); ?>">
                                        <?php if($errors->has('email')): ?>
                                        <span class="help-block">
                                            <strong><?php echo e($errors->first('email')); ?></strong>
                                        </span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="form-group<?php echo e($errors->has('description') ? ' has-error' : ''); ?>">
                                    <label class="col-md-4 control-label"><?php echo e(trans('resellers.description')); ?></label>
                                    <div class="col-md-6">
                                        <textarea type="text" class="form-control" name="description" ><?php echo e($reseller->description); ?></textarea>
                                        
                                        <?php if($errors->has('description')): ?>
                                        <span class="help-block">
                                            <strong><?php echo e($errors->first('description')); ?></strong>
                                        </span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="form-group<?php echo e($errors->has('image') ? ' has-error' : ''); ?>">
                                    <label class="col-md-4 control-label"><?php echo e(trans('resellers.image')); ?></label>
                                    <div class="col-md-6">
                                        
                                        <input type="file" name="image" value="<?php echo e(old('image')); ?>">
                                        <?php if($errors->has('photo')): ?>
                                        <span class="help-block">
                                            <strong><?php echo e($errors->first('photo')); ?></strong>
                                        </span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-3">
                                <h3><?php echo e(trans('resellers.users')); ?></h3>
                                <div class="checkbox">
                                    <?php foreach($users as $user): ?>
                                    <label><input type="checkbox" name='users[]' value="<?php echo e($user->id); ?>" <?php if(in_array($user->id, $assignedUsers->toArray())): ?> checked <?php endif; ?>><?php echo e($user->name); ?> <?php if(in_array($user->id, $assignedUsers->toArray())): ?>
                                    <i class="fa fa-check" aria-hidden="true"></i>

<?php endif; ?></label> <br>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <a href=javascript:; id='re-del-btn' class="btn btn-danger">
                                                    <i class="fa fa-trash"></i>  <?php echo e(trans('general.delete-btn')); ?>

                                                </a>
                                            </div>
                                            <div class="col-md-2">
                                                <button type="submit" class="btn btn-primary">
                                                <i class="fa fa-pencil"></i>  <?php echo e(trans('general.edit-btn')); ?>

                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
<script>
$( "#re-del-btn" ).click(function() {
$("#re-edit-method").val("DELETE");
$('#re_update_form').submit();
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>