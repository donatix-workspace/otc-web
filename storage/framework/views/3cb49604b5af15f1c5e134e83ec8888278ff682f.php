<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-plus-circle"></i>
                <?php echo e(trans('promotions.create-new-promo')); ?></div>
                <div class="panel-body">
                    <form  class="form-horizontal" role="form" method="POST" files='true' action="<?php echo e(url('/promotions')); ?>">
                        <?php echo csrf_field(); ?>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group<?php echo e($errors->has('name') ? ' has-error' : ''); ?>">
                                    <label class="col-md-4 control-label"><?php echo e(trans('promotions.name')); ?></label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="name" value="<?php echo e(old('name')); ?>">
                                        <?php if($errors->has('name')): ?>
                                        <span class="help-block">
                                            <strong><?php echo e($errors->first('name')); ?></strong>
                                        </span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="form-group<?php echo e($errors->has('start_at') ? ' has-error' : ''); ?>">
                                    <label class="col-md-4 control-label"><?php echo e(trans('promotions.promo-start-date')); ?></label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control datepicker" name="start_at" value="<?php echo e(date("Y-m-d")); ?>">
                                        <?php if($errors->has('start_at')): ?>
                                        <span class="help-block">
                                            <strong><?php echo e($errors->first('start_at')); ?></strong>
                                        </span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="form-group<?php echo e($errors->has('start_at') ? ' has-error' : ''); ?>">
                                    <label class="col-md-4 control-label"><?php echo e(trans('promotions.promo-end-date')); ?></label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control datepicker" name="end_at" value="<?php echo e(date("Y-m-d")); ?>">
                                        <?php if($errors->has('end_at')): ?>
                                        <span class="help-block">
                                            <strong><?php echo e($errors->first('end_at')); ?></strong>
                                        </span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="form-group<?php echo e($errors->has('start_at') ? ' has-error' : ''); ?>">
                                    <label class="col-md-4 control-label"><?php echo e(trans('promotions.private')); ?></label>
                                    <div class="col-md-4">
                                        <input type="checkbox" id="is_private" name='is_private' value="private" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-1 col-md-offset-2">
                                            <span>Продукт</span>
                                        </div>
                                        <div class="col-md-2 col-md-offset-1">
                                            <span>Количество</span>
                                        </div>
                                        <div class="col-md-2 col-md-offset-1">
                                            <span>Рабат</span>
                                        </div>
                                        <hr>
                                    </div>
                                    <div class="input_fields_wrap">
                                    </div>
                                </div>
                                
                                <div class="col-md-8 users">
                                <h3><?php echo e(trans('resellers.users')); ?></h3>
                                    <div class="checkbox has-success" >
                                    <?php foreach($users as $user): ?>
                                        <label><input type="checkbox" name='users[]' value="<?php echo e($user->id); ?>" ><?php echo e($user->name); ?></label> <br>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-btn fa-user-plus"></i><?php echo e(trans('general.create-btn')); ?>

                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h4><?php echo e(trans('general.products-label')); ?></h4>
                                <table id="products-table" class="display" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th><?php echo e(trans('promotions.table-id')); ?></th>
                                            <th><?php echo e(trans('promotions.name')); ?></th>
                                            <th><?php echo e(trans('general.add-label')); ?> </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($products as $product): ?>
                                            <tr>
                                                
                                                <td><?php echo e($product['id']); ?></td>
                                                <td><?php echo e($product['name']); ?></td>
                                                <td><a href="javascript:;" class="add_field_button" data-product-id="<?php echo e($product['id']); ?>" data-product-name="<?php echo e($product['name']); ?>"><i class="fa fa-btn fa-plus"></i></a> </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        
                    </form>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
<script type="text/javascript">
$(document).ready(function() {

var is_private = $("#is_private");
var users = $(".users");

$(is_private).on('click', function () {
    if ($(is_private).is(":checked"))
    {
      $(users).css("display","block");
    }
    else{
      $(users).css("display","none");
    }
} );


 $(".datepicker").datepicker({dateFormat: "yy-mm-dd"});


            //todo if promo 1
var max_fields      = 10; //maximum input boxes allowed
var wrapper         = $(".input_fields_wrap"); //Fields wrapper
var add_button      = $(".add_field_button"); //Add button ID

var x = 1; //initlal text box count
$('#products-table').dataTable({

    "bAutoWidth": false,
    "bDeferRender": true,
    "fnDrawCallback": function() {
        

        
    },

"language": { 
    "lengthMenu": "_MENU_ "+"<?php echo e(trans('general.table-display-tail')); ?>",
    "zeroRecords": "<?php echo e(trans('general.table-zero-records')); ?>",
    "info": "<?php echo e(trans('general.table-page')); ?>"+" _PAGE_ "+"<?php echo e(trans('general.table-from')); ?>"+" _PAGES_",
    "infoEmpty": "<?php echo e(trans('general.table-zero-records')); ?>",
    "infoFiltered": "(<?php echo e(trans('general.table-filtered-from')); ?> _MAX_ <?php echo e(trans('general.table-filtered-records')); ?>)",
    "search": "<?php echo e(trans('general.search-label')); ?>:",
    "paginate": {
        "first":      "<?php echo e(trans('general.table-first')); ?>",
        "last":       "<?php echo e(trans('general.table-last')); ?>",
        "next":       "<?php echo e(trans('general.table-next')); ?>",
        "previous":   "<?php echo e(trans('general.table-previous')); ?>"
    },
}
});
$(add_button).on('click', function (e) {
            e.preventDefault();
            var element=$(this);
            var id=element.data('product-id');
            var name=element.data('product-name');  
            var label = '<div class="row"><div class="col-md-4 col-md-offset-1"><span>'+name+'</span></div><div class="col-md-2 col-md-offset-1"><input type="number" class="form-control" name="products['+id+'][quantity]"></div><div class="col-md-2 col-md-offset-1"><input type="number" class="form-control" name="products['+id+'][rabat]"></div></div>';
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append(label); //add input box
            }
        });
} );

</script>
<?php $__env->stopSection(); ?>
>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>