<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-bars"></i>
                    </i>
                <?php echo e(trans('orders.orders')); ?></div>
                <div class="panel-body">
                    
                    <?php if(!Auth::guest()): ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="orders-for-comfirm">
                                <h3><?php echo e(trans('orders.orders-comfirm-table')); ?></h3>
                                <table id="orders-comfirm-table" class="display" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th><?php echo e(trans('orders.number')); ?></th>
                                            <th><?php echo e(trans('orders.object-name')); ?></th>
                                            <th><?php echo e(trans('orders.from')); ?></th>
                                            <th><?php echo e(trans('orders.submit-date')); ?></th>
                                            <th><?php echo e(trans('orders.send-date')); ?></th>
                                            <th><?php echo e(trans('general.actions')); ?></th>
                                            <th><?php echo e(trans('general.state')); ?></th>
                                            
                                        </tr>
                                    </thead>
                                    
                                    <tbody>
                                    <?php if(!empty($ordersForConfirm)): ?>
                                        <?php foreach($ordersForConfirm as $order): ?>
                                        <tr>
                                            <td><?php echo e($order->id); ?></td>
                                            <td><?php echo e($order->object_name); ?></td>
                                            <td><?php echo e($order->user_name); ?></td>
                                            <td><?php echo e($order->created_at); ?></td>
                                            <td><?php echo e(formatDate($order->order_date?$order->order_date:$order->created_at)); ?></td>
                                            <td class="text-center">
                                                <a href="<?php echo e(url('orders').'/'.$order->id); ?>" type="button"  id='info' class="btn btn-info " aria-label="Left Align" data-product-name="1">
                                                    <i class="fa fa-info"></i>
                                                </a>
                                                

                                            </td>
                                            <td>
                                                <i class="fa fa-refresh" aria-hidden="true"></i>
                                            </td>
                                        </tr>
                                        <?php endforeach; ?>
                                        <?php endif; ?>
                                    </tbody>
                                </table>
                                <hr>
                            </div>
                            <div class="orders-history">
                                <h3><?php echo e(trans('orders.orders-history-table')); ?></h3>
                                <table id="orders-history-table" class="display" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th><?php echo e(trans('orders.number')); ?></th>
                                            <th><?php echo e(trans('orders.object-name')); ?></th>
                                            <th><?php echo e(trans('orders.from')); ?></th>
                                            <th><?php echo e(trans('orders.submit-date')); ?></th>
                                            <th><?php echo e(trans('orders.send-date')); ?></th>
                                            <th><?php echo e(trans('general.actions')); ?></th>
                                            <th><?php echo e(trans('general.state')); ?></th>
                                        </tr>
                                    </thead>
                                    
                                    <tbody>
                                    <?php if(!empty($orders)): ?>
                                        <?php foreach($orders as $order): ?>
                                        <tr>
                                            <td><?php echo e($order->id); ?></td>
                                            <td><?php echo e($order->object_name); ?></td>
                                            <td><?php echo e($order->user_name); ?></td>
                                            <td><?php echo e($order->created_at); ?></td>
                                            <td><?php echo e(formatDate($order->order_date?$order->order_date:$order->created_at)); ?></td>
                                            <td class="text-center"><a href="<?php echo e(url('orders').'/'.$order->id); ?>" type="button"  id='info' class="btn btn-info " aria-label="Left Align" data-product-name="1">
                                                <i class="fa fa-info"></i>
                                            </a>
                                            </td>
                                            <td>
                                               <i class="<?php echo e(getOrderState($order)); ?>"></i>
                                            </td>
                                        </tr>
                                        <?php endforeach; ?>
                                      <?php endif; ?>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <?php else: ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-info">
                                <?php echo e(trans('messages.page-no-login')); ?>

                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
<script type="text/javascript">
$(document).ready(function() {
//$('#promotions-table').DataTable();
$('#orders-comfirm-table').dataTable({
"language": {
"lengthMenu": "_MENU_ "+"<?php echo e(trans('general.table-display-tail')); ?>",
"zeroRecords": "<?php echo e(trans('general.table-zero-records')); ?>",
"info": "<?php echo e(trans('general.table-page')); ?>"+" _PAGE_ "+"<?php echo e(trans('general.table-from')); ?>"+" _PAGES_",
"infoEmpty": "<?php echo e(trans('general.table-zero-records')); ?>",
"infoFiltered": "(<?php echo e(trans('general.table-filtered-from')); ?> _MAX_ <?php echo e(trans('general.table-filtered-records')); ?>)",
"search": "<?php echo e(trans('general.search-label')); ?>:",
"paginate": {
"first":      "<?php echo e(trans('general.table-first')); ?>",
"last":       "<?php echo e(trans('general.table-last')); ?>",
"next":       "<?php echo e(trans('general.table-next')); ?>",
"previous":   "<?php echo e(trans('general.table-previous')); ?>"
},
}
});
$('#orders-history-table').dataTable({
"order": [[ 1, "asc" ]],
"language": {
"lengthMenu": "_MENU_ "+"<?php echo e(trans('general.table-display-tail')); ?>",
"zeroRecords": "<?php echo e(trans('general.table-zero-records')); ?>",
"info": "<?php echo e(trans('general.table-page')); ?>"+" _PAGE_ "+"<?php echo e(trans('general.table-from')); ?>"+" _PAGES_",
"infoEmpty": "<?php echo e(trans('general.table-zero-records')); ?>",
"infoFiltered": "(<?php echo e(trans('general.table-filtered-from')); ?> _MAX_ <?php echo e(trans('general.table-filtered-records')); ?>)",
"search": "<?php echo e(trans('general.search-label')); ?>:",
"paginate": {
"first":      "<?php echo e(trans('general.table-first')); ?>",
"last":       "<?php echo e(trans('general.table-last')); ?>",
"next":       "<?php echo e(trans('general.table-next')); ?>",
"previous":   "<?php echo e(trans('general.table-previous')); ?>"
},
}
});
} );
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>