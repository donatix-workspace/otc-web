<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-bars"></i>
</i>
 <?php echo e(trans('resellers.cpanel')); ?></div>

                <div class="panel-body">
                               
                <?php if (app('Illuminate\Contracts\Auth\Access\Gate')->check('create_resellers')): ?>
                    <a class="btn btn-primary" href="<?php echo e(url('/resellers/create')); ?>"><i class="fa fa-plus-circle"></i>
  <?php echo e(trans('resellers.create-new-reseller')); ?></a>
                    
                    <a class="btn btn-primary" href="<?php echo e(url('/resellers/groups/create')); ?>"><i class="fa fa-plus-circle"></i>
  <?php echo e(trans('resellers.create-new-reseller-group')); ?></a>
                    <hr>
                <?php endif; ?>
                <div class="row">
                    <?php foreach($resellers as $reseller): ?>
                        <div class="col-md-4">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <?php echo e($reseller->name); ?> - <?php echo e($reseller->email); ?> 
                                    <?php if (app('Illuminate\Contracts\Auth\Access\Gate')->check('edit_resellers')): ?>

                                    <a class="pull-right" href='resellers/<?php echo e($reseller->id); ?>/edit'>
                                        <i class="fa fa-pencil" aria-hidden="true"></i>

                                    </a>
                                     
                                    <?php endif; ?>
                                </div>
                                <div class="re-images">
                                    <img src="/uploaded/images/<?php echo e($reseller->image); ?>">
                                 </div>
                            </div>
                        
                            
                        </div>
                       

                     <?php endforeach; ?>
                 </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>