<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-8">
                            <i class="fa fa-bars"></i>
                            <?php echo e(trans('promotions.promotions')); ?>

                        </div>
                        <div class="col-md-4">
                            
                        </div>
                    </div>
                    
                </div>
                <div class="panel-body">
                    
                    
                    <div class="row">
                        <div class="col-md-6">
                            <form enctype="multipart/form-data" id="promo_update_form" class="form-horizontal" role="form" method="POST" action="<?php echo e(url('/promotions/'.$promo['id'])); ?>">
                                <input name="_method" id="promo-edit-method" type="hidden" value="PATCH"></input>
                                <?php echo csrf_field(); ?>

                                <dl class="dl-horizontal">
                                    <dt><?php echo e(trans('promotions.name')); ?></dt>
                                    <dd><input type="text" class="form-control" name="name" value="<?php echo e($promo['name']); ?>"></dd>
                                    <dt><?php echo e(trans('promotions.date-start')); ?></dt>
                                    <dd><?php echo e(formatDate($promo['start_at'])); ?></dd>
                                    <dt><?php echo e(trans('promotions.date-end')); ?></dt>
                                    <dd><input type="text" class="form-control datepicker" name="end_at" value="<?php echo e(formatDate($promo['end_at'])); ?>"></dd>
                                </dl>
                           
                            <button type="submit" class="btn btn-primary">
                            <i class="fa fa-pencil"></i>  <?php echo e(trans('general.edit-btn')); ?>

                            </button>
                            </form>
                        </div>
                        <div class="col-md-6">
                            <div class="list-group">
                                <a href="#" class="list-group-item active">
                                    <h4 class="list-group-item-heading"><?php echo e(trans('promotions.products')); ?></h4>
                                </a>
                                <table id="promotions-detail-table" class="display" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th><?php echo e(trans('promotions.product-name')); ?></th>
                                            <th><?php echo e(trans('promotions.quantity')); ?></th>
                                            <th><?php echo e(trans('promotions.rabat')); ?></th>
                                        </tr>
                                    </thead>
                                    
                                    <tbody>
                                        <?php foreach($promo['products'] as $product): ?>
                                        <tr>
                                            <td><?php echo e($product['name']); ?></td>
                                            <td><?php echo e($product['product_quantity']); ?></td>
                                            <td><span class="label label-success"><?php echo e($product['rabat']); ?></span></td>
                                        </tr>
                                        <?php endforeach; ?>
                                        
                                    </tbody>
                                </table>
                                
                            </div>
                            
                            
                            
                            
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
<script type="text/javascript">
$(document).ready(function() {
$(".datepicker").datepicker({dateFormat: "yy-mm-dd"});
$('#promotions-detail-table').dataTable({
"language": {
"lengthMenu": "_MENU_ "+"<?php echo e(trans('general.table-display-tail')); ?>",
"zeroRecords": "<?php echo e(trans('general.table-zero-records')); ?>",
"info": "<?php echo e(trans('general.table-page')); ?>"+" _PAGE_ "+"<?php echo e(trans('general.table-from')); ?>"+" _PAGES_",
"infoEmpty": "<?php echo e(trans('general.table-zero-records')); ?>",
"infoFiltered": "(<?php echo e(trans('general.table-filtered-from')); ?> _MAX_ <?php echo e(trans('general.table-filtered-records')); ?>)",
"search": "<?php echo e(trans('general.search-label')); ?>:",
"paginate": {
"first":      "<?php echo e(trans('general.table-first')); ?>",
"last":       "<?php echo e(trans('general.table-last')); ?>",
"next":       "<?php echo e(trans('general.table-next')); ?>",
"previous":   "<?php echo e(trans('general.table-previous')); ?>"
},
},
"paging":   false,
"ordering": false,
"searching": false,
"info":     false
});
} );
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>