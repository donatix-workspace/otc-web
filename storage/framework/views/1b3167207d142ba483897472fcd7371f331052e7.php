<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-8">
                            <i class="fa fa-bars"></i>
                            <?php echo e(trans('orders.order')); ?> # <?php echo e($order->id); ?> | <?php echo e(trans('orders.from')); ?> <?php echo e($order->created_at); ?> |  <?php echo e(trans('orders.send-date')); ?> <?php echo e(formatDate($order->send_date?$order->send_date:$order->created_at)); ?>

                        </div>
                        <div class="col-md-4">
                            <div class="btn-group pull-right">
                                 <form class="form-inline" action="<?php echo e(url('orders', $order->id)); ?>" method="POST">
                                <?php if(!$order->need_confirmation && !$order->confirmed): ?>
                                    <i>
                                <i class="fa fa-thumbs-o-down" aria-hidden="true"></i>
                                <?php echo e(trans('orders.declined')); ?>

                                    </i>
                                <?php elseif($order->confirmed): ?>
                                <i><i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                                <?php echo e(trans('orders.confirmed')); ?></i>
                                <?php else: ?>
                                   
                                    <a href="<?php echo e(url('orders').'/'.$order->id.'/confirmation'); ?>" class="btn btn-success btn-sm"><i class="fa fa-check">  <?php echo e(trans('orders.accept')); ?></i></a>
                                    <a href="<?php echo e(url('orders').'/'.$order->id.'/decline'); ?>" class="btn btn-warning btn-sm"><i class="fa fa-times">  <?php echo e(trans('orders.decline')); ?></i></a>
                                   
                                <?php endif; ?>
                                <input type="hidden" name="_method" value="DELETE">
                                <?php echo e(csrf_field()); ?>

                                <?php if (app('Illuminate\Contracts\Auth\Access\Gate')->check('approve_orders')): ?>    
                                    <button class="btn btn-danger btn-sm"><i class="fa fa-trash"> <?php echo e(trans('orders.delete')); ?></i></button> 
                                <?php endif; ?>    
                                </form>
                            </div>
                        </div>
                    </div>
                    
                    
                </div>
                <div class="panel-body">
                    
                    
                    <div class="row">
                        <div class="col-md-12">
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <h3><?php echo e(trans('orders.sales-representative')); ?></h3>
                                    <div class="row">
                                        
                                        <div class="col-md-12">
                                            <dl class="dl-horizontal">
                                                <dt><?php echo e(trans('orders.sales-representative-name')); ?></dt>
                                                <dd><?php echo e($user->name); ?></dd>
                                                <dt><?php echo e(trans('orders.sales-representative-email')); ?></dt>
                                                <dd><?php echo e($user->email); ?></dd>
                                                <dt><?php echo e(trans('orders.sales-representative-cwid')); ?></dt>
                                                <dd><?php echo e($user->cwid); ?></dd>
                                            </dl>
                                            
                                            
                                        </div>
                                    </div>
                                    <h3><?php echo e(trans('orders.signiture')); ?></h3>
                                    <div class="row">
                                        
                                        <div class="col-md-12">
                                            
                                            <img alt="<?php echo e(trans('orders.signiture')); ?>" src="data:image/jpeg;base64,<?php echo e($order->signiture); ?>" />
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <h3><?php echo e(trans('orders.sales-resellers')); ?></h3>
                                    <div class="row">
                                        
                                        <div class="col-md-12">
                                            <dl class="dl-horizontal">
                                                <?php foreach($order->resellers as $reseller): ?>
                                                <dt><?php echo e(trans('orders.sales-resselers-name')); ?></dt>
                                                <dd><?php echo e(resellerName($reseller->reseller_id)); ?> - <?php echo e(resellerEmail($reseller->reseller_id)); ?></dd>
                                                <dt><?php echo e(trans('orders.sales-resselers-comment')); ?></dt>
                                                <dd><p><i><?php echo e($reseller->comment); ?></i> </p></dd>
                                                
                                                <?php endforeach; ?>
                                                
                                            </dl>
                                            
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <hr>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <h3><?php echo e(trans('orders.details')); ?></h3>
                                    <table id="orders-table" class="display" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th><?php echo e(trans('orders.pharmacy')); ?></th>
                                                <th><?php echo e(trans('orders.chain')); ?></th>
                                                <th><?php echo e(trans('orders.package_id')); ?></th>
                                                <th><?php echo e(trans('orders.product-type')); ?></th>
                                                <th><?php echo e(trans('orders.product-name')); ?></th>
                                                <th><?php echo e(trans('orders.product-quantity')); ?></th>
                                                <th><?php echo e(trans('orders.product-rabat')); ?></th>
                                                <th><?php echo e(trans('orders.product-reseller')); ?></th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            <?php foreach($order->details as $detail): ?>
                                            <?php foreach($detail->items as $item): ?>
                                            <tr>
                                                <td><?php echo e(pharmName($detail->pharm_id)); ?></td>
                                                <td><?php echo e($detail->chain['name']); ?></td>
                                                <td><?php echo e(packageName($item->package_id)); ?></td>
                                                <td><?php echo e(trans('orders.'.$item->item_type)); ?></td>
                                                <td><?php echo e(productName($item->item_type,$item->item_id)); ?></td>
                                                <td><?php echo e($item->quantity); ?></td>
                                                <td><?php echo e($item->rabat); ?></td>
                                                <td><?php echo e(resellerName($item->reseller_id)); ?></td>
                                            </tr>
                                            <?php endforeach; ?>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                    <br>
                                </div>
                            </div>
                            <hr>
                        </div>
                    </div>
                    
                </div>
                
                <br>
            </div>
        </div>
    </div>
</div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
<script type="text/javascript">
$(document).ready(function() {
//$('#promotions-table').DataTable();
$('#orders-table').dataTable({
"language": { 
    "lengthMenu": "_MENU_ "+"<?php echo e(trans('general.table-display-tail')); ?>",
    "zeroRecords": "<?php echo e(trans('general.table-zero-records')); ?>",
    "info": "<?php echo e(trans('general.table-page')); ?>"+" _PAGE_ "+"<?php echo e(trans('general.table-from')); ?>"+" _PAGES_",
    "infoEmpty": "<?php echo e(trans('general.table-zero-records')); ?>",
    "infoFiltered": "(<?php echo e(trans('general.table-filtered-from')); ?> _MAX_ <?php echo e(trans('general.table-filtered-records')); ?>)",
    "search": "<?php echo e(trans('general.search-label')); ?>:",
    "paginate": {
        "first":      "<?php echo e(trans('general.table-first')); ?>",
        "last":       "<?php echo e(trans('general.table-last')); ?>",
        "next":       "<?php echo e(trans('general.table-next')); ?>",
        "previous":   "<?php echo e(trans('general.table-previous')); ?>"
    },
}
});
} );
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>