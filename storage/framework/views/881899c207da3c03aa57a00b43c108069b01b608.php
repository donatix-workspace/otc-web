<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-bars"></i>
                    </i>
                <?php echo e(trans('promotions.promotions')); ?></div>
                <div class="panel-body">
                    
                    <?php if (app('Illuminate\Contracts\Auth\Access\Gate')->check('create_promotions')): ?>
                    <a class="btn btn-primary" href="<?php echo e(url('/promotions/create')); ?>"><i class="fa fa-plus-circle"></i>
                    <?php echo e(trans('promotions.create-new-promo')); ?></a>
                    <hr>
                    <?php endif; ?>
                    <div class="row">
                        <div class="col-md-12">
                            <table id="promotions-table" class="display" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th><?php echo e(trans('promotions.table-id')); ?></th>
                                        <th><?php echo e(trans('promotions.name')); ?></th>
                                        <th><?php echo e(trans('promotions.promo-start-date')); ?></th>
                                        <th><?php echo e(trans('promotions.promo-end-date')); ?></th>
                                        <th><?php echo e(trans('promotions.actions')); ?></th>
                                    </tr>
                                </thead>
                                
                                <tbody>
                                    <?php foreach($promotions as $promo): ?>
                                    <tr>
                                        <td><?php echo e($promo->id); ?></td>
                                        <td><?php echo e($promo->name); ?></td>
                                        <td><?php echo e($promo->start_at); ?></td>
                                        <td><?php echo e($promo->end_at); ?></td>
                                        <td class="text-center">
                                            <form class="form-inline" action="<?php echo e(url('promotions', $promo->id)); ?>" method="POST">
                                                <input type="hidden" name="_method" value="DELETE">
                                                <?php echo e(csrf_field()); ?>


                                                <a href="<?php echo e(url('promotions', $promo->id)); ?>" class="btn btn-info" aria-label="Left Align" data-product-name="1">
                                                    <i class="fa fa-info"></i>
                                                </a>
                                            <?php if (app('Illuminate\Contracts\Auth\Access\Gate')->check('create_promotions')): ?>    
                                                <button class="btn btn-danger"><i class="fa fa-trash"></i></button> 
                                            <?php endif; ?>    
                                            </form>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
<script type="text/javascript">
$(document).ready(function() {
//$('#promotions-table').DataTable();
$('#promotions-table').dataTable({
"language": { 
    "lengthMenu": "_MENU_ "+"<?php echo e(trans('general.table-display-tail')); ?>",
    "zeroRecords": "<?php echo e(trans('general.table-zero-records')); ?>",
    "info": "<?php echo e(trans('general.table-page')); ?>"+" _PAGE_ "+"<?php echo e(trans('general.table-from')); ?>"+" _PAGES_",
    "infoEmpty": "<?php echo e(trans('general.table-zero-records')); ?>",
    "infoFiltered": "(<?php echo e(trans('general.table-filtered-from')); ?> _MAX_ <?php echo e(trans('general.table-filtered-records')); ?>)",
    "search": "<?php echo e(trans('general.search-label')); ?>:",
    "paginate": {
        "first":      "<?php echo e(trans('general.table-first')); ?>",
        "last":       "<?php echo e(trans('general.table-last')); ?>",
        "next":       "<?php echo e(trans('general.table-next')); ?>",
        "previous":   "<?php echo e(trans('general.table-previous')); ?>"
    },
}
});
} );
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>