<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-bars"></i>
</i>
 <?php echo e(trans('users.users')); ?></div>

                <div class="panel-body">
                               
                <?php if (app('Illuminate\Contracts\Auth\Access\Gate')->check('create_users')): ?>
                    <a class="btn btn-primary" href="<?php echo e(url('/users/create')); ?>"><i class="fa fa-plus-circle"></i>
  <?php echo e(trans('users.new-user')); ?></a>
                    <hr>
                <?php endif; ?>
                <div class="row">
                    <div class="col-md-12">
                        <table id="promotions-table" class="display" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th><?php echo e(trans('users.id')); ?></th>
                                    <th><?php echo e(trans('users.username')); ?></th>
                                    <th><?php echo e(trans('users.email')); ?></th>
                                    <th><?php echo e(trans('users.role')); ?></th>
                                    <th><?php echo e(trans('users.cwid')); ?></th>
                                    <th><?php echo e(trans('users.phone')); ?></th>
                                    <th><?php echo e(trans('users.state')); ?></th>
                                    <th><?php echo e(trans('users.action')); ?></th>
                                </tr>
                            </thead>
                            
                            <tbody>
                                 <?php foreach($users as $user): ?>
                                 	<tr>
                                 		<td><?php echo e($user->id); ?></td>
                                 		<td><?php echo e($user->name); ?></td>
                                 		<td><?php echo e($user->email); ?></td>
                                 		<td>
                                 			<?php foreach($user->roles()->get() as $role): ?>
                                 			<?php echo e($role->label); ?> <br>
                                 			<?php endforeach; ?>  
                                 		</td>
                                        <td><?php echo e($user->cwid); ?></td>
                                        <td><?php echo e($user->phone); ?></td>
                                 		<td><?php echo e(($user->is_active)?trans('users.active-yes'):trans('users.active-no')); ?></td>
                                 		<td class="text-center">
                                          <?php if (app('Illuminate\Contracts\Auth\Access\Gate')->check('create_users')): ?>  
                                            <a href="<?php echo e(url('users').'/'.$user->id.'/edit'); ?>" type="button"  id='info' class="btn btn-info" aria-label="Left Align" data-product-name="1">
                                                <i class="fa fa-cog" aria-hidden="true"></i>

                                            </a>
                                            
                                            <?php endif; ?>
                                        </td>
                                 	</tr>
                                 <?php endforeach; ?>  
                            </tbody>
                        </table>
                    </div>
                 </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
    <script type="text/javascript">
        $(document).ready(function() {
            //$('#promotions-table').DataTable();

            $('#promotions-table').dataTable({
                "language": { 
    "lengthMenu": "_MENU_ "+"<?php echo e(trans('general.table-display-tail')); ?>",
    "zeroRecords": "<?php echo e(trans('general.table-zero-records')); ?>",
    "info": "<?php echo e(trans('general.table-page')); ?>"+" _PAGE_ "+"<?php echo e(trans('general.table-from')); ?>"+" _PAGES_",
    "infoEmpty": "<?php echo e(trans('general.table-zero-records')); ?>",
    "infoFiltered": "(<?php echo e(trans('general.table-filtered-from')); ?> _MAX_ <?php echo e(trans('general.table-filtered-records')); ?>)",
    "search": "<?php echo e(trans('general.search-label')); ?>:",
    "paginate": {
        "first":      "<?php echo e(trans('general.table-first')); ?>",
        "last":       "<?php echo e(trans('general.table-last')); ?>",
        "next":       "<?php echo e(trans('general.table-next')); ?>",
        "previous":   "<?php echo e(trans('general.table-previous')); ?>"
    },
}
            });

        } );
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>