<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Bayer OTC</title>

    <!-- Fonts -->
    <link href="<?php echo e(url('assets/libs/font-awesome-4.6.3/css/font-awesome.min.css')); ?>" rel='stylesheet' type='text/css'>
    <link href="<?php echo e(url('assets/css/css.css')); ?>" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="<?php echo e(url('assets/css/bootstrap.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(url('assets/css/main.css')); ?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('assets/css/datatables.min.css')); ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('assets/css/jquery-ui.css')); ?>"/>
 
    <script type="text/javascript" src="<?php echo e(url('assets/scripts/libs/datatables.min.js')); ?>"></script>
    
    <?php /* <link href="<?php echo e(elixir('css/app.css')); ?>" rel="stylesheet"> */ ?>

    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
</head>
<body id="app-layout">
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="<?php echo e(url('/')); ?>">
                   <img id="logo" src="<?php echo e(url('assets/images/logo.png')); ?>"> Bayer OTC
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    <?php echo $__env->make('partials.nav', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    <?php if(Auth::guest()): ?>
                        <li><a href="<?php echo e(url('/login')); ?>"><i class="fa fa-sign-in"></i>
<?php echo e(trans('general.login')); ?></a></li>
                        
                    <?php else: ?>
                       <!--<li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                <i class="fa fa-bell"></i>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="<?php echo e(url('/orders')); ?>"><i class="fa fa-check-circle-o"></i> 5 заявки за одобрение</a></li>
                            </ul>

                        </li>
                         -->
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                <?php echo e(Auth::user()->name); ?> <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="<?php echo e(url('/logout')); ?>"><i class="fa fa-btn fa-sign-out"></i><?php echo e(trans('general.logout')); ?></a></li>
                            </ul>
                        </li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </nav>

    <?php echo $__env->yieldContent('content'); ?>

    <!-- JavaScripts -->
    <script src="<?php echo e(url('assets/scripts/libs/jquery.min.js')); ?>"></script>
    <script src="<?php echo e(url('assets/scripts/libs/bootstrap.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/scripts/jquery-ui.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(url('assets/scripts/libs/datatables.min.js')); ?>"></script>
         <?php echo $__env->yieldContent('javascript'); ?>


        <?php /* <script src="<?php echo e(elixir('js/app.js')); ?>"></script> */ ?>
</body>
</html>
