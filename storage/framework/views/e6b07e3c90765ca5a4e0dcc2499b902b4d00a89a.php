<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-bars"></i></i>
                    <?php echo e(trans('general.reports')); ?>

                    <hr>
                    <form action="<?php echo e(url('reports')); ?>" class="form-inline">
                        <div class="row">
                            
                            <div class="col-md-4">
                                <?php /*<div class="row">*/ ?>

                                    <?php /*<div class="col-md-12">*/ ?>
                                        <?php /*<label for="inputDate_month"><?php echo e(trans('reports.month')); ?>:</label>*/ ?>
                                        <?php /*<select name="month" id="inputDate_month" class="form-control">*/ ?>
                                            <?php /*<option value="<?php echo e(\Carbon\Carbon::now()->month); ?>"><?php echo e(trans('reports.'.\Carbon\Carbon::now()->format('M'))); ?></option>*/ ?>
                                            <?php /*<option value="1"><?php echo e(trans('reports.Jan')); ?></option>*/ ?>
                                            <?php /*<option value="2"><?php echo e(trans('reports.Feb')); ?></option>*/ ?>
                                            <?php /*<option value="3"><?php echo e(trans('reports.Mar')); ?></option>*/ ?>
                                            <?php /*<option value="4"><?php echo e(trans('reports.Apr')); ?></option>*/ ?>
                                            <?php /*<option value="5"><?php echo e(trans('reports.May')); ?></option>*/ ?>
                                            <?php /*<option value="6"><?php echo e(trans('reports.Jun')); ?></option>*/ ?>
                                            <?php /*<option value="7"><?php echo e(trans('reports.Jul')); ?></option>*/ ?>
                                            <?php /*<option value="8"><?php echo e(trans('reports.Aug')); ?></option>*/ ?>
                                            <?php /*<option value="9"><?php echo e(trans('reports.Sep')); ?></option>*/ ?>
                                            <?php /*<option value="10"><?php echo e(trans('reports.Oct')); ?></option>*/ ?>
                                            <?php /*<option value="11"><?php echo e(trans('reports.Nov')); ?></option>*/ ?>
                                            <?php /*<option value="12"><?php echo e(trans('reports.Dec')); ?></option>*/ ?>
                                        <?php /*</select>*/ ?>
                                        <?php /**/ ?>
                                    <?php /**/ ?>
                                        <?php /*<label for="inputDate_year"><?php echo e(trans('reports.year')); ?>:</label>*/ ?>
                                    <?php /**/ ?>
                                        <?php /*<select name="year" id="inputDate_month" class="form-control">*/ ?>
                                            <?php /*<option value="<?php echo e(\Carbon\Carbon::now()->year); ?>"><?php echo e(\Carbon\Carbon::now()->year); ?></option>*/ ?>
                                        <?php /*</select>*/ ?>
                                        <?php /**/ ?>
                                    <?php /*</div>*/ ?>
                                <?php /*</div>*/ ?>
                                <?php /*<hr>    */ ?>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="venue_type"><?php echo e(trans('reports.venue-type')); ?>:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <select name="venue_type" id="inputVenue_type" class="form-control">
                                            <option value="single"><?php echo e(trans('reports.single')); ?></option>
                                            <option value="chain"><?php echo e(trans('reports.chain')); ?></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="venue_id"><?php echo e(trans('reports.venue-id')); ?>:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" id="venue_id" name="venue_name" class="form-control" value="<?php echo e(old('venue_id')); ?>">
                                    </div>
                                </div>
                                <hr>    
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="promo_type"><?php echo e(trans('reports.product-type')); ?>:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <select name="promo_type" id="inputPromo_type" class="form-control">
                                            <option value=""></option>
                                            <option value="promotion"><?php echo e(trans('reports.product-type-promo')); ?></option>
                                            <option value="package"><?php echo e(trans('reports.product-type-package')); ?></option>
                                            <option value="product"><?php echo e(trans('reports.product-type-single')); ?></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="promo_name"><?php echo e(trans('reports.product')); ?>:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" id="promo_name" name="promo_name" class="form-control" value="<?php echo e(old('promo_name')); ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">

                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="inputDate_from"><?php echo e(trans('reports.from')); ?>:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="date" name="date_from" id="inputDate_from" class="form-control datepicker" value="<?php echo e(getDateBeforeDays(30)); ?>" title="From:">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="inputDate_to"><?php echo e(trans('reports.to')); ?>:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="date" name="date_to" id="inputDate_to" class="form-control datepicker" value="<?php echo e(getDateBeforeDays(0)); ?>" title="To:">
                                    </div>
                                </div>
                                <hr>    
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="bulstat"><?php echo e(trans('reports.bulstat')); ?>:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" id="bulstat" name="bulstat" class="form-control" value="<?php echo e(old('bulstat')); ?>">
                                        
                                    </div>
                                </div>
                                <br> 
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="seller_id"><?php echo e(trans('reports.reseller')); ?>:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" id="seller_name" name="seller_name" class="form-control" value="<?php echo e(old('seller_name')); ?>">
                                        
                                    </div>
                                </div>
                                <br>  
                                
                                <?php if(!isRepresentative(Auth::user())): ?>   
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="user"><?php echo e(trans('reports.user')); ?>:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" id="user" name="user" class="form-control" value="<?php echo e(old('bulstat')); ?>">
                                        
                                    </div>
                                </div>
                                <?php endif; ?>
                            </div>
                            <div class="col-md-4">

                               <div class="row">
                                    <div class="col-md-4">
                                        <label for="sortBy"><?php echo e(trans('reports.sort-by')); ?>:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <select name="sortBy" id="sortBy" class="form-control">
                                            <option value=""></option>
                                            <option value="date_asc"><?php echo e(trans('reports.date_asc')); ?></option>
                                            <option value="date_desc"><?php echo e(trans('reports.date_desc')); ?></option>
                                            <option value="seller"><?php echo e(trans('reports.reseller')); ?></option>
                                            <option value="pharm"><?php echo e(trans('reports.pharm')); ?></option>
                                            <option value="chain"><?php echo e(trans('reports.chain')); ?></option>
                                            <option value="city"><?php echo e(trans('reports.city')); ?></option>
                                            <option value="user"><?php echo e(trans('reports.user')); ?></option>
                                        </select>
                                        
                                    </div>
                                </div>

                                <hr>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="sortBy"><?php echo e(trans('reports.state')); ?>:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <select name="state" id="state" class="form-control">
                                            <option value=""></option>
                                            <option value="confirmed"><?php echo e(trans('orders.confirmed')); ?></option>
                                            <option value="declined"><?php echo e(trans('orders.declined')); ?></option>
                                            <option value="waiting"><?php echo e(trans('orders.waiting')); ?></option>
                                        </select>
                                        
                                    </div>
                                </div>
                                <br>    
                                <br>    
                                <br>    
                                <div class="row"> 
                                    <div class="col-md-6 col-md-offset-3">
                                        <button type="submit" class="btn btn-lg btn-primary"><i class="fa fa-search" aria-hidden="true"> <?php echo e(trans('reports.search')); ?></i></button>
                                    </div>
                                </div>  
                            </div>
                        </div>
                           
                        </div>
                       
                        
                    </form>
                </div>
               
                <div class="panel-body">
                    <div class="btn-group pull-right">
                        <form class="form-inline" action="<?php echo e(url('reports/export')); ?>" method="POST">
                            <input type="hidden" name="view" value="<?php echo e($view); ?>">

                            <input type="hidden" name="orders" value="<?php echo e($orders); ?>">
                            <?php echo e(csrf_field()); ?>

                            
                            <button class="btn btn-info"><i class="fa fa-download"> <?php echo e(trans('promotions.export')); ?> </i></button>
                        </form>
                    </div>
                    <br>
                    <br>
                    <div>
                        <?php echo $__env->make($view,$orders, array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    </div>
                </div>
               
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
<script type="text/javascript">
$(document).ready(function() {

 $(".datepicker").datepicker({dateFormat: "yy-mm-dd"});

//$('#promotions-table').DataTable();
$('#reports-table').dataTable({
"language": {
"lengthMenu": "_MENU_ "+"<?php echo e(trans('general.table-display-tail')); ?>",
"zeroRecords": "<?php echo e(trans('general.table-zero-records')); ?>",
"info": "<?php echo e(trans('general.table-page')); ?>"+" _PAGE_ "+"<?php echo e(trans('general.table-from')); ?>"+" _PAGES_",
"infoEmpty": "<?php echo e(trans('general.table-zero-records')); ?>",
"infoFiltered": "(<?php echo e(trans('general.table-filtered-from')); ?> _MAX_ <?php echo e(trans('general.table-filtered-records')); ?>)",
"search": "<?php echo e(trans('general.search-label')); ?>:",
"paging": "false",
/*"paginate": {
"first":      "<?php echo e(trans('general.table-first')); ?>",
"last":       "<?php echo e(trans('general.table-last')); ?>",
"next":       "<?php echo e(trans('general.table-next')); ?>",
"previous":   "<?php echo e(trans('general.table-previous')); ?>"
},*/
}
});
$('#orders-history-table').dataTable({
"language": {
"lengthMenu": "_MENU_ "+"<?php echo e(trans('general.table-display-tail')); ?>",
"zeroRecords": "<?php echo e(trans('general.table-zero-records')); ?>",
"info": "<?php echo e(trans('general.table-page')); ?>"+" _PAGE_ "+"<?php echo e(trans('general.table-from')); ?>"+" _PAGES_",
"infoEmpty": "<?php echo e(trans('general.table-zero-records')); ?>",
"infoFiltered": "(<?php echo e(trans('general.table-filtered-from')); ?> _MAX_ <?php echo e(trans('general.table-filtered-records')); ?>)",
"search": "<?php echo e(trans('general.search-label')); ?>:",
/*"paginate": {
"first":      "<?php echo e(trans('general.table-first')); ?>",
"last":       "<?php echo e(trans('general.table-last')); ?>",
"next":       "<?php echo e(trans('general.table-next')); ?>",
"previous":   "<?php echo e(trans('general.table-previous')); ?>"
},*/
}
});
} );
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>