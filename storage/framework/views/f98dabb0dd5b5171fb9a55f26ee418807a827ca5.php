<table id="reports-table" class="display" width="100%" cellspacing="0">
	<thead>
		<tr>
			<th><?php echo e(trans('reports.date')); ?></th>
			<th><?php echo e(trans('reports.user')); ?></th>
			<th><?php echo e(trans('reports.pharmacy')); ?></th>
			<th><?php echo e(trans('reports.product-type')); ?></th>
			<th><?php echo e(trans('reports.product')); ?></th>
			<th><?php echo e(trans('reports.total-quantity')); ?></th>
			<th><?php echo e(trans('reports.total-rabat')); ?></th>
			<th><?php echo e(trans('reports.reseller')); ?></th>
			<th><?php echo e(trans('reports.state')); ?></th>
			
		</tr>
	</thead>
	
	<tbody>
		<?php foreach($orders as $item): ?>
			<tr>
				<td><?php echo e(formatDate($item->order_date)); ?></td>
				<td><?php echo e($item->user_name); ?></td>
				<td><?php echo e($item->pharmacy_name?:$item->pharmacy_id); ?></td>
				<td><?php echo e(getPromoTypeName($item->item_type)); ?></td>
				<td><?php echo e($item->item_name); ?></td>
				<td><?php echo e($item->quantity); ?></td>
				<td><?php echo e($item->rabat); ?></td>
				<td><?php echo e($item->reseller_name); ?></td>
				<td><?php echo e(trans('orders.'.getStateByOrder($item))); ?></td>
			</tr>
		<?php endforeach; ?>
		
		
	</tbody>
</table>