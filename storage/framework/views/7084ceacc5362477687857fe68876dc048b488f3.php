<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-plus-circle"></i>
                <?php echo e(trans('resellers.create-new-reseller-group')); ?></div>
                <div class="panel-body">
                    <form  class="form-horizontal" role="form" method="POST"  action="<?php echo e(url('/resellers/groups')); ?>">
                        <?php echo csrf_field(); ?>

                        <div class="form-group<?php echo e($errors->has('name') ? ' has-error' : ''); ?>">
                            <label class="col-md-4 control-label"><?php echo e(trans('resellers.group-name')); ?></label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="name" value="<?php echo e(old('name')); ?>">
                                <?php if($errors->has('name')): ?>
                                <span class="help-block">
                                    <strong><?php echo e($errors->first('name')); ?></strong>
                                </span>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                <i class="fa fa-btn fa-user-plus"></i><?php echo e(trans('general.create-btn')); ?>

                                </button>
                            </div>
                        </div>
                    </form>
                    
                    <div class="list-group">
                        <a href="#" class="list-group-item active">
                            <h4 class="list-group-item-heading"><?php echo e(trans('resellers.groups')); ?></h4>
                        </a>
                        <?php foreach($groups as $group): ?>
                        <a href="#" class="list-group-item"><?php echo e($group->name); ?></a>
                        <?php endforeach; ?>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>