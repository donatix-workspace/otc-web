<!DOCTYPE html>
<html>
	<head>
		<title>Reports</title>
		<font face="arial">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<style>
		*{
			font-family:'dejavu sans';
			text-align: center;
			
		}
		th {
			background-color: #337ab7;
			color: white;
		}
		table{
			border-collapse:unset;  
		}
		.page-break {
		    page-break-after: always;
		}
		tr{page-break-after: always;}
		tr:nth-child(even) {background-color: #f2f2f2; }
		</style>
	</head>
	<body>
		<?php echo $__env->make($view,$chunks, array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	</body>
</html>