<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><?php echo e(trans('users.edit')); ?></div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="<?php echo e(url('/users', $user['id'])); ?>">
                        <input type="hidden" name="_method" value="PUT">
                        <?php echo csrf_field(); ?>

                        <div class="form-group<?php echo e($errors->has('name') ? ' has-error' : ''); ?>">
                            <label class="col-md-4 control-label"><?php echo e(trans('users.name')); ?></label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="name" value="<?php echo e($user['name']); ?>">
                                <?php if($errors->has('name')): ?>
                                <span class="help-block">
                                    <strong><?php echo e($errors->first('name')); ?></strong>
                                </span>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="form-group<?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
                            <label class="col-md-4 control-label"><?php echo e(trans('users.email')); ?></label>
                            <div class="col-md-6">
                                <input type="email" class="form-control" name="email" value="<?php echo e($user['email']); ?>">
                                <?php if($errors->has('email')): ?>
                                <span class="help-block">
                                    <strong><?php echo e($errors->first('email')); ?></strong>
                                </span>
                                <?php endif; ?>
                            </div>
                        </div>
                         <div class="form-group<?php echo e($errors->has('phone') ? ' has-error' : ''); ?>">
                            <label class="col-md-4 control-label"><?php echo e(trans('users.phone')); ?></label>
                            <div class="col-md-6">
                                <input type="phone" class="form-control" name="phone" value="<?php echo e($user->phone); ?>">
                            </div>
                            </div>
                        <div class="form-group<?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
                            <label class="col-md-4 control-label"><?php echo e(trans('users.cwid')); ?></label>
                            <div class="col-md-6">
                                <input type="cwid" class="form-control" name="cwid" value="<?php echo e($user['cwid']); ?>">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-md-4 control-label"><?php echo e(trans('users.role')); ?></label>
                            <div class="col-md-4">
                                <select name="role_name" class="form-control">
                                    <?php foreach($roles as $role): ?>
                                    <option <?php echo e($user->hasRole($role->name) ? 'selected' : ''); ?> value="<?php echo e($role->name); ?>">
                                        <?php echo e($role->label); ?>

                                    </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label"><?php echo e(trans('users.state')); ?></label>
                            <div class="col-md-4">
                                <div class="radio" class="form-control">
                                    <label><input type="radio" name="is_active" value="1" <?php echo e($user->is_active?'checked':''); ?>><?php echo e(trans('users.active-yes')); ?></label>
                                </div>
                                <div class="radio" class="form-control">
                                    <label><input type="radio" name="is_active" value="0" <?php echo e($user->is_active?'':'checked'); ?>><?php echo e(trans('users.active-no')); ?></label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group<?php echo e($errors->has('password') ? ' has-error' : ''); ?>">
                            <label class="col-md-4 control-label"><?php echo e(trans('users.password')); ?></label>
                            <div class="col-md-6">
                                <input type="password" class="form-control" id="password" name="password">
                                <?php if($errors->has('password')): ?>
                                <span class="help-block">
                                    <strong><?php echo e($errors->first('password')); ?></strong>
                                </span>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="form-group<?php echo e($errors->has('password_confirmation') ? ' has-error' : ''); ?>">
                            <label class="col-md-4 control-label"><?php echo e(trans('users.confirm-password')); ?></label>
                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password_confirmation">
                                <?php if($errors->has('password_confirmation')): ?>
                                <span class="help-block">
                                    <strong><?php echo e($errors->first('password_confirmation')); ?></strong>
                                </span>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                <i class="fa fa-btn fa-user"></i><?php echo e(trans('users.edit')); ?>

                                </button>
                            </div>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>