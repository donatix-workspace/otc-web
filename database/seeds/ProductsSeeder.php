<?php

use App\Http\Utilities\Product;
use App\ProductViewModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;


class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        foreach (Product::all() as $row) {
            ProductViewModel::create($row);
        }

        Model::reguard();
    }
}
