<?php

use Illuminate\Database\Seeder;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            'name' => 'create_resellers',
            'label' => 'Create Resellers',
        ]);
        DB::table('permissions')->insert([
            'name' => 'edit_resellers',
            'label' => 'Edit Resellers',
        ]);
        DB::table('permissions')->insert([
            'name' => 'generate_reports',
            'label' => 'Generate Reports',
        ]);
        DB::table('permissions')->insert([
            'name' => 'create_promotions',
            'label' => 'Generate Promotions',
        ]);
         DB::table('permissions')->insert([
            'name' => 'see_orders',
            'label' => 'Access Orders',
        ]); 
         DB::table('permissions')->insert([
            'name' => 'approve_orders',
            'label' => 'Approve Orders',
        ]);
          DB::table('permissions')->insert([
            'name' => 'create_promos',
            'label' => 'Create Promotions',
        ]);
            DB::table('permissions')->insert([
            'name' => 'create_users',
            'label' => 'Create Users',
        ]);
            DB::table('permissions')->insert([
            'name' => 'delete_promos',
            'label' => 'Delete Promotions',
        ]);
            DB::table('permissions')->insert([
            'name' => 'generate_reports',
            'label' => 'Generate Reports',
        ]);




        DB::table('roles')->insert([
            'name' => 'super_admin',
            'label' => 'Super Admin',
        ]);

        DB::table('permission_role')->insert([
            'permission_id' => 1,
            'role_id' => 1,
        ]);

        DB::table('permission_role')->insert([
            'permission_id' => 2,
            'role_id' => 1,
        ]);
        DB::table('permission_role')->insert([
            'permission_id' => 3,
            'role_id' => 1,
        ]);
        DB::table('permission_role')->insert([
            'permission_id' => 4,
            'role_id' => 1,
        ]);
         DB::table('permission_role')->insert([
            'permission_id' => 5,
            'role_id' => 1,
        ]);
          DB::table('permission_role')->insert([
            'permission_id' => 6,
            'role_id' => 1,
        ]);
           DB::table('permission_role')->insert([
            'permission_id' => 7,
            'role_id' => 1,
        ]);
            DB::table('permission_role')->insert([
            'permission_id' => 8,
            'role_id' => 1,
        ]);
             DB::table('permission_role')->insert([
            'permission_id' => 9,
            'role_id' => 1,
        ]);
        DB::table('permission_role')->insert([
            'permission_id' => 10,
            'role_id' => 1,
        ]);

        
        DB::table('users')->insert([
            'name' => 'Виктор Лалев',
            'cwid' => '',
            'token' => 'viktor',
            'email' => 'viktor.lalev@donatix.net',
            'password' => bcrypt('bayer123'),
        ]);  

        DB::table('role_user')->insert([
            'role_id' => 1,
            'user_id' => 1,
        ]);   
    }
}
