<?php

use App\Http\Utilities\Pharmacy;
use App\PharmacyViewModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class PharmaciesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        foreach (Pharmacy::all() as $row) {
            PharmacyViewModel::create($row);
        }

        Model::reguard();
    }
}
