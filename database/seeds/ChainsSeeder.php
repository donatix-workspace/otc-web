<?php

use App\ChainViewModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class ChainsSeeder extends Seeder
{
    private $data = [
        ['id' => '@535-5W', 'name' => "АвиценаЧА", 'is_deleted' => false],
        ['id' => '@535-7W', 'name' => "АИППМП", 'is_deleted' => false],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
            
        foreach($this->data as $row) {
            ChainViewModel::create($row);
        }

        Model::reguard();
    }
}
