<?php

use App\Permission;
use App\Role;
use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    private $permissions = [
        ['name' => 'create_resellers','label' => 'Create Resellers'],
        ['name' => 'edit_resellers', 'label' => 'Edit Resellers'],
        ['name' => 'generate_reports', 'label' => 'Generate Reports'],
        ['name' => 'create_promotions', 'label' => 'Generate Promotions'],
        ['name' => 'see_orders', 'label' => 'Access Orders'], 
        ['name' => 'approve_orders', 'label' => 'Approve Orders'],
        ['name' => 'create_promos', 'label' => 'Create Promotions'],
        ['name' => 'create_users', 'label' => 'Create Users'],
        ['name' => 'delete_promos', 'label' => 'Delete Promotions'],
        ['name' => 'generate_reports', 'label' => 'Generate Reports',]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        DB::table('permission_role')->truncate();
        DB::table('permissions')->truncate();

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        foreach ($this->permissions as $permission) {
            Permission::create($permission);
        }

        $admin = Role::where('name', 'super_admin')->first();
        $admin->permissions()->attach([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
        
        Role::create([
            'name' => 'promotion_approver',
            'label' => 'Promotion Approver',
        ])->permissions()->attach([5, 6, 9]);

        Role::create([
            'name' => 'technical_admin',
            'label' => 'Technical Admin',
        ])->permissions()->attach([1, 2, 4, 7, 8, 10]);
    }
}
