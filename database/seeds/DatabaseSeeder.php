<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//Roles Seeder
        //$this->call(RolesAndPermissionsSeeder::class);
        $this->call(RolesSeeder::class);
    }
}
