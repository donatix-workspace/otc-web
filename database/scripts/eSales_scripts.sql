--Script for products view - v_bayer_otc_products
ALTER VIEW [dbo].[v_bayer_otc_products]
AS
SELECT     pf.OBJECT_ID AS id, pf.PRODUCT_FAMILY AS name, pf.CHANGED AS updated_at, pf.DELETED AS is_deleted,pf.STATUS as status
FROM         dbo_aws.PRODUCT_FAMILY_PRODUCT_LINE AS pfl INNER JOIN
             dbo_aws.PRODUCT_FAMILY AS pf ON pfl.Product_Family_Id = pf.PRODUCT_FAMILY_ID
WHERE     (pfl.Product_Line_Id = '20') or (pfl.Product_Line_Id = '30')

--Script for Pharmacies -view v_bayer_otc_pharmacies

ALTER VIEW [dbo].[v_bayer_otc_pharmacies]
AS
SELECT o.ORGANIZATION_ID AS id,
       ao.ACCOUNT_ID AS chain_id,
       o.COMPANY_FULL_NAME AS name,
       oo.USER_ID AS owner_id,
       o.B_CITY AS city,
       o.B_STREET_ADDRESS1 AS address,
       o.CHANGED AS updated_at,
       o.DELETED AS is_deleted,
       oes.EXTERNAL_SYSTEM_NO AS bulstat,
       ao.DELETED as ao_deleted
FROM dbo_aws.ORGANIZATION_OWNER AS oo
         INNER JOIN dbo_aws.ORGANIZATION AS o ON oo.ORGANIZATION_ID = o.ORGANIZATION_ID
         LEFT OUTER JOIN dbo_aws.Organization_External_System AS oes ON o.ORGANIZATION_ID = oes.ORGANIZATION_ID
         LEFT OUTER JOIN dbo_aws.ACCOUNT_ORGANIZATION AS ao ON ao.ORGANIZATION_ID = o.ORGANIZATION_ID
WHERE ((oes.EXTERNAL_SYSTEM_ID = 'EQVJM_IJ9Z2HC5')
    OR (oes.EXTERNAL_SYSTEM_ID = 'EUIHS_K1TD72CA')
    OR (oes.EXTERNAL_SYSTEM_ID IS NULL))
  AND oo.DELETED=0

--Script for Chains -view v_bayer_otc_chains
ALTER VIEW [dbo].[v_bayer_otc_chains]
AS
SELECT     TOP (1000) ACCOUNT_ID AS id, ACCOUNT_NAME AS name, CHANGED AS updated_at, STATUS AS is_deleted
FROM         dbo_aws.ACCOUNT