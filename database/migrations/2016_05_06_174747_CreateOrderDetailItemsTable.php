<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDetailItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_detail_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_detail_id')->unsigned();
            $table->integer('reseller_id')->unsigned()->nullable;
            $table->string('item_type');
            $table->string('item_id');
            $table->integer('package_id')->unsigned()->nullable;
            $table->integer('quantity')->unsigned();
            $table->integer('rabat')->unsigned();
            $table->timestamps();

            $table->foreign('order_detail_id')
                    ->references('id')
                    ->on('order_details')
                    ->onDelete('cascade');
                    
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('order_detail_items');
    }
}
