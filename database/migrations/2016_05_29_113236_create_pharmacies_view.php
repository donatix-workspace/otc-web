<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePharmaciesView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('v_bayer_otc_pharmacies', function (Blueprint $table) {
            $table->string('id', 20)->primary();
            $table->string('chain_id', 20)->index()->nullable();
            $table->string('owner_id', 10)->index()->nullable();
            $table->string('name');
            $table->string('city');
            $table->string('address');
            $table->string('bulstat')->index();
            $table->boolean('is_deleted');
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('v_bayer_otc_pharmacies');
    }
}
