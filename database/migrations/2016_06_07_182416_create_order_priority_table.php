<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderPriorityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_priority', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('reseller_id')->unsigned();
            $table->integer('order_id')->unsigned();
            $table->string('order_created_at')->nullable();
            $table->string('manufacturer')->nullable();
            $table->string('city')->nullable();
            $table->string('client')->nullable();
            $table->string('address')->nullable();
            $table->string('bulstat')->nullable();
            $table->string('item_type');
            $table->string('type');
            $table->integer('package_id')->unsigned();
            $table->string('item');
            $table->integer('quantity')->unsigned();
            $table->integer('rabat')->unsigned();
            $table->text('comment');
            $table->string('email');
            $table->boolean('ready_for_send');         
            $table->timestamps();

            $table->foreign('order_id')
                    ->references('id')
                    ->on('orders')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('order_priority');
    }
}
