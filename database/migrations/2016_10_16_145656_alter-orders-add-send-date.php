<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOrdersAddSendDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function ($table) {
            $table->timestamp('send_date')->nullable();

        });
        Schema::table('order_queue', function ($table) {
            $table->timestamp('send_date')->nullable();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function ($table) {
            $table->dropColumn('send_date');
        });
        Schema::table('order_queue', function ($table) {
            $table->dropColumn('send_date');
        });
    }
}
