<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChainsView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('v_bayer_otc_chains', function (Blueprint $table) {
            $table->string('id', 20)->primary();
            $table->string('name');
            $table->boolean('is_deleted');
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('v_bayer_otc_chains');
    }
}
