<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_histories', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('order_id')->unsigned();
            $table->timestamp('order_date');
            $table->string('order_type');
            $table->string('city');
            $table->string('bulstat');
            $table->string('pharmacy_id');
            $table->string('pharmacy_name');
            $table->string('chain_id')->nullable();
            $table->string('chain_name')->nullable();
            $table->enum('item_type',['product','promotion','packadge']);
            $table->string('item_id');
            $table->string('item_name')->nullable();
            $table->string('package_id')->nullable();
            $table->integer('quantity');
            $table->integer('rabat');
            $table->integer('user_id')->unsigned();
            $table->string('user_name');
            $table->integer('reseller_id')->unsigned();
            $table->string('reseller_name');
            $table->boolean('need_confirmation')->default(0);
            $table->boolean('confirmed')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('order_histories');
    }
}
