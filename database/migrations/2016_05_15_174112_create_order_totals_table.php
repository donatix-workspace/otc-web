<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTotalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_total_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->unsigned();
            $table->string('type');
            $table->integer('package_id')->unsigned();
            $table->integer('package_quantity')->unsigned();
            $table->string('item_id');
            $table->integer('quantity')->unsigned();
            $table->integer('rabat')->unsigned();
            $table->integer('reseller_id')->unsigned();
            $table->timestamps();

            $table->foreign('order_id')
                    ->references('id')
                    ->on('orders')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('order_total_items');
    }
}
