<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderQueueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_queue', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('reseller_id')->unsigned();
            $table->integer('order_id')->unsigned();
            $table->string('order_created_at')->nullable();
            $table->string('manufacturer');
            $table->string('city');
            $table->string('client');
            $table->string('address');
            $table->string('bulstat')->nullable();
            $table->string('item_type');
            $table->string('type');
            $table->integer('package_id')->unsigned();
            $table->string('item');
            $table->integer('quantity')->unsigned();
            $table->integer('rabat')->unsigned();
            $table->text('comment');
            $table->string('email');
            $table->boolean('ready_for_send');         
            $table->timestamps();

            $table->foreign('order_id')
                    ->references('id')
                    ->on('orders')
                    ->onDelete('cascade');
        });
    }
                   
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('order_queue');
    }
}
