<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResellerUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reseller_user', function (Blueprint $table) {
           
            $table->integer('reseller_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->foreign('reseller_id')
                  ->references('id')
                  ->on('resellers')
                  ->onDelete('cascade');
            $table->foreign('user_id')
                  ->references('id')
                  ->on('users')
                  ->onDelete('cascade');
            $table->primary(['reseller_id', 'user_id']);
        });    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reseller_user');
    }
}
