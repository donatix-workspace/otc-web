@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-bars"></i>
</i>
 {{ trans('users.users') }}</div>

                <div class="panel-body">
                               
                @can('create_users')
                    <a class="btn btn-primary" href="{{ url('/users/create') }}"><i class="fa fa-plus-circle"></i>
  {{ trans('users.new-user') }}</a>
                    <hr>
                @endcan
                <div class="row">
                    <div class="col-md-12">
                        <table id="promotions-table" class="display" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>{{ trans('users.id') }}</th>
                                    <th>{{ trans('users.username') }}</th>
                                    <th>{{ trans('users.email') }}</th>
                                    <th>{{ trans('users.role') }}</th>
                                    <th>{{ trans('users.cwid') }}</th>
                                    <th>{{ trans('users.phone') }}</th>
                                    <th>{{ trans('users.state') }}</th>
                                    <th>{{ trans('users.action') }}</th>
                                </tr>
                            </thead>
                            
                            <tbody>
                                 @foreach ($users as $user)
                                 	<tr>
                                 		<td>{{$user->id}}</td>
                                 		<td>{{$user->name}}</td>
                                 		<td>{{$user->email}}</td>
                                 		<td>
                                 			@foreach ($user->roles()->get() as $role)
                                 			{{$role->label}} <br>
                                 			@endforeach  
                                 		</td>
                                        <td>{{$user->cwid}}</td>
                                        <td>{{$user->phone}}</td>
                                 		<td>{{($user->is_active)?trans('users.active-yes'):trans('users.active-no')}}</td>
                                 		<td class="text-center">
                                          @can('create_users')  
                                            <a href="{{url('users').'/'.$user->id.'/edit'}}" type="button"  id='info' class="btn btn-info" aria-label="Left Align" data-product-name="1">
                                                <i class="fa fa-cog" aria-hidden="true"></i>

                                            </a>
                                            
                                            @endcan
                                        </td>
                                 	</tr>
                                 @endforeach  
                            </tbody>
                        </table>
                    </div>
                 </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
    <script type="text/javascript">
        $(document).ready(function() {
            //$('#promotions-table').DataTable();

            $('#promotions-table').dataTable({
                "language": { 
    "lengthMenu": "_MENU_ "+"{{ trans('general.table-display-tail') }}",
    "zeroRecords": "{{ trans('general.table-zero-records') }}",
    "info": "{{ trans('general.table-page') }}"+" _PAGE_ "+"{{ trans('general.table-from') }}"+" _PAGES_",
    "infoEmpty": "{{ trans('general.table-zero-records') }}",
    "infoFiltered": "({{ trans('general.table-filtered-from') }} _MAX_ {{ trans('general.table-filtered-records') }})",
    "search": "{{ trans('general.search-label') }}:",
    "paginate": {
        "first":      "{{ trans('general.table-first') }}",
        "last":       "{{ trans('general.table-last') }}",
        "next":       "{{ trans('general.table-next') }}",
        "previous":   "{{ trans('general.table-previous') }}"
    },
}
            });

        } );
    </script>
@endsection
