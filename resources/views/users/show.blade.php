@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{ trans('users.edit') }}</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/users', $user['id']) }}">
                        <input type="hidden" name="_method" value="PUT">
                        {!! csrf_field() !!}
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">{{ trans('users.name') }}</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="name" value="{{ $user['name'] }}">
                                @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">{{ trans('users.email') }}</label>
                            <div class="col-md-6">
                                <input type="email" class="form-control" name="email" value="{{ $user['email'] }}">
                                @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                         <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">{{ trans('users.phone') }}</label>
                            <div class="col-md-6">
                                <input type="phone" class="form-control" name="phone" value="{{ $user->phone }}">
                            </div>
                            </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">{{ trans('users.cwid') }}</label>
                            <div class="col-md-6">
                                <input type="cwid" class="form-control" name="cwid" value="{{ $user['cwid'] }}">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('users.role') }}</label>
                            <div class="col-md-4">
                                <select name="role_name" class="form-control">
                                    @foreach ($roles as $role)
                                    <option {{ $user->hasRole($role->name) ? 'selected' : '' }} value="{{$role->name}}">
                                        {{$role->label}}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">{{ trans('users.state') }}</label>
                            <div class="col-md-4">
                                <div class="radio" class="form-control">
                                    <label><input type="radio" name="is_active" value="1" {{$user->is_active?'checked':''}}>{{ trans('users.active-yes') }}</label>
                                </div>
                                <div class="radio" class="form-control">
                                    <label><input type="radio" name="is_active" value="0" {{$user->is_active?'':'checked'}}>{{ trans('users.active-no') }}</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">{{ trans('users.password') }}</label>
                            <div class="col-md-6">
                                <input type="password" class="form-control" id="password" name="password">
                                @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">{{ trans('users.confirm-password') }}</label>
                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password_confirmation">
                                @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                <i class="fa fa-btn fa-user"></i>{{ trans('users.edit') }}
                                </button>
                            </div>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection