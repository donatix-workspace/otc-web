@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-bars"></i>
                    </i>
                {{ trans('orders.orders') }}</div>
                <div class="panel-body">
                    
                    @if (!Auth::guest())
                    <div class="row">
                        <div class="col-md-12">
                            <div class="orders-for-comfirm">
                                <h3>{{ trans('orders.orders-comfirm-table') }}</h3>
                                <table id="orders-comfirm-table" class="display" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>{{ trans('orders.number') }}</th>
                                            <th>{{ trans('orders.object-name') }}</th>
                                            <th>{{ trans('orders.from') }}</th>
                                            <th>{{ trans('orders.submit-date') }}</th>
                                            <th>{{ trans('orders.send-date') }}</th>
                                            <th>{{ trans('general.actions') }}</th>
                                            <th>{{ trans('general.state') }}</th>
                                            
                                        </tr>
                                    </thead>
                                    
                                    <tbody>
                                    @if(!empty($ordersForConfirm))
                                        @foreach ($ordersForConfirm as $order)
                                        <tr>
                                            <td>{{$order->id}}</td>
                                            <td>{{$order->object_name}}</td>
                                            <td>{{$order->user_name}}</td>
                                            <td>{{$order->created_at}}</td>
                                            <td>{{formatDate($order->order_date?$order->order_date:$order->created_at)}}</td>
                                            <td class="text-center">
                                                <a href="{{url('orders').'/'.$order->id}}" type="button"  id='info' class="btn btn-info " aria-label="Left Align" data-product-name="1">
                                                    <i class="fa fa-info"></i>
                                                </a>
                                                

                                            </td>
                                            <td>
                                                <i class="fa fa-refresh" aria-hidden="true"></i>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>
                                <hr>
                            </div>
                            <div class="orders-history">
                                <h3>{{ trans('orders.orders-history-table') }}</h3>
                                <table id="orders-history-table" class="display" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>{{ trans('orders.number') }}</th>
                                            <th>{{ trans('orders.object-name') }}</th>
                                            <th>{{ trans('orders.from') }}</th>
                                            <th>{{ trans('orders.submit-date') }}</th>
                                            <th>{{ trans('orders.send-date') }}</th>
                                            <th>{{ trans('general.actions') }}</th>
                                            <th>{{ trans('general.state') }}</th>
                                        </tr>
                                    </thead>
                                    
                                    <tbody>
                                    @if(!empty($orders))
                                        @foreach ($orders as $order)
                                        <tr>
                                            <td>{{$order->id}}</td>
                                            <td>{{$order->object_name}}</td>
                                            <td>{{$order->user_name}}</td>
                                            <td>{{$order->created_at}}</td>
                                            <td>{{formatDate($order->order_date?$order->order_date:$order->created_at)}}</td>
                                            <td class="text-center"><a href="{{url('orders').'/'.$order->id}}" type="button"  id='info' class="btn btn-info " aria-label="Left Align" data-product-name="1">
                                                <i class="fa fa-info"></i>
                                            </a>
                                            </td>
                                            <td>
                                               <i class="{{getOrderState($order)}}"></i>
                                            </td>
                                        </tr>
                                        @endforeach
                                      @endif
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    @else
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-info">
                                {{ trans('messages.page-no-login') }}
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
@section('javascript')
<script type="text/javascript">
$(document).ready(function() {
//$('#promotions-table').DataTable();
$('#orders-comfirm-table').dataTable({
"language": {
"lengthMenu": "_MENU_ "+"{{ trans('general.table-display-tail') }}",
"zeroRecords": "{{ trans('general.table-zero-records') }}",
"info": "{{ trans('general.table-page') }}"+" _PAGE_ "+"{{ trans('general.table-from') }}"+" _PAGES_",
"infoEmpty": "{{ trans('general.table-zero-records') }}",
"infoFiltered": "({{ trans('general.table-filtered-from') }} _MAX_ {{ trans('general.table-filtered-records') }})",
"search": "{{ trans('general.search-label') }}:",
"paginate": {
"first":      "{{ trans('general.table-first') }}",
"last":       "{{ trans('general.table-last') }}",
"next":       "{{ trans('general.table-next') }}",
"previous":   "{{ trans('general.table-previous') }}"
},
}
});
$('#orders-history-table').dataTable({
"order": [[ 1, "asc" ]],
"language": {
"lengthMenu": "_MENU_ "+"{{ trans('general.table-display-tail') }}",
"zeroRecords": "{{ trans('general.table-zero-records') }}",
"info": "{{ trans('general.table-page') }}"+" _PAGE_ "+"{{ trans('general.table-from') }}"+" _PAGES_",
"infoEmpty": "{{ trans('general.table-zero-records') }}",
"infoFiltered": "({{ trans('general.table-filtered-from') }} _MAX_ {{ trans('general.table-filtered-records') }})",
"search": "{{ trans('general.search-label') }}:",
"paginate": {
"first":      "{{ trans('general.table-first') }}",
"last":       "{{ trans('general.table-last') }}",
"next":       "{{ trans('general.table-next') }}",
"previous":   "{{ trans('general.table-previous') }}"
},
}
});
} );
</script>
@endsection