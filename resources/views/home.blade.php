@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                @can('generate_reports')
                    You can generate reports!
                @endcan
                
                @can('create_resellers')
                    You Can Create Resellers!
                @endcan
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
