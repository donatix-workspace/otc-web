<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Bayer OTC</title>

    <!-- Fonts -->
    <link href="{{url('assets/libs/font-awesome-4.6.3/css/font-awesome.min.css')}}" rel='stylesheet' type='text/css'>
    <link href="{{url('assets/css/css.css')}}" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="{{url('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{url('assets/css/main.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{url('assets/css/datatables.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{url('assets/css/jquery-ui.css')}}"/>
 
    <script type="text/javascript" src="{{url('assets/scripts/libs/datatables.min.js')}}"></script>
    
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
</head>
<body id="app-layout">
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                   <img id="logo" src="{{url('assets/images/logo.png')}}"> Bayer OTC
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    @include('partials.nav')
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}"><i class="fa fa-sign-in"></i>
{{ trans('general.login') }}</a></li>
                        
                    @else
                       <!--<li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                <i class="fa fa-bell"></i>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/orders') }}"><i class="fa fa-check-circle-o"></i> 5 заявки за одобрение</a></li>
                            </ul>

                        </li>
                         -->
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>{{ trans('general.logout') }}</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')

    <!-- JavaScripts -->
    <script src="{{url('assets/scripts/libs/jquery.min.js')}}"></script>
    <script src="{{url('assets/scripts/libs/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/scripts/jquery-ui.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/scripts/libs/datatables.min.js')}}"></script>
         @yield('javascript')


        {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
</body>
</html>
