@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-info">
                        {{ trans('messages.page-no-login') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
