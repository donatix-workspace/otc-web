@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-plus-circle"></i>
{{ trans('promotions.create-new-package') }}</div>
                <div class="panel-body">

                    <form  class="form-horizontal" role="form" method="POST" files='true' action="{{ url('/promotions') }}">
                        {!! csrf_field() !!}
                         <div class="row">
                            <div class="col-md-8">
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label class="col-md-4 control-label">{{ trans('promotions.name') }}</label>

                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="name" value="{{ old('name') }}">

                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('start_at') ? ' has-error' : '' }}">
                                    <label class="col-md-4 control-label">{{ trans('promotions.promo-start-date') }}</label>

                                    <div class="col-md-4">
                                        <input type="date" class="form-control" name="start_at" value="{{ date("Y-m-d") }}">

                                        @if ($errors->has('start_at'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('start_at') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('start_at') ? ' has-error' : '' }}">
                                    <label class="col-md-4 control-label">{{ trans('promotions.promo-end-date') }}</label>

                                    <div class="col-md-4">
                                        <input type="date" class="form-control" name="end_at" value="{{ date("Y-m-d")  }}">

                                        @if ($errors->has('end_at'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('end_at') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-1 col-md-offset-2">
                                        <span>Продукт</span>
                                    </div>
                                    <div class="col-md-2 col-md-offset-1">
                                        <span>Количество</span>
                                    </div>
                                    <div class="col-md-2 col-md-offset-1">
                                        <span>Рабат</span>
                                    </div>
                                    <hr>
                                </div>
                                <div class="row">
                                    <div class="col-md-1 col-md-offset-2">
                                        <span>Продукт</span>
                                    </div>
                                    <div class="col-md-2 col-md-offset-1">
                                       <input type="number" class="form-control" name="products[]" value="">
                                    </div>
                                    <div class="col-md-2 col-md-offset-1">
                                        <input type="number" class="form-control" name="products[]" value="">
                                    </div>
                                </div>
                                
                            </div>
                                

                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            <i class="fa fa-btn fa-user-plus"></i>{{ trans('general.create-btn') }}
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <h4>{{ trans('general.products-label') }}</h4>
                                <table id="products-table" class="display" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>{{ trans('promotions.table-id') }}</th>
                                            <th>{{ trans('promotions.name') }}</th>
                                            <th> {{ trans('general.add-label') }} </th>
                                        </tr>
                                    </thead>
                                    
                                    <tbody>
                                        
                                         @foreach (App\Http\Utilities\Product::all() as $product)
                                           <tr>
                                                <td>{{$product['id']}}</td>
                                                <td>{{$product['name']}}</td>
                                                <td><a href="javascript:;"><i class="fa fa-btn fa-plus"></i></a></td>
                                            </tr> 
                                         @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
    <script type="text/javascript">
        $(document).ready(function() {
            //$('#promotions-table').DataTable();

            $('#products-table').dataTable({
                "language": { 
    "lengthMenu": "_MENU_ "+"{{ trans('general.table-display-tail') }}",
    "zeroRecords": "{{ trans('general.table-zero-records') }}",
    "info": "{{ trans('general.table-page') }}"+" _PAGE_ "+"{{ trans('general.table-from') }}"+" _PAGES_",
    "infoEmpty": "{{ trans('general.table-zero-records') }}",
    "infoFiltered": "({{ trans('general.table-filtered-from') }} _MAX_ {{ trans('general.table-filtered-records') }})",
    "search": "{{ trans('general.search-label') }}:",
    "paginate": {
        "first":      "{{ trans('general.table-first') }}",
        "last":       "{{ trans('general.table-last') }}",
        "next":       "{{ trans('general.table-next') }}",
        "previous":   "{{ trans('general.table-previous') }}"
    },
}
            });

        } );
    </script>
@endsection