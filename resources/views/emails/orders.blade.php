<!DOCTYPE html>
<html>
<head>
	<title>Поръчки</title>
	<font face="arial"> 
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>

@if(!empty($groupedOrders['totalsPromoForSinglePharms']))
	<div>
	<p>Заявка за единчни промоции/ продукти за аптеки</p>
		<table>
		
			<tr style="background-color: #ccc;">
				<td>Дата</td>
				<td>Производител</td>
				<td>Град</td>
				<td>Име на клиент</td>
				<td>адрес</td>
				<td>булстат</td>
				<td>вид</td>
				<td>продукт ид</td>
				<td>продукт</td>
				<td>количество</td>
				<td>рабат</td>
				<td>коментар</td>
				<td>представител</td>
				<td>телефон</td>
			</tr>
		@foreach ($groupedOrders['totalsPromoForSinglePharms'] as $total)
			@if($total->quantity != 0 || $total->rabat!=0)

				<tr>
					<td>{{$total->created_at}}</td>
					<td>{{$total->manufacturer}}</td>
					<td>{{$total->city}}</td>
					<td>{{$total->client}}</td>
					<td>{{$total->address}}</td>
					<td>{{$total->bulstat}}</td>
					<td>{{ trans('orders.'.$total->type) }}</td>
					<td>{{$total->item}}</td>
					<td>{{productName($total->item_type,$total->item)}}</td>
					<td>{{$total->quantity}}</td>
					<td>{{$total->rabat}}</td>
					<td>{{$total->comment}}</td>
					<td>{{$total->order->user->name}}</td>
					<td>{{$total->order->user->phone}}</td>
				</tr>

			@endif
		@endforeach
			<tr style="background-color: #ffff00;">
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td>{{$groupedOrders['totalsPromoForSinglePharms']->sum('quantity')}}</td>
				<td>{{$groupedOrders['totalsPromoForSinglePharms']->sum('rabat')}}</td>
				<td></td>
				<td></td>
				<td></td>

			</tr>
		</table>
	</div>
@endif
@if(!empty($groupedOrders['totalsPromoForChains']))
	<div>
	<p>Заявка за единични пакети/ продукти за вериги ( ако е към един и същ дистрибутор)</p>
		<table>
		
			<tr style="background-color: #ccc;">
				<td>Дата</td>
				<td>Производител</td>
				<td>Град</td>
				<td>Име на клиент</td>
				<td>адрес</td>
				<td>булстат</td>
				<td>вид</td>
				<td>продукт ид</td>
				<td>продукт</td>
				<td>количество</td>
				<td>рабат</td>
				<td>коментар</td>
				<td>представител</td>
				<td>телефон</td>
			</tr>
		@foreach ($groupedOrders['totalsPromoForChains'] as $total)
			@if($total->quantity != 0 || $total->rabat!=0)
				<tr>
					<td>{{$total->created_at}}</td>
					<td>{{$total->manufacturer}}</td>
					<td>{{$total->city}}</td>
					<td>{{$total->client}}</td>
					<td>{{$total->address}}</td>
					<td>{{$total->bulstat}}</td>
					<td>{{ trans('orders.'.$total->type) }}</td>
					<td>{{$total->item}}</td>
					<td>{{productName($total->item_type,$total->item)}}</td>
					<td>{{$total->quantity}}</td>
					<td>{{$total->rabat}}</td>
					<td>{{$total->comment}}</td>
					<td>{{$total->order->user->name}}</td>
					<td>{{$total->order->user->phone}}</td>
				</tr>
			@endif
		@endforeach
			<tr style="background-color: #ffff00;">
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td>{{$groupedOrders['totalsPromoForChains']->sum('quantity')}}</td>
				<td>{{$groupedOrders['totalsPromoForChains']->sum('rabat')}}</td>
				<td></td>
				<td></td>
				<td></td>

			</tr>
		</table>
	</div>
@endif


@if( !empty($packagesArray))

	<div>
		@foreach ($packagesArray as $package)
			<p>{{packageName($package->id)}}</p>
			<table>
				<tr style="background-color: #ccc;">
					<td>Продукт</td>
					<td>Количество</td>
					<td>Натурален рабат</td>
				</tr>
				@foreach ($package->products as $product)
				<tr>
					<td>{{productName('product',$product->product_id)}}</td>
					<td>{{$product->quantity}}</td>
					<td>{{$product->rabat}}</td>
				</tr>
				@endforeach
				<tr style="background-color: #ffff00;">
					<td></td>
					<td>{{$package->products->sum('quantity')}}</td>
					<td>{{$package->products->sum('rabat')}}</td>
				</tr>
			</table>
		@endforeach
	</div>
@endif
@if(!empty($groupedOrders['totalsPackageForSinglePharms']))
	<div>
	<p>Заявка за ПАКЕТ / единична аптека</p>
		
		<table>
			<tr style="background-color: #ccc;">
				<td>Дата</td>
				<td>Производител</td>
				<td>Град</td>
				<td>Име на клиент</td>
				<td>адрес</td>
				<td>булстат</td>
				<td>вид</td>
				<td>продукт ид</td>
				<td>продукт</td>
				<td>количество</td>
				<td>рабат</td>
				<td>коментар</td>
				<td>представител</td>
				<td>телефон</td>
			</tr>
		@foreach ($groupedOrders['totalsPackageForSinglePharms'] as $total)
			@if($total->quantity != 0 || $total->rabat!=0)
				<tr>
					<td>{{$total->created_at}}</td>
					<td>{{$total->manufacturer}}</td>
					<td>{{$total->city}}</td>
					<td>{{$total->client}}</td>
					<td>{{$total->address}}</td>
					<td>{{$total->bulstat}}</td>
					<td>{{ trans('orders.'.$total->type) }}</td>
					<td>{{$total->item}}</td>
					<td>{{productName($total->item_type,$total->item)}}</td>
					<td>{{$total->quantity}}</td>
					<td>{{$total->rabat}}</td>
					<td>{{$total->comment}}</td>
					<td>{{$total->order->user->name}}</td>
					<td>{{$total->order->user->phone}}</td>
				</tr>
			@endif
		@endforeach
			<tr style="background-color: #ffff00;">
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td>{{$groupedOrders['totalsPackageForSinglePharms']->sum('quantity')}}</td>
				<td>{{$groupedOrders['totalsPackageForSinglePharms']->sum('rabat')}}</td>
				<td></td>
				<td></td>
				<td></td>

			</tr>
		</table>
	</div>
@endif
@if(!empty($groupedOrders['totalsPackageForChains']))
	<div>
	<p>Заявка за ПАКЕТ към вериги</p>
		<table>
		
			<tr style="background-color: #ccc;">
				<td>Дата</td>
				<td>Производител</td>
				<td>Град</td>
				<td>Име на клиент</td>
				<td>адрес</td>
				<td>булстат</td>
				<td>вид</td>
				<td>продукт ид</td>
				<td>продукт</td>
				<td>количество</td>
				<td>рабат</td>
				<td>коментар</td>
				<td>представител</td>
				<td>телефон</td>
			</tr>
		@foreach ($groupedOrders['totalsPackageForChains'] as $total)
			@if($total->quantity != 0 || $total->rabat!=0)
				<tr>
					<td>{{$total->created_at}}</td>
					<td>{{$total->manufacturer}}</td>
					<td>{{$total->city}}</td>
					<td>{{$total->client}}</td>
					<td>{{$total->address}}</td>
					<td>{{$total->bulstat}}</td>
					<td>{{ trans('orders.'.$total->type) }}</td>
					<td>{{$total->item}}</td>
					<td>{{productName($total->item_type,$total->item)}}</td>
					<td>{{$total->quantity}}</td>
					<td>{{$total->rabat}}</td>
					<td>{{$total->comment}}</td>
					<td>{{$total->order->user->name}}</td>
					<td>{{$total->order->user->phone}}</td>
				</tr>
			@endif
		@endforeach
			<tr style="background-color: #ffff00;">
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td>{{$groupedOrders['totalsPackageForChains']->sum('quantity')}}</td>
				<td>{{$groupedOrders['totalsPackageForChains']->sum('rabat')}}</td>
				<td></td>
				<td></td>
				<td></td>

			</tr>
		</table>
	</div>
@endif
</body>
</html>