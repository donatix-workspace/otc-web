@foreach($chunks as $orders)
<table id="reports-table" class="display" width="100%" cellspacing="0">
	<thead>
		<tr>
			<th>{{ trans('reports.date') }}</th>
			<th>{{ trans('reports.chain') }}</th>	
			<th>{{ trans('reports.product') }}</th>
			<th>{{ trans('reports.pharmacy') }}</th>
			<th>{{ trans('reports.total-quantity') }}</th>
			<th>{{ trans('reports.total-rabat') }}</th>	
			<th>{{ trans('reports.reseller') }}</th>
		</tr>
	</thead>
	
	<tbody>
		@foreach($orders as $item)
			@if(isset($item->orderDetail->chain))
			<tr>
				<td>{{formatDate($item->order_date)}}</td>
				<td>{{$item->chain_name}}</td>
				<td>{{$item->item_name}}</td>
				<td>{{$item->pharmacy_name}}</td>
				<td>{{$item->quantity}}</td>
				<td>{{$item->rabat}}</td>
				<td>{{$item->reseller_name}}</td>
			</tr>
			@endif
		@endforeach
		
		
	</tbody>
</table>
<br>
<div class="page-break"></div>
@endforeach