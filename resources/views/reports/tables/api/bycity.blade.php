@foreach($chunks as $orders)
<table id="reports-table" class="display" width="100%" cellspacing="0">
	<thead>
		<tr>
			<th>{{ trans('reports.date') }}</th>
			<th>{{ trans('reports.city') }}</th>
			<th>{{ trans('reports.pharmacy') }}</th>
			<th>{{ trans('reports.product') }}</th>
			<th>{{ trans('reports.total-quantity') }}</th>
			<th>{{ trans('reports.total-rabat') }}</th>
		</tr>
	</thead>
	
	<tbody>
		@foreach($orders as $item)
			{{--			@if($item['order_detail']['order']['type']=='single')--}}
			<tr>
				<td></td>
				<td>{{formatDate($item->order_date)}}</td>
				<td>{{$item->city}}</td>
				<td>{{$item->city}}</td>
				<td>{{$item->pharmacy_name?:$item->pharmacy_id}}</td>
				<td>{{$item->item_name}}</td>
				<td>{{$item->quantity}}</td>
				<td>{{$item->rabat}}</td>
				<td>{{$item->user_name}}</td>
				<td>{{ trans('orders.'.getStateByOrder($item)) }}</td>

			</tr>
			{{--@endif--}}
		@endforeach
		
		
	</tbody>
</table>
<br>
<div class="page-break"></div>
@endforeach