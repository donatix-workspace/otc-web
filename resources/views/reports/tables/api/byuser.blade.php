@foreach($chunks as $orders)
<table id="reports-table" class="display" width="100%" cellspacing="0">
	<thead>
		<tr>
			<th>{{ trans('reports.date') }}</th>
			<th>{{ trans('reports.pharmacy') }}</th>
			<th>{{ trans('reports.product-type') }}</th>
			<th>{{ trans('reports.product') }}</th>
			<th>{{ trans('reports.total-quantity') }}</th>
			<th>{{ trans('reports.total-rabat') }}</th>
			<th>{{ trans('reports.reseller') }}</th>
		</tr>
	</thead>
	
	<tbody>
		@foreach($orders as $item)
			<tr>
				<td>{{formatDate($item->order_date)}}</td>
				<td>{{$item->pharmacy_name?:$item->pharmacy_id}}</td>
				<td>{{getPromoTypeName($item->item_type)}}</td>
				<td>{{$item->item_name}}</td>
				<td>{{$item->quantity}}</td>
				<td>{{$item->rabat}}</td>
				<td>{{$item->reseller_name}}</td>
			</tr>
		@endforeach
		
		
	</tbody>
</table>
<br>
<div class="page-break"></div>
@endforeach