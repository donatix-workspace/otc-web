<table id="reports-table" class="display" width="100%" cellspacing="0">
	<thead>
		<tr>
			<th>{{ trans('reports.date') }}</th>
			<th>{{ trans('reports.chain') }}</th>	
			<th>{{ trans('reports.product') }}</th>
			<th>{{ trans('reports.pharmacy') }}</th>
			<th>{{ trans('reports.total-quantity') }}</th>
			<th>{{ trans('reports.total-rabat') }}</th>	
			<th>{{ trans('reports.user') }}</th>
			<th>{{ trans('reports.reseller') }}</th>
			<th>{{ trans('reports.state') }}</th>
			
		</tr>
	</thead>
	
	<tbody>
		@foreach($orders as $item)
			@if(isset($item->chain_id))
			<tr>
				<td>{{formatDate($item->order_date)}}</td>
				<td>{{$item->chain_name}}</td>
				<td>{{$item->item_name}}</td>
				<td>{{$item->pharmacy_name}}</td>
				<td>{{$item->quantity}}</td>
				<td>{{$item->rabat}}</td>
				<td>{{$item->user_name}}</td>
				<td>{{$item->reseller_name}}</td>
				<td>{{ trans('orders.'.getStateByOrder($item)) }}</td>

			</tr>
			@endif
		@endforeach
		
		
	</tbody>
</table>