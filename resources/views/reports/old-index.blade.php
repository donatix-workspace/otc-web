@if($totalsPromoForSinglePharms->count()>0)
    <div>
    <p>Заявка за единчни промоции/ продукти за аптеки</p>
        <table>
        
            <tr style="background-color: #ccc;">
                <td>Дата</td>
                <td>Производител</td>
                <td>Град</td>
                <td>Име на клиент</td>
                <td>адрес</td>
                <td>булстат</td>
                <td>вид</td>
                <td>продукт ид</td>
                <td>продукт</td>
                <td>количество</td>
                <td>рабат</td>
                <td>коментар</td>
            </tr>
        @foreach ($totalsPromoForSinglePharms as $total)
            <tr>
                <td>{{$total->order_created_at}}</td>
                <td>{{$total->manufacturer}}</td>
                <td>{{$total->city}}</td>
                <td>{{$total->client}}</td>
                <td>{{$total->address}}</td>
                <td>{{$total->bulstat}}</td>
                <td>{{ trans('orders.'.$total->type) }}</td>
                <td>{{$total->item}}</td>
                <td>{{productName($total->item_type,$total->item)}}</td>
                <td>{{$total->quantity}}</td>
                <td>{{$total->rabat}}</td>
                <td>{{$total->comment}}</td>
            </tr>
        @endforeach
            <tr style="background-color: #ffff00;">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>{{$totalsPromoForSinglePharms->sum('quantity')}}</td>
                <td>{{$totalsPromoForSinglePharms->sum('rabat')}}</td>
                <td></td>

            </tr>
        </table>
    </div>
@endif
@if($totalsPromoForChains->count()>0)
    <div>
    <p>Заявка за единични пакети/ продукти за вериги ( ако е към един и същ дистрибутор)</p>
        <table>
        
            <tr style="background-color: #ccc;">
                <td>Дата</td>
                <td>Производител</td>
                <td>Град</td>
                <td>Име на клиент</td>
                <td>адрес</td>
                <td>булстат</td>
                <td>вид</td>
                <td>продукт ид</td>
                <td>продукт</td>
                <td>количество</td>
                <td>рабат</td>
                <td>коментар</td>
            </tr>
        @foreach ($totalsPromoForChains as $total)
            <tr>
                <td>{{$total->order_created_at}}</td>
                <td>{{$total->manufacturer}}</td>
                <td>{{$total->city}}</td>
                <td>{{$total->client}}</td>
                <td>{{$total->address}}</td>
                <td>{{$total->bulstat}}</td>
                <td>{{ trans('orders.'.$total->type) }}</td>
                <td>{{$total->item}}</td>
                <td>{{productName($total->item_type,$total->item)}}</td>
                <td>{{$total->quantity}}</td>
                <td>{{$total->rabat}}</td>
                <td>{{$total->comment}}</td>
            </tr>
        @endforeach
            <tr style="background-color: #ffff00;">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>{{$totalsPromoForChains->sum('quantity')}}</td>
                <td>{{$totalsPromoForChains->sum('rabat')}}</td>
                <td></td>

            </tr>
        </table>
    </div>
@endif

@if(isset($$packagesArray))
@if($packagesArray[0]->count()>0)
    <div>

    @foreach ($packagesArray[0] as $package)
            <p>{{packageName($package->id)}}</p>
        <table>
            <tr style="background-color: #ccc;">
                <td>Продукт</td>
                <td>Количество</td>
                <td>Натурален рабат</td>
            </tr>
            @foreach ($package->products as $product)
            <tr>
                <td>{{productName('product',$product->product_id)}}</td>
                <td>{{$product->quantity}}</td>
                <td>{{$product->rabat}}</td>
            </tr>
            @endforeach
            <tr style="background-color: #ffff00;">
                <td></td>
                <td>{{$package->products->sum('quantity')}}</td>
                <td>{{$package->products->sum('rabat')}}</td>
            </tr>
        </table>
    @endforeach
    </div>
@endif
@endif
@if($totalsPackageForSinglePharms->count()>0)
    <div>
    <p>Заявка за ПАКЕТ / единична аптека</p>
        
        <table>
            <tr style="background-color: #ccc;">
                <td>Дата</td>
                <td>Производител</td>
                <td>Град</td>
                <td>Име на клиент</td>
                <td>адрес</td>
                <td>булстат</td>
                <td>вид</td>
                <td>продукт ид</td>
                <td>продукт</td>
                <td>количество</td>
                <td>рабат</td>
                <td>коментар</td>
            </tr>
        @foreach ($totalsPackageForSinglePharms as $total)
            <tr>
                <td>{{$total->order_created_at}}</td>
                <td>{{$total->manufacturer}}</td>
                <td>{{$total->city}}</td>
                <td>{{$total->client}}</td>
                <td>{{$total->address}}</td>
                <td>{{$total->bulstat}}</td>
                <td>{{ trans('orders.'.$total->type) }}</td>
                <td>{{$total->item}}</td>
                <td>{{productName($total->item_type,$total->item)}}</td>
                <td>{{$total->quantity}}</td>
                <td>{{$total->rabat}}</td>
                <td>{{$total->comment}}</td>
            </tr>
        @endforeach
            <tr style="background-color: #ffff00;">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>{{$totalsPackageForSinglePharms->sum('quantity')}}</td>
                <td>{{$totalsPackageForSinglePharms->sum('rabat')}}</td>
                <td></td>

            </tr>
        </table>
    </div>
@endif
@if($totalsPackageForChains->count()>0)
    <div>
    <p>Заявка за ПАКЕТ към вериги</p>
        <table>
        
            <tr style="background-color: #ccc;">
                <td>Дата</td>
                <td>Производител</td>
                <td>Град</td>
                <td>Име на клиент</td>
                <td>адрес</td>
                <td>булстат</td>
                <td>вид</td>
                <td>продукт ид</td>
                <td>продукт</td>
                <td>количество</td>
                <td>рабат</td>
                <td>коментар</td>
            </tr>
        @foreach ($totalsPackageForChains as $total)
            <tr>
                <td>{{$total->order_created_at}}</td>
                <td>{{$total->manufacturer}}</td>
                <td>{{$total->city}}</td>
                <td>{{$total->client}}</td>
                <td>{{$total->address}}</td>
                <td>{{$total->bulstat}}</td>
                <td>{{ trans('orders.'.$total->type) }}</td>
                <td>{{$total->item}}</td>
                <td>{{productName($total->item_type,$total->item)}}</td>
                <td>{{$total->quantity}}</td>
                <td>{{$total->rabat}}</td>
                <td>{{$total->comment}}</td>
            </tr>
        @endforeach
            <tr style="background-color: #ffff00;">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>{{$totalsPackageForChains->sum('quantity')}}</td>
                <td>{{$totalsPackageForChains->sum('rabat')}}</td>
                <td></td>

            </tr>
        </table>
    </div>
@endif