<table id="reports-table" class="display" width="100%" cellspacing="0">
	<thead>
		<tr>
		<th></th>
			<th>{{ trans('reports.date') }}</th>
			<th>{{ trans('reports.reseller') }}</th>
			<th>{{ trans('reports.product-type') }}</th>
			<th>{{ trans('reports.product') }}</th>
			<th>{{ trans('reports.total-quantity') }}</th>
			<th>{{ trans('reports.total-rabat') }}</th>
			<th>{{ trans('reports.user') }}</th>
			<th>{{ trans('reports.state') }}</th>

		</tr>
	</thead>
	
	<tbody>
		@foreach($orders as $item)
			<tr>
				<td></td>
				<td>{{formatDate($item->order_date)}}</td>
				<td>{{$item->reseller_name}}</td>
				<td>{{getPromoTypeName($item->item_type)}}</td>
				<td>{{$item->item_name}}</td>
				<td>{{$item->quantity}}</td>
				<td>{{$item->rabat}}</td>
				<td>{{$item->user_name}}</td>
				<td>{{ trans('orders.'.getStateByOrder($item)) }}</td>
			</tr>
		@endforeach
		
		
	</tbody>
</table>