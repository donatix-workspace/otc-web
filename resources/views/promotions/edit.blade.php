@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-8">
                            <i class="fa fa-bars"></i>
                            {{ trans('promotions.promotions') }}
                        </div>
                        <div class="col-md-4">
                            
                        </div>
                    </div>
                    
                </div>
                <div class="panel-body">
                    
                    
                    <div class="row">
                        <div class="col-md-6">
                            <form enctype="multipart/form-data" id="promo_update_form" class="form-horizontal" role="form" method="POST" action="{{ url('/promotions/'.$promo['id']) }}">
                                <input name="_method" id="promo-edit-method" type="hidden" value="PATCH"></input>
                                {!! csrf_field() !!}
                                <dl class="dl-horizontal">
                                    <dt>{{ trans('promotions.name') }}</dt>
                                    <dd><input type="text" class="form-control" name="name" value="{{ $promo['name']}}"></dd>
                                    <dt>{{ trans('promotions.date-start') }}</dt>
                                    <dd>{{formatDate($promo['start_at'])}}</dd>
                                    <dt>{{ trans('promotions.date-end') }}</dt>
                                    <dd><input type="text" class="form-control datepicker" name="end_at" value="{{ formatDate($promo['end_at'])}}"></dd>
                                    <dt>{{ trans('promotions.private') }}</dt>
                                    <dd> <input type="checkbox" id="is_private" name='is_private' value="private" @if($promo['is_private'] )checked @endif)></dd>

                                     @if($promo['is_private'])
                                        <div class="checkbox">
                                        @foreach ($users as $user)
                                        <label><input type="checkbox" name='users[]' value="{{$user->id}}" @if(in_array($user->id, $assignedUsers->toArray())) checked @endif>{{$user->name}} @if(in_array($user->id, $assignedUsers->toArray()))
                                        <i class="fa fa-check" aria-hidden="true"></i>

                                             @endif</label> <br>
                                        @endforeach
                                    </div>
                                    @endif
                                </dl>
                           
                            <button type="submit" class="btn btn-primary">
                            <i class="fa fa-pencil"></i>  {{ trans('general.edit-btn') }}
                            </button>
                            </form>
                        </div>
                        <div class="col-md-6">
                            <div class="list-group">
                                <a href="#" class="list-group-item active">
                                    <h4 class="list-group-item-heading">{{ trans('promotions.products') }}</h4>
                                </a>
                                <table id="promotions-detail-table" class="display" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>{{ trans('promotions.product-name') }}</th>
                                            <th>{{ trans('promotions.quantity') }}</th>
                                            <th>{{ trans('promotions.rabat') }}</th>
                                        </tr>
                                    </thead>
                                    
                                    <tbody>
                                        @foreach ($promo['products'] as $product)
                                        <tr>
                                            <td>{{$product['name']}}</td>
                                            <td>{{$product['product_quantity']}}</td>
                                            <td><span class="label label-success">{{$product['rabat']}}</span></td>
                                        </tr>
                                        @endforeach
                                        
                                    </tbody>
                                </table>
                                
                            </div>
                            
                            
                            
                            
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script type="text/javascript">
$(document).ready(function() {
$(".datepicker").datepicker({dateFormat: "yy-mm-dd"});
$('#promotions-detail-table').dataTable({
"language": {
"lengthMenu": "_MENU_ "+"{{ trans('general.table-display-tail') }}",
"zeroRecords": "{{ trans('general.table-zero-records') }}",
"info": "{{ trans('general.table-page') }}"+" _PAGE_ "+"{{ trans('general.table-from') }}"+" _PAGES_",
"infoEmpty": "{{ trans('general.table-zero-records') }}",
"infoFiltered": "({{ trans('general.table-filtered-from') }} _MAX_ {{ trans('general.table-filtered-records') }})",
"search": "{{ trans('general.search-label') }}:",
"paginate": {
"first":      "{{ trans('general.table-first') }}",
"last":       "{{ trans('general.table-last') }}",
"next":       "{{ trans('general.table-next') }}",
"previous":   "{{ trans('general.table-previous') }}"
},
},
"paging":   false,
"ordering": false,
"searching": false,
"info":     false
});
} );
</script>
@endsection