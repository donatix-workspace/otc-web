@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-plus-circle"></i>
                {{ trans('promotions.create-new-promo') }}</div>
                <div class="panel-body">
                    <form  class="form-horizontal" role="form" method="POST" files='true' action="{{ url('/promotions') }}">
                        {!! csrf_field() !!}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label class="col-md-4 control-label">{{ trans('promotions.name') }}</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                                        @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('start_at') ? ' has-error' : '' }}">
                                    <label class="col-md-4 control-label">{{ trans('promotions.promo-start-date') }}</label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control datepicker" name="start_at" value="{{ date("Y-m-d") }}">
                                        @if ($errors->has('start_at'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('start_at') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('start_at') ? ' has-error' : '' }}">
                                    <label class="col-md-4 control-label">{{ trans('promotions.promo-end-date') }}</label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control datepicker" name="end_at" value="{{ date("Y-m-d")  }}">
                                        @if ($errors->has('end_at'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('end_at') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('start_at') ? ' has-error' : '' }}">
                                    <label class="col-md-4 control-label">{{ trans('promotions.private') }}</label>
                                    <div class="col-md-4">
                                        <input type="checkbox" id="is_private" name='is_private' value="private" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-1 col-md-offset-2">
                                            <span>Продукт</span>
                                        </div>
                                        <div class="col-md-2 col-md-offset-1">
                                            <span>Количество</span>
                                        </div>
                                        <div class="col-md-2 col-md-offset-1">
                                            <span>Рабат</span>
                                        </div>
                                        <hr>
                                    </div>
                                    <div class="input_fields_wrap">
                                    </div>
                                </div>
                                
                                <div class="col-md-8 users">
                                <h3>{{ trans('resellers.users') }}</h3>
                                    <div class="checkbox has-success" >
                                    @foreach ($users as $user)
                                        <label><input type="checkbox" name='users[]' value="{{$user->id}}" >{{$user->name}}</label> <br>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-btn fa-user-plus"></i>{{ trans('general.create-btn') }}
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h4>{{ trans('general.products-label') }}</h4>
                                <table id="products-table" class="display" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>{{ trans('promotions.table-id') }}</th>
                                            <th>{{ trans('promotions.name') }}</th>
                                            <th>{{ trans('general.add-label') }} </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($products as $product)
                                            <tr>
                                                
                                                <td>{{$product['id']}}</td>
                                                <td>{{$product['name']}}</td>
                                                <td><a href="javascript:;" class="add_field_button" data-product-id="{{$product['id']}}" data-product-name="{{$product['name']}}"><i class="fa fa-btn fa-plus"></i></a> </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        
                    </form>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
@section('javascript')
<script type="text/javascript">
$(document).ready(function() {

var is_private = $("#is_private");
var users = $(".users");

$(is_private).on('click', function () {
    if ($(is_private).is(":checked"))
    {
      $(users).css("display","block");
    }
    else{
      $(users).css("display","none");
    }
} );


 $(".datepicker").datepicker({dateFormat: "yy-mm-dd"});


            //todo if promo 1
var max_fields      = 10; //maximum input boxes allowed
var wrapper         = $(".input_fields_wrap"); //Fields wrapper
var add_button      = $(".add_field_button"); //Add button ID

var x = 1; //initlal text box count
$('#products-table').dataTable({

    "bAutoWidth": false,
    "bDeferRender": true,
    "fnDrawCallback": function() {
        

        
    },

"language": { 
    "lengthMenu": "_MENU_ "+"{{ trans('general.table-display-tail') }}",
    "zeroRecords": "{{ trans('general.table-zero-records') }}",
    "info": "{{ trans('general.table-page') }}"+" _PAGE_ "+"{{ trans('general.table-from') }}"+" _PAGES_",
    "infoEmpty": "{{ trans('general.table-zero-records') }}",
    "infoFiltered": "({{ trans('general.table-filtered-from') }} _MAX_ {{ trans('general.table-filtered-records') }})",
    "search": "{{ trans('general.search-label') }}:",
    "paginate": {
        "first":      "{{ trans('general.table-first') }}",
        "last":       "{{ trans('general.table-last') }}",
        "next":       "{{ trans('general.table-next') }}",
        "previous":   "{{ trans('general.table-previous') }}"
    },
}
});
$(add_button).on('click', function (e) {
            e.preventDefault();
            var element=$(this);
            var id=element.data('product-id');
            var name=element.data('product-name');  
            var label = '<div class="row"><div class="col-md-4 col-md-offset-1"><span>'+name+'</span></div><div class="col-md-2 col-md-offset-1"><input type="number" class="form-control" name="products['+id+'][quantity]"></div><div class="col-md-2 col-md-offset-1"><input type="number" class="form-control" name="products['+id+'][rabat]"></div></div>';
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append(label); //add input box
            }
        });
} );

</script>
@endsection
>