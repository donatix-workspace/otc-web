@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-bars"></i>
                    </i>
                {{ trans('promotions.promotions') }}</div>
                <div class="panel-body">
                    
                    @can('create_promotions')
                    <a class="btn btn-primary" href="{{ url('/promotions/create') }}"><i class="fa fa-plus-circle"></i>
                    {{ trans('promotions.create-new-promo') }}</a>
                    <hr>
                    @endcan
                    <div class="row">
                        <div class="col-md-12">
                            <table id="promotions-table" class="display" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>{{ trans('promotions.table-id') }}</th>
                                        <th>{{ trans('promotions.name') }}</th>
                                        <th>{{ trans('promotions.promo-start-date') }}</th>
                                        <th>{{ trans('promotions.promo-end-date') }}</th>
                                        <th>{{ trans('promotions.actions') }}</th>
                                    </tr>
                                </thead>
                                
                                <tbody>
                                    @foreach ($promotions as $promo)
                                    <tr>
                                        <td>{{$promo->id}}</td>
                                        <td>{{$promo->name}}</td>
                                        <td>{{$promo->start_at}}</td>
                                        <td>{{$promo->end_at}}</td>
                                        <td class="text-center">
                                            <form class="form-inline" action="{{ url('promotions', $promo->id) }}" method="POST">
                                                <input type="hidden" name="_method" value="DELETE">
                                                {{ csrf_field() }}

                                                <a href="{{ url('promotions', $promo->id) }}" class="btn btn-info" aria-label="Left Align" data-product-name="1">
                                                    <i class="fa fa-info"></i>
                                                </a>
                                            @can('create_promotions')    
                                                <button class="btn btn-danger"><i class="fa fa-trash"></i></button> 
                                            @endcan    
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script type="text/javascript">
$(document).ready(function() {
//$('#promotions-table').DataTable();
$('#promotions-table').dataTable({
"language": { 
    "lengthMenu": "_MENU_ "+"{{ trans('general.table-display-tail') }}",
    "zeroRecords": "{{ trans('general.table-zero-records') }}",
    "info": "{{ trans('general.table-page') }}"+" _PAGE_ "+"{{ trans('general.table-from') }}"+" _PAGES_",
    "infoEmpty": "{{ trans('general.table-zero-records') }}",
    "infoFiltered": "({{ trans('general.table-filtered-from') }} _MAX_ {{ trans('general.table-filtered-records') }})",
    "search": "{{ trans('general.search-label') }}:",
    "paginate": {
        "first":      "{{ trans('general.table-first') }}",
        "last":       "{{ trans('general.table-last') }}",
        "next":       "{{ trans('general.table-next') }}",
        "previous":   "{{ trans('general.table-previous') }}"
    },
}
});
} );
</script>
@endsection