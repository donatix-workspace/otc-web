@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-8">
                            <i class="fa fa-bars"></i>
                            {{ trans('promotions.promotions') }}
                        </div>
                        <div class="col-md-4">
                            <div class="btn-group pull-right">
                                @can('create_promotions')
                                <a href="{{url('promotions').'/'.$promo['id'].'/edit'}}" type="button"  id='info' class="btn btn-info" aria-label="Left Align" data-product-name="1">
                                    <i class="fa fa-cog" aria-hidden="true"></i>
                                    {{ trans('promotions.edit') }}
                                </a>
                                @endcan
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="panel-body">
                    
                    @can('create_promotions')
                    <a class="btn btn-primary" href="{{ url('/promotions/create') }}"><i class="fa fa-plus-circle"></i>
                    {{ trans('promotions.create-new-promo') }}</a>
                    <hr>
                    @endcan
                    <div class="row">
                        <div class="col-md-6">
                            <dl class="dl-horizontal">
                                <dt>{{ trans('promotions.name') }}</dt>
                                <dd>{{$promo['name']}}</dd>
                                <dt>{{ trans('promotions.date-start') }}</dt>
                                <dd>{{$promo['start_at']}}</dd>
                                <dt>{{ trans('promotions.date-end') }}</dt>
                                <dd>{{$promo['end_at']}}</dd>
                                <dt>{{ trans('promotions.type') }}</dt>
                                <dd>{{isPromoPrivate($promo['is_private'])}}</dd>
                            </dl>
                            @if($promo['is_private'])
                            <dl class="dl-horizontal">
                                <dt>{{ trans('promotions.private-description') }}</dt>
                                
                                @foreach ($promo['users'] as $user)
                                <dd>{{$user['name']}}</dd>
                                @endforeach
                                
                            </dl>
                            @endif
                        </div>
                        <div class="col-md-6">
                            <div class="list-group">
                                <a href="#" class="list-group-item active">
                                    <h4 class="list-group-item-heading">{{ trans('promotions.products') }}</h4>
                                </a>
                                <table id="promotions-detail-table" class="display" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>{{ trans('promotions.product-name') }}</th>
                                            <th>{{ trans('promotions.quantity') }}</th>
                                            <th>{{ trans('promotions.rabat') }}</th>
                                        </tr>
                                    </thead>
                                    
                                    <tbody>
                                        @foreach ($promo['products'] as $product)
                                        <tr>
                                            <td>{{$product['name']}}</td>
                                            <td>{{$product['product_quantity']}}</td>
                                            <td><span class="label label-success">{{$product['rabat']}}</span></td>
                                        </tr>
                                        @endforeach
                                        
                                    </tbody>
                                </table>
                                
                            </div>
                            
                            
                            
                            
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script type="text/javascript">
$(document).ready(function() {
$('#promotions-detail-table').dataTable({
"language": {
"lengthMenu": "_MENU_ "+"{{ trans('general.table-display-tail') }}",
"zeroRecords": "{{ trans('general.table-zero-records') }}",
"info": "{{ trans('general.table-page') }}"+" _PAGE_ "+"{{ trans('general.table-from') }}"+" _PAGES_",
"infoEmpty": "{{ trans('general.table-zero-records') }}",
"infoFiltered": "({{ trans('general.table-filtered-from') }} _MAX_ {{ trans('general.table-filtered-records') }})",
"search": "{{ trans('general.search-label') }}:",
"paginate": {
"first":      "{{ trans('general.table-first') }}",
"last":       "{{ trans('general.table-last') }}",
"next":       "{{ trans('general.table-next') }}",
"previous":   "{{ trans('general.table-previous') }}"
},
},
"paging":   false,
"ordering": false,
"searching": false,
"info":     false
});
} );
</script>
@endsection