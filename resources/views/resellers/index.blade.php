@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-bars"></i>
</i>
 {{ trans('resellers.cpanel') }}</div>

                <div class="panel-body">
                               
                @can('create_resellers')
                    <a class="btn btn-primary" href="{{ url('/resellers/create') }}"><i class="fa fa-plus-circle"></i>
  {{ trans('resellers.create-new-reseller') }}</a>
                    
                    <a class="btn btn-primary" href="{{ url('/resellers/groups/create') }}"><i class="fa fa-plus-circle"></i>
  {{ trans('resellers.create-new-reseller-group') }}</a>
                    <hr>
                @endcan
                <div class="row">
                    @foreach ($resellers as $reseller)
                        <div class="col-md-4">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    {{ $reseller->name }} - {{ $reseller->email }} 
                                    @can('edit_resellers')

                                    <a class="pull-right" href='resellers/{{ $reseller->id }}/edit'>
                                        <i class="fa fa-pencil" aria-hidden="true"></i>

                                    </a>
                                     
                                    @endcan
                                </div>
                                <div class="re-images">
                                    <img src="/uploaded/images/{{$reseller->image}}">
                                 </div>
                            </div>
                        
                            
                        </div>
                       

                     @endforeach
                 </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection