@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-plus-circle"></i>
                {{ trans('resellers.create-new-reseller-group') }}</div>
                <div class="panel-body">
                    <form  class="form-horizontal" role="form" method="POST"  action="{{ url('/resellers/groups') }}">
                        {!! csrf_field() !!}
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">{{ trans('resellers.group-name') }}</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                                @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                <i class="fa fa-btn fa-user-plus"></i>{{ trans('general.create-btn') }}
                                </button>
                            </div>
                        </div>
                    </form>
                    
                    <div class="list-group">
                        <a href="#" class="list-group-item active">
                            <h4 class="list-group-item-heading">{{ trans('resellers.groups') }}</h4>
                        </a>
                        @foreach ($groups as $group)
                        <a href="#" class="list-group-item">{{$group->name}}</a>
                        @endforeach
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>
</div>
@endsection