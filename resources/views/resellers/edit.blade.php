@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">{{ trans('resellers.reseller') }}</div>
                <div class="panel-body">
                    <form enctype="multipart/form-data" id="re_update_form" class="form-horizontal" role="form" method="POST" files='true' action="{{ url('/resellers/'.$reseller->id) }}">
                        <input name="_method" id="re-edit-method" type="hidden" value="PATCH"></input>
                        <input name="is_deleted" id="re-edit-is-deleted" type="hidden" value="0"></input>
                        {!! csrf_field() !!}
                        <div class="row">
                            <div class="col-md-9">
                                
                                <div class="media">
                                    <div class="media-left">
                                        <a href="#">
                                            <img width='64' class="media-object" src="/uploaded/images/{{$reseller->image}}" >
                                        </a>
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label class="col-md-4 control-label">{{ trans('resellers.name') }}</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="name" value="{{ $reseller->name }}">
                                        @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('group_id') ? ' has-error' : '' }}">
                                    <label class="col-md-4 control-label">{{ trans('resellers.group') }}</label>
                                    <div class="col-md-6">

                                        <select name="group_id" class="form-control" id="sel1">
                                            
                                            @foreach ($groups as $group)

                                            <option {{ $reseller->group['id']== $group->id ? 'selected' : '' }} value="{{$group->id}}">{{$group->name}}</option>
                                            @endforeach
                                        </select>
                                        
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label class="col-md-4 control-label">{{ trans('resellers.email') }}</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="email" value="{{ $reseller->email }}">
                                        @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                    <label class="col-md-4 control-label">{{ trans('resellers.description') }}</label>
                                    <div class="col-md-6">
                                        <textarea type="text" class="form-control" name="description" >{{$reseller->description }}</textarea>
                                        
                                        @if ($errors->has('description'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('description') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                                    <label class="col-md-4 control-label">{{ trans('resellers.image') }}</label>
                                    <div class="col-md-6">
                                        
                                        <input type="file" name="image" value="{{ old('image') }}">
                                        @if ($errors->has('photo'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('photo') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-3">
                                <h3>{{ trans('resellers.users') }}</h3>
                                <div class="checkbox">
                                    @foreach ($users as $user)
                                    <label><input type="checkbox" name='users[]' value="{{$user->id}}" @if(in_array($user->id, $assignedUsers->toArray())) checked @endif>{{$user->name}} @if(in_array($user->id, $assignedUsers->toArray()))
                                    <i class="fa fa-check" aria-hidden="true"></i>

@endif</label> <br>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <a href=javascript:; id='re-del-btn' class="btn btn-danger">
                                                    <i class="fa fa-trash"></i>  {{ trans('general.delete-btn') }}
                                                </a>
                                            </div>
                                            <div class="col-md-2">
                                                <button type="submit" class="btn btn-primary">
                                                <i class="fa fa-pencil"></i>  {{ trans('general.edit-btn') }}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script>
$( "#re-del-btn" ).click(function() {
$("#re-edit-method").val("DELETE");
$('#re_update_form').submit();
});
</script>
@endsection