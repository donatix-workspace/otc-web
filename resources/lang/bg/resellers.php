<?php 


return [


    'resellers' => 'Дистрибутори',
    'reseller' => 'Дистрибутор',
    'create-new-reseller' => 'Нов дистрибутор',
    'create-new-reseller-group' => 'Нова група',
    'group' => 'Група',
    'groups' => 'Групи',

    'name' => 'Име на дистрибутора',
    'group-name' => 'Име на група',
    'email' => 'Имейл за контакт',
    'description' => 'Описание',
    'image' => 'Лого на дистрибутора',
    'users' => 'Търговски представители',
    

    'cpanel' => 'Контролен панел',


];