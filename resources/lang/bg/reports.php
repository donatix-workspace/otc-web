<?php 

return [
		
		'pharm'=>'Аптека',
		'product-type-single'=>'Единичен продукт',
		'product-type-promo'=>'Промоция',
		'product-type-package'=>'Пакет',
		'city'=>'Град',
		'chain'=>'Верига',
		'single'=>'Аптека',
		'pharmacy'=>'Аптека',
		'product'=>'Продукт',
		'user'=>'Представител',
		'total-quantity'=>'Количество',
		'total-rabat'=>'Рабат',
		'reseller'=>'Дистрибутор',
		'product-type'=>'Тип на продукт',


		'date'=>'Дата на изпращане',
		'date_asc'=>'Възходящ ред',
		'date_desc'=>'Низходящ ред',
		'sort-by'=>'Групиране',

		'search'=>'Търсене',
		'from'=>'От Дата',
		'to'=>'До Дата',
		'venue-type'=>'Тип на обект',
		'venue-id'=>'Обект',
		'bulstat'=>'Булстат',
		'state'=>'Статус',

		'month'=>'Месец',
		'year'=>'Година',
		'Jan'=>'Януари',
		'Feb'=>'Февруари',
		'Mar'=>'Март',
		'Apr'=>'Април',
		'May'=>'Май',
		'Jun'=>'Юни',
		'Jul'=>'Юли',
		'Aug'=>'Август',
		'Sep'=>'Септември',
		'Oct'=>'Октомври',
		'Nov'=>'Ноември',
		'Dec'=>'Декември',

];