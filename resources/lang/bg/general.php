<?php 


return [


    'home' => 'Начало',
    'login' => 'Вход',
    'logout' => 'Изход',
    'register' => 'Регистрация',
    'delete-label' => 'изтрий',
    'edit-label' => 'промени',
    'edit-btn' => 'Промени',
    'create-btn' => 'Създай',
    'delete-btn' => 'Изтрий',
    'search-label' => 'Търсене',
    'products-label' => 'Продукти',
    'add-label' => 'Добави',
    'actions' => 'Действия',
    'users' => 'Потребители',
    'orders' => 'Поръчки',
    'reports' => 'Справки',
    'table-display-tail' => 'реда',
    'table-zero-records' => 'Няма налични записи',
    'table-page' => 'Страница',
    'table-from' => 'от',
    'table-filtered-from' => 'от',
    'table-filtered-records' => 'записа',
    'table-first' => 'Първи',
    'table-last' => 'Последен',
    'table-next' => 'Следващ',
    'table-previous' => 'Предходен',
    'state' => 'Статус',


];