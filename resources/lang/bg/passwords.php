<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Паролата трябва да е минимум 6 символа',
    'reset' => 'Паролата Ви е рестартирана',
    'sent' => 'Изпартихме Ви e-mail с връзка за подновяване',
    'token' => 'Линкът за подновяване е невалиден',
    'user' => "Не откриваме потребител с този email",

];
