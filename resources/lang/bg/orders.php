<?php 

return [


    'order' => 'Поръчка',
    'orders' => 'Поръчки',
    'from' => 'От',
    'accept' => 'Потвърди',
    'decline' => 'Откажи',
    'delete' => 'Изтрий',

    'orders-comfirm-table' => 'Поръчки чакащи одобрение',
    'orders-history-table' => 'История на поръчките',

    'chain-name' => 'Верига',
    'pharm-name' => 'Аптека',
    'object-name' => 'Обект',
    'number' => '№',
    'submit-date' => 'Дата на съставяне',
    'send-date' => 'Дата за изпращане',
    'signiture' => 'Подпис',

    'sales-representative' => 'Търговски представител',
    'sales-representative-name' => 'Име',
    'sales-representative-email' => 'Мейл',
    'sales-representative-cwid' => 'CWID',


    'sales-resellers' => 'Дистрибутори',
    'sales-resselers-name' => 'Име',
    'sales-resselers-comment' => 'Коментар',


    'details' => 'Детайли на поръчката',
    'pharmacy' => 'Аптека',
    'single' => 'Аптека',
    'chain' => 'Верига',
    'package_id' => 'Пакет',
    'product-type' => 'Тип',
    'product-name' => 'Има на продукт',
    'product-quantity' => 'Количество',
    'product-rabat' => 'Рабат',
    'product-reseller' => 'Дистрибутор',

    'packadge' => 'Пакет',
    'promotion' => 'Промоция',
    'product' => 'Продукт',

    'confirmed' => 'Потвърдена',
    'declined' => 'Отхвърлена',
    'waiting' => 'Очаква одобрениe',





];