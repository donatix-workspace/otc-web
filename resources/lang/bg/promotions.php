<?php 


return [


    'promotions' => 'Промоции',
    'packages' => 'Пакети',
    'create-new-promo' => 'Новa промоция',
    'create-new-package' => 'Нов пакет',
    'actions' => 'Действия',

    'name' => 'Име на промоцията',
    'description' => 'Описание',
    'promo-start-date' => 'Начало на промоцията',
    'date-start' => 'Начало на промоцията',
    'promo-end-date' => 'Край на промоцията',
    'date-end' => 'Край на промоцията',

    'export' => 'Експорт',

    'products' => 'Продукти',
    'product-name' => 'Име на продукт',
    'quantity' => 'Количество',
    'rabat' => 'Рабат',
    'private'=>'Частна',
    'type'=>'Тип',
    'public'=>'Публична',
    'private-description'=>'Описание',


    'cpanel' => 'Контролен панел',
    'delete' => 'Изтриване',
    'edit' => 'Промяна',
   
    'table-id' => '№',




];