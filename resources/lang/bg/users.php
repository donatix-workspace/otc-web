<?php 

return [
	
	'new-user'=>'Нов потребител',
	'users'=>'Потребители',
	'create'=>'Създай потребител',
	'id'=>'ID',
	'username'=>'Име',
	'name'=>'Име',
	'email'=>'E-mail',
	'role'=>'Роля',
	'cwid'=>'CWID',
	'action'=>'Действие',
	'edit'=>'Промени',
	'password'=>'Парола',
	'confirm-password'=>'Потвърди парола',
	'generate-password'=>'Генериране на парола',
	'state'=>'Статус',
	'phone'=>'Телефон',
	'active-yes'=>'Активен',
	'active-no'=>'Неактивен',
];