<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Order;
use App\OrderFilters;
use App\User;
use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;


use App\OrderDetailItem;
use App\OrderDetail;


use Excel;

class ReportController extends Controller
{

    public function __construct()
    {
        //$this->middleware('page.permission:super_admin');   
    }
    
    public function index(OrderFilters $filters, Request $request)
    {   
        if (Auth::guest()) {
           return redirect('no-permissions');
        }
        $startDate=Carbon::create($request->year,$request->month,12,0,0)->startOfMonth()->toDateTimeString();
        $endDate=Carbon::create($request->year,$request->month,12,0,0)->endOfMonth()->toDateTimeString();
        // $startDate=formatDateReverse($startDate);
        // $endDate=formatDateReverse($endDate);


        $view='reports.tables.bytypepharm';
        
        $viewMap=[
            'seller'=>'reports.tables.byreseller',
            'city'=>'reports.tables.bycity',
            'state'=>'reports.tables.bystate',
            'chain'=>'reports.tables.bytypechain',
            'pharm'=>'reports.tables.bytypepharm',
            'promo'=>'reports.tables.bypromo',
            'user'=>'reports.tables.byuser',
        ];
        // dd($request->sortBy);
        if(isset($request->sortBy) && !empty($viewMap[$request->sortBy])){
          $view=$viewMap[$request->sortBy];
        }
        if (isRepresentative(Auth::user())) {
            
            $userId=Auth::user()->id;

            $orders=OrderDetailItem::with(
                [
                'orderDetail.order.user',
                'orderDetail.chain',
                'orderDetail.pharmacy',
                'promotion',
                'package',
                'product',
                'reseller'
                ])
            ->madeBy($userId)
            ->forMonth($startDate,$endDate)
            ->notEmpty()
            ->filter($filters)
            ->get();
            return view('reports.index', compact('orders','view'));

        }

        $orders=OrderDetailItem::with(
            [
            'orderDetail.order.user',
            'orderDetail.chain',
            'orderDetail.pharmacy',
            'promotion',
            'package',
            'product',
            'reseller'
            ])
        ->notEmpty()
        ->forMonth($startDate,$endDate)
        ->filter($filters)
        ->get();

        // ->paginate(10);
        return view('reports.index', compact('orders','view'));
        
    }

    public function export(Request $request)
    {
        $orders=json_decode($request->orders,1);

        $view=str_replace('tables', 'exports', $request->view);
        // return view('reports.export', compact('view','orders'));
        $exportName = uniqid();
        Excel::create($exportName, function($excel) use ($view,$orders) {
            $excel->sheet('Export', function($sheet) use ($view,$orders) {
                $sheet->setfitToWidth(true);
                $sheet->loadView('reports.export', compact('view','orders'));
            });
        })->download('xlsx');
    	
    }
}
