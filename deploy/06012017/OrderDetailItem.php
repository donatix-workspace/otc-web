<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetailItem extends Model
{
    protected $table = 'order_detail_items';
    protected $fillable = [
        'order_detail_id','reseller_id','item_type','package_id' ,'item_id','quantity','rabat'
    ];
    
    // protected $dates = ['start_at', 'end_at'];

    public function scopeGetProducts($query)
    {
        return $query->where('item_type','product');
    }
    public function scopeGetPromos($query)
    {
        return $query->where('item_type','promotion');
    }
    public function scopeGetPackages($query)
    {
        return $query->where('item_type','packadges');
    }
    public function orderDetail()
    {
        return $this->belongsTo('App\OrderDetail');
    }
    public function reseller()
    {
        return $this->belongsTo('App\Reseller');
    }
    
    public function promotion()
    {   
        return $this->belongsTo('App\Promotion','item_id','id');
    }
     public function package()
    {   
        return $this->belongsTo('App\Promotion','package_id','id');
    }
     public function product()
    {   
        return $this->belongsTo('App\ProductViewModel','item_id','id');
    }
    public function scopeFilter($query, $filters)
    {
        return $filters->apply($query);
    }
    public function scopeMadeBy($query,$user_id)
    {   
        return $query->whereHas('orderDetail',function($query) use ($user_id){
            return $query->whereHas('order',function($orderQuery) use ($user_id){
                return $orderQuery->where('user_id',$user_id);
            });
        });
    }
    public function scopeNotEmpty($query)
    {   
        return $query->where('quantity','!=',0)->orWhere('rabat','!=' ,0);
    }
    public function scopeForMonth($query,$startDate,$endDate)
    {   
        
        return $query->whereBetween('created_at', [$startDate,$endDate]);
    }
}


 