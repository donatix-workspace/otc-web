<?php 

return [
		
		'pharm'=>'Аптека',
		'product-type-single'=>'Единичен продукт',
		'product-type-promo'=>'Промоция',
		'product-type-package'=>'Пакет',
		'city'=>'Град',
		'chain'=>'Верига',
		'single'=>'Аптека',
		'pharmacy'=>'Аптека',
		'product'=>'Продукт',
		'user'=>'Представител',
		'total-quantity'=>'Количество',
		'total-rabat'=>'Рабат',
		'reseller'=>'Дистрибутор',
		'product-type'=>'Тип на продукт',


		'date'=>'Дата',
		'date_asc'=>'Възходящ ред',
		'date_desc'=>'Низходящ ред',
		'sort-by'=>'Групиране',

		'search'=>'Търсене',
		'from'=>'От Дата',
		'to'=>'До Дата',
		'venue-type'=>'Тип на обект',
		'venue-id'=>'Обект',
		'bulstat'=>'Булстат',
		'state'=>'Статус',

];