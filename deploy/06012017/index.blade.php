@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-bars"></i></i>
                    {{ trans('general.reports') }}
                    <hr>
                    <form action="{{ url('reports') }}" class="form-inline">
                        <div class="row">
                            
                            <div class="col-md-4">
                                <div class="row">

                                    <div class="col-md-12">
                                        <label for="inputDate_month">{{ trans('reports.month') }}:</label>
                                    	<select name="month" id="inputDate_month" class="form-control">
                                            <option value="{{\Carbon\Carbon::now()->month}}">{{ trans('reports.'.\Carbon\Carbon::now()->format('M')) }}</option>
                                            <option value="1">{{ trans('reports.Jan') }}</option>
                                            <option value="2">{{ trans('reports.Feb') }}</option>
                                            <option value="3">{{ trans('reports.Mar') }}</option>
                                            <option value="4">{{ trans('reports.Apr') }}</option>
                                            <option value="5">{{ trans('reports.May') }}</option>
                                            <option value="6">{{ trans('reports.Jun') }}</option>
                                            <option value="7">{{ trans('reports.Jul') }}</option>
                                            <option value="8">{{ trans('reports.Aug') }}</option>
                                            <option value="9">{{ trans('reports.Sep') }}</option>
                                            <option value="10">{{ trans('reports.Oct') }}</option>
                                            <option value="11">{{ trans('reports.Nov') }}</option>
                                            <option value="12">{{ trans('reports.Dec') }}</option>
                                        </select>
                                        
                                    
                                        <label for="inputDate_year">{{ trans('reports.year') }}:</label>
                                    
                                        <select name="year" id="inputDate_month" class="form-control">
                                            <option value="{{\Carbon\Carbon::now()->year}}">{{\Carbon\Carbon::now()->year}}</option>
                                            <option value="2016">2016</option>
                                        </select>
                                        
                                    </div>
                                </div>
                                <hr>    
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="venue_type">{{ trans('reports.venue-type') }}:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <select name="venue_type" id="inputVenue_type" class="form-control">
                                            <option value="single">{{ trans('reports.single') }}</option>
                                            <option value="chain">{{ trans('reports.chain') }}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="venue_id">{{ trans('reports.venue-id') }}:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" id="venue_id" name="venue_name" class="form-control" value="{{ old('venue_id') }}">
                                    </div>
                                </div>
                                <hr>    
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="promo_type">{{ trans('reports.product-type') }}:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <select name="promo_type" id="inputPromo_type" class="form-control">
                                            <option value=""></option>
                                            <option value="promotion">{{ trans('reports.product-type-promo') }}</option>
                                            <option value="package">{{ trans('reports.product-type-package') }}</option>
                                            <option value="product">{{ trans('reports.product-type-single') }}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="promo_name">{{ trans('reports.product') }}:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" id="promo_name" name="promo_name" class="form-control" value="{{ old('promo_name') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">

                                <div class="row">
                                	<div class="col-md-4">
                                        <label for="inputDate_from">{{ trans('reports.from') }}:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="date" name="date_from" id="inputDate_from" class="form-control datepicker" value="{{\Carbon\Carbon::now()->startOfMonth()->format("Y-m-d")}}" title="From:">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="inputDate_to">{{ trans('reports.to') }}:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="date" name="date_to" id="inputDate_to" class="form-control datepicker" value="{{\Carbon\Carbon::now()->endOfMonth()->format("Y-m-d")}}" title="To:">
                                    </div>
                                </div>
                                <hr>    
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="bulstat">{{ trans('reports.bulstat') }}:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" id="bulstat" name="bulstat" class="form-control" value="{{ old('bulstat') }}">
                                        
                                    </div>
                                </div>
                                <br> 
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="seller_id">{{ trans('reports.reseller') }}:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" id="seller_name" name="seller_name" class="form-control" value="{{ old('seller_name') }}">
                                        
                                    </div>
                                </div>
                                <br>  
                                
                                @if(!isRepresentative(Auth::user()))   
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="user">{{ trans('reports.user') }}:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" id="user" name="user" class="form-control" value="{{ old('bulstat') }}">
                                        
                                    </div>
                                </div>
                                @endif
                            </div>
                            <div class="col-md-4">

                               <div class="row">
                                    <div class="col-md-4">
                                        <label for="sortBy">{{ trans('reports.sort-by') }}:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <select name="sortBy" id="sortBy" class="form-control">
                                            <option value=""></option>
                                            <option value="date_asc">{{ trans('reports.date_asc') }}</option>
                                            <option value="date_desc">{{ trans('reports.date_desc') }}</option>
                                            <option value="seller">{{ trans('reports.reseller') }}</option>
                                            <option value="pharm">{{ trans('reports.pharm') }}</option>
                                            <option value="chain">{{ trans('reports.chain') }}</option>
                                            <option value="city">{{ trans('reports.city') }}</option>
                                            <option value="user">{{ trans('reports.user') }}</option>
                                        </select>
                                        
                                    </div>
                                </div>

                                <hr>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="sortBy">{{ trans('reports.state') }}:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <select name="state" id="state" class="form-control">
                                            <option value=""></option>
                                            <option value="confirmed">{{ trans('orders.confirmed') }}</option>
                                            <option value="declined">{{ trans('orders.declined') }}</option>
                                            <option value="waiting">{{ trans('orders.waiting') }}</option>
                                        </select>
                                        
                                    </div>
                                </div>
                                <br>    
                                <br>    
                                <br>    
                                <div class="row"> 
                                    <div class="col-md-6 col-md-offset-3">
                                        <button type="submit" class="btn btn-lg btn-primary"><i class="fa fa-search" aria-hidden="true"> {{trans('reports.search')}}</i></button>
                                    </div>
                                </div>  
                            </div>
                        </div>
                           
                        </div>
                       
                        
                    </form>
                </div>
               
                <div class="panel-body">
                    <div class="btn-group pull-right">
                        <form class="form-inline" action="{{ url('reports/export') }}" method="POST">
                            <input type="hidden" name="view" value="{{$view}}">

                            <input type="hidden" name="orders" value="{{$orders}}">
                            {{ csrf_field() }}
                            
                            <button class="btn btn-info"><i class="fa fa-download"> {{ trans('promotions.export') }} </i></button>
                        </form>
                    </div>
                    <br>
                    <br>
                    <div>
                        @include($view,$orders)
                    </div>
                </div>
               
            </div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script type="text/javascript">
$(document).ready(function() {

 $(".datepicker").datepicker({dateFormat: "yy-mm-dd"});

//$('#promotions-table').DataTable();
$('#reports-table').dataTable({
"language": {
"lengthMenu": "_MENU_ "+"{{ trans('general.table-display-tail') }}",
"zeroRecords": "{{ trans('general.table-zero-records') }}",
"info": "{{ trans('general.table-page') }}"+" _PAGE_ "+"{{ trans('general.table-from') }}"+" _PAGES_",
"infoEmpty": "{{ trans('general.table-zero-records') }}",
"infoFiltered": "({{ trans('general.table-filtered-from') }} _MAX_ {{ trans('general.table-filtered-records') }})",
"search": "{{ trans('general.search-label') }}:",
"paging": "false",
/*"paginate": {
"first":      "{{ trans('general.table-first') }}",
"last":       "{{ trans('general.table-last') }}",
"next":       "{{ trans('general.table-next') }}",
"previous":   "{{ trans('general.table-previous') }}"
},*/
}
});
$('#orders-history-table').dataTable({
"language": {
"lengthMenu": "_MENU_ "+"{{ trans('general.table-display-tail') }}",
"zeroRecords": "{{ trans('general.table-zero-records') }}",
"info": "{{ trans('general.table-page') }}"+" _PAGE_ "+"{{ trans('general.table-from') }}"+" _PAGES_",
"infoEmpty": "{{ trans('general.table-zero-records') }}",
"infoFiltered": "({{ trans('general.table-filtered-from') }} _MAX_ {{ trans('general.table-filtered-records') }})",
"search": "{{ trans('general.search-label') }}:",
/*"paginate": {
"first":      "{{ trans('general.table-first') }}",
"last":       "{{ trans('general.table-last') }}",
"next":       "{{ trans('general.table-next') }}",
"previous":   "{{ trans('general.table-previous') }}"
},*/
}
});
} );
</script>
@endsection