# Migration guide for CR5 `18.01.2018`
1. copy modified files as follows
    ```
     .gitignore
     app/Console/Commands/MigrateOrderHistoryCommand.php
     app/Console/Kernel.php
     app/Helpers/ViewHelpers.php
     app/Http/Controllers/Api/ReportControllerer.php
     app/Http/Controllers/OrderController.php
     app/Http/Controllers/ReportController.php
     app/Jobs/PopulateOrderHistoryJob.php
     app/Observers/OrderDetailItemObserver.php
     app/Observers/OrderObserver.php
     app/OrderHistory.php
     app/OrderHistoryFilters.php
     app/Providers/AppServiceProvider.php
     composer.json
     composer.lock
     vendor/
     database/migrations/2018_01_09_163240_Alter_promotions.php
     database/migrations/2018_01_10_122544_create_order_histories_table.php
     resources/lang/bg/orders.php
     resources/views/orders/index.blade.php
     resources/views/reports/exports/bycity.blade.php
     resources/views/reports/exports/bypromo.blade.php
     resources/views/reports/exports/byreseller.blade.php
     resources/views/reports/exports/bystate.blade.php
     resources/views/reports/exports/bytypechain.blade.php
     resources/views/reports/exports/bytypepharm.blade.php
     resources/views/reports/exports/byuser.blade.php
     resources/views/reports/index.blade.php
     resources/views/reports/tables/api/bycity.blade.php
     resources/views/reports/tables/api/bypromo.blade.php
     resources/views/reports/tables/api/byreseller.blade.php
     resources/views/reports/tables/api/bystate.blade.php
     resources/views/reports/tables/api/bytypechain.blade.php
     resources/views/reports/tables/api/bytypepharm.blade.php
     resources/views/reports/tables/api/byuser.blade.php
     resources/views/reports/tables/bycity.blade.php
     resources/views/reports/tables/bypromo.blade.php
     resources/views/reports/tables/byreseller.blade.php
     resources/views/reports/tables/bystate.blade.php
     resources/views/reports/tables/bytypechain.blade.php
     resources/views/reports/tables/bytypepharm.blade.php
     resources/views/reports/tables/byuser.blade.php
     ```
2. configure the database for the environment in the `.env`
    + for MSSQL add
        ```
        DBVIEWCONNECTION = 'sqlsrvesales'
        TIMEPOSTFIX = '.000'
        
        ```
    + for MySQL add
       ```
       DBVIEWCONNECTION = 'mysql'
       TIMEPOSTFIX =
       
       ```
3. run `php artisan migrate`
4. run `php artisan orders:migrate` to insert old orders in OrderHistory table for the reports to appear correctly
5. delete blade templates cache