<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderQueue extends Model
{
    protected $table = 'order_queue';
    
    protected $fillable = [
        'client','address','bulstat','item_type','type','item',
        'reseller_id','order_id','order_created_at','manufacturer','city',
        'package_id','quantity','rabat','comment','email','ready_for_send'
    ];

    public function scopeMadeBy($query,$user_id)
    {
        return $query->where('user_id', '=', $user_id);
    }

    public function order()
    {
        return $this->belongsTo('App\Order');
    }

    public function scopeReady($query)
    {
        return $query->where('ready_for_send', 1);
    }

    public function scopeForReseller($query, $resellerId)
    {
        return $query->where('reseller_id', $resellerId);
    }
}
