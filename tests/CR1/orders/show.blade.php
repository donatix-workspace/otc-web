@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-8">
                            <i class="fa fa-bars"></i>
                            {{ trans('orders.order') }} # {{$order->id}} | {{ trans('orders.from') }} {{$order->created_at}} |  {{ trans('orders.send-date') }} {{formatDate($order->send_date?$order->send_date:$order->created_at)}}
                        </div>
                        <div class="col-md-4">
                            <div class="btn-group pull-right">
                                 <form class="form-inline" action="{{ url('orders', $order->id) }}" method="POST">
                                @if (!$order->need_confirmation && !$order->confirmed)
                                    <i>
                                <i class="fa fa-thumbs-o-down" aria-hidden="true"></i>
                                {{ trans('orders.declined') }}
                                    </i>
                                @elseif($order->confirmed)
                                <i><i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                                {{ trans('orders.confirmed') }}</i>
                                @else
                                   
                                    <a href="{{url('orders').'/'.$order->id.'/confirmation'}}" class="btn btn-success btn-sm"><i class="fa fa-check">  {{ trans('orders.accept') }}</i></a>
                                    <a href="{{url('orders').'/'.$order->id.'/decline'}}" class="btn btn-warning btn-sm"><i class="fa fa-times">  {{ trans('orders.decline') }}</i></a>
                                   
                                @endif
                                <input type="hidden" name="_method" value="DELETE">
                                {{ csrf_field() }}
                                @can('approve_orders')    
                                    <button class="btn btn-danger btn-sm"><i class="fa fa-trash"> {{ trans('orders.delete') }}</i></button> 
                                @endcan    
                                </form>
                            </div>
                        </div>
                    </div>
                    
                    
                </div>
                <div class="panel-body">
                    
                    
                    <div class="row">
                        <div class="col-md-12">
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <h3>{{ trans('orders.sales-representative') }}</h3>
                                    <div class="row">
                                        
                                        <div class="col-md-12">
                                            <dl class="dl-horizontal">
                                                <dt>{{ trans('orders.sales-representative-name') }}</dt>
                                                <dd>{{$user->name}}</dd>
                                                <dt>{{ trans('orders.sales-representative-email') }}</dt>
                                                <dd>{{$user->email}}</dd>
                                                <dt>{{ trans('orders.sales-representative-cwid') }}</dt>
                                                <dd>{{$user->cwid}}</dd>
                                            </dl>
                                            
                                            
                                        </div>
                                    </div>
                                    <h3>{{ trans('orders.signiture') }}</h3>
                                    <div class="row">
                                        
                                        <div class="col-md-12">
                                            
                                            <img alt="{{ trans('orders.signiture') }}" src="data:image/jpeg;base64,{{$order->signiture}}" />
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <h3>{{ trans('orders.sales-resellers') }}</h3>
                                    <div class="row">
                                        
                                        <div class="col-md-12">
                                            <dl class="dl-horizontal">
                                                @foreach ($order->resellers as $reseller)
                                                <dt>{{ trans('orders.sales-resselers-name') }}</dt>
                                                <dd>{{resellerName($reseller->reseller_id)}} - {{resellerEmail($reseller->reseller_id)}}</dd>
                                                <dt>{{ trans('orders.sales-resselers-comment') }}</dt>
                                                <dd><p><i>{{$reseller->comment}}</i> </p></dd>
                                                
                                                @endforeach
                                                
                                            </dl>
                                            
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <hr>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <h3>{{ trans('orders.details') }}</h3>
                                    <table id="orders-table" class="display" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>{{ trans('orders.pharmacy') }}</th>
                                                <th>{{ trans('orders.chain') }}</th>
                                                <th>{{ trans('orders.package_id') }}</th>
                                                <th>{{ trans('orders.product-type') }}</th>
                                                <th>{{ trans('orders.product-name') }}</th>
                                                <th>{{ trans('orders.product-quantity') }}</th>
                                                <th>{{ trans('orders.product-rabat') }}</th>
                                                <th>{{ trans('orders.product-reseller') }}</th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            @foreach ($order->details as $detail)
                                            @foreach ($detail->items as $item)
                                            <tr>
                                                <td>{{pharmName($detail->pharm_id)}}</td>
                                                <td>{{$detail->chain['name']}}</td>
                                                <td>{{packageName($item->package_id)}}</td>
                                                <td>{{trans('orders.'.$item->item_type)}}</td>
                                                <td>{{productName($item->item_type,$item->item_id)}}</td>
                                                <td>{{$item->quantity}}</td>
                                                <td>{{$item->rabat}}</td>
                                                <td>{{resellerName($item->reseller_id)}}</td>
                                            </tr>
                                            @endforeach
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <br>
                                </div>
                            </div>
                            <hr>
                        </div>
                    </div>
                    
                </div>
                
                <br>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
@section('javascript')
<script type="text/javascript">
$(document).ready(function() {
//$('#promotions-table').DataTable();
$('#orders-table').dataTable({
"language": { 
    "lengthMenu": "_MENU_ "+"{{ trans('general.table-display-tail') }}",
    "zeroRecords": "{{ trans('general.table-zero-records') }}",
    "info": "{{ trans('general.table-page') }}"+" _PAGE_ "+"{{ trans('general.table-from') }}"+" _PAGES_",
    "infoEmpty": "{{ trans('general.table-zero-records') }}",
    "infoFiltered": "({{ trans('general.table-filtered-from') }} _MAX_ {{ trans('general.table-filtered-records') }})",
    "search": "{{ trans('general.search-label') }}:",
    "paginate": {
        "first":      "{{ trans('general.table-first') }}",
        "last":       "{{ trans('general.table-last') }}",
        "next":       "{{ trans('general.table-next') }}",
        "previous":   "{{ trans('general.table-previous') }}"
    },
}
});
} );
</script>
@endsection