# Bayer OTC Web
Get Started
- install xampp
- install composer
- clone repo
- create database: bayer_otc
- run `php artisan key:generate` in root directory
- rename .env.exampke to .env
- modify DB configs in .env
- run `php artisan migrate`

## Milestones
### 1: Initial Setup - done
### 2: Create Dummy API endpoints
| POST     | api/v1/auth
| GET|HEAD | api/v1/orders
| POST     | api/v1/orders
| GET|HEAD | api/v1/orders/{id}
| POST     | api/v1/push    
| GET|HEAD | api/v1/report 
| POST     | api/v1/sync
### 3: Authentication
TODO: 
- Add token column in migrations
- Generate token
- Make api Authentication
### 4: Roles and Permissions
- Define all roles and permissions
### 5: Read-only Views – MSSQL Data 
### 6: Resselers Implementation
### 7: Promos & Packages Implementation
### 8: Orders
### 9: Orders Queue
### 10: Cronjobs
### 11: Push Notifications
### 12: Reports

# Migration guide for CR5
- run `composer update`
- configure the database for the environment in the `.env`
    + for MSSQL add
        ```
        DBVIEWCONNECTION = 'sqlsrvesales'
        TIMEPOSTFIX = '.000'
        
        ```
    + for MySQL add
       ```
       DBVIEWCONNECTION = 'mysql'
       TIMEPOSTFIX =
       
       ```
- run `php artisan migrate`
- run `php artisan orders:migrate` to insert old orders in OrderHistory table for the reports to appear correctly
- delete blade templates cache
