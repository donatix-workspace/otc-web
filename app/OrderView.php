<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderView extends Model
{
    protected $table = 'v_bayer_otc_orders';
    
    public function scopeMadeBy($query,$user_id)
    {
        return $query->where('user_id', '=', $user_id);
    }
}
