<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderTotal extends Model
{
    protected $table = 'order_total_items';

    protected $fillable = [
        'order_id','type','package_id','package_quantity' ,'item_id','quantity','rabat','reseller_id'
    ];

    public function order()
    {
        return $this->belongsTo('App\Order');
    }

    public function promotion()
    {
        return $this->belongsTo(Promotion::class, 'item_id');
    }

    public function package()
    {
        return $this->belongsTo(Promotion::class, 'package_id');
    }

    public function productView()
    {
        return $this->belongsTo(ProductViewModel::class, 'item_id');
    }

    public function scopeGetByType($query,$type)
    {
        return $query->where('type', $type);
    }

}
