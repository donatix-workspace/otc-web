<?php

namespace App;

use App\Http\Responses\CitiesResponse;
use App\Http\Utilities\Pharmacy;
use App\Promotion;
use App\PharmacyViewModel;
use App\ProductViewModel;
use App\ChainViewModel;
use Carbon\Carbon;


class OrderFilters extends QueryFilters
{
   //  public function __construct() {
   //     //dd('s');
   // }
    public function venueType($type)
    {
        if ($type != 'chain' && $type != 'single') {
            return $this->builder;
        }

        return $this->builder->whereHas('orderDetail',function($query) use ($type){
            return $query->whereHas('order',function($orderQuery) use ($type){
                return $orderQuery->where('type',$type);
            });
        });
    }

    public function venueName($name)
    {   
        $name=trim($name);
        if($this->filters('venue_type') == 'chain'){
            $vanue=ChainViewModel::where('name','like', '%'.$name.'%')->get(['id'])->first();
        }
        else{
            $vanue=PharmacyViewModel::where('name','like', '%'.$name.'%')->get(['id'])->first();
        }
        return $vanue?($this->venueId($vanue->id)):($this->builder);
            
    }
    public function venueId($id)
    {   
        return $this->builder->whereHas('orderDetail', function($query) use ($id) {
            $against = $this->filters('venue_type') == 'chain' ? 'chain_id' : 'pharm_id';
            return $query->where($against, $id);
        });        
    }

    public function dateFrom($date)
    {
        
        $date=Carbon::createFromFormat('Y-m-d', $date)->startOfDay()->toDateTimeString();
        return $this->builder->where('created_at', '>', formatDateReverse($date));
    }

    public function dateTo($date)
    {
        $date=Carbon::createFromFormat('Y-m-d', $date)->endOfDay()->toDateTimeString();
        return $this->builder->where('created_at', '<=',formatDateReverse($date));
    }

    public function bulstat($bulstat)
    {
        $bulstat=trim($bulstat);

        $pharm = PharmacyViewModel::where('bulstat', $bulstat)->first();
        if (is_null($pharm)) return;

        return $this->venueId($pharm->id);
    }
    public function sellerName($sellerName)
    {   
        $sellerName=trim($sellerName);

        $reseller=Reseller::where('name','like','%'.$sellerName.'%')->first();
        if (is_null($reseller)) return;
        return $this->builder->where('reseller_id', $reseller->id);
    }
    public function sellerId($sellerId)
    {   
        return $this->builder->where('reseller_id', $sellerId);
    }

    public function cityId($cityId)
    {
        $city = (new CitiesResponse())->getCity($cityId);
        
        if (is_null($city)) return;

        return $this->builder->whereHas('queue', function($query) use ($city) {
            return $query->where('city', $city['name']);
        });
    }

    public function promotionId($id)
    {   
        $against = Promotion::find($id)->is_package == '1' ? 'package_id' : 'item_id';

        return $this->builder->where($against, $id);
              
    }
    public function productId($id)
    {   
        return $this->builder->where('item_id', $id);
              
    }
    public function promoType($type)
    {
        if ($type != 'promotion' && $type != 'product' && $type != 'package') {
                return $this->builder;
            }

        return $this->builder->where('item_type',$type);
    }
    public function promoName($name)
    {   
        $name=trim($name);

        if($this->filters('promo_type') == 'product'){
            $promo=ProductViewModel::where('name','like', '%'.$name.'%')->get(['id'])->first();
            return $promo?($this->productId($promo->id)):($this->builder);
        }
        $promo=Promotion::where('name','like', '%'.$name.'%')->get(['id'])->first()?:null;
        

        return $promo?($this->promotionId($promo->id)):$this->builder;
              
    }
    public function user($name)
    {   
        $name=trim($name);
        $user=User::where('name','like','%'.$name.'%')->first();
        if (is_null($user)) return;

        return $this->userId($user->id);
              
    }
    public function userId($id)
    {   
        return $this->builder->whereHas('orderDetail', function($query) use ($id) {
            return $query->whereHas('order', function($q) use ($id){
                return $q->where('user_id', $id);
            });
            
        }); 
              
    }

    public function sortBy($criteria)
    {
        $criterias = [
            'date_asc' => 'sortByDateAsc',
            'date_desc' => 'sortByDateDesc',
            'seller' => 'sortBySeller',
            'venue' => 'sortByVenue',
            'city' => 'sortByCity',
        ];

        if ( ! in_array($criteria, array_keys($criterias))) return;
        return $this->$criterias[$criteria]();
    }

    private function sortByDateAsc()
    {
        return $this->builder->orderBy('created_at', 'asc');
    }

    private function sortByDateDesc()
    {   
        return $this->builder->orderBy('created_at', 'desc');
    }
    
    private function sortBySeller()
    {
        return;
    }
    
    private function sortByVenue()
    {
        // return $this->builder->orderBy('created_at', 'asc');
    }
    
    private function sortByCity()
    {
    //     return $this->builder->with(['queue' => function($query) {
    //         return $query->orderBy('id', 'desc');
    //     }]);
    }
    
    public function state($state)
    {
        $states = [
            'confirmed' => 'filterConfirmed',
            'declined' => 'filterDeclined',
            'waiting' => 'filterWaiting',
        ];
        if ( ! in_array($state, array_keys($states))) return;
        return $this->$states[$state]();
        
    }
    private function filterConfirmed()
    {
        return $this->builder->whereHas('orderDetail', function($query) {
            return $query->whereHas('order', function($q){
                return $q->where('confirmed', 1);
            });
            
        }); 
    }
    private function filterDeclined()
    {
        return $this->builder->whereHas('orderDetail', function($query) {
            return $query->whereHas('order', function($q){
                return $q->where('confirmed', 0)->where('need_confirmation', 0);
            });
            
        }); 
    }
    private function filterWaiting()
    {
        return $this->builder->whereHas('orderDetail', function($query) {
            return $query->whereHas('order', function($q){
                return $q->where('confirmed', 0)->where('need_confirmation', 1);
            });
            
        }); 
    }
    
    
}