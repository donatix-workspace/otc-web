<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    use HasRoles;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email','cwid','token', 'password','is_active','phone'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    
    protected $hidden = [
        'password', 'remember_token',
    ];

     public function resellers()
    {
        return $this->belongsToMany('App\Reseller');
    }

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }
     public function scopeActive($query)
    {
        return $query->where('is_active',1);
    }
    public function isActive()
    {
        return $this->is_active;
    }
     
}
