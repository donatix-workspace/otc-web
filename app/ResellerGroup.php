<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResellerGroup extends Model
{
    protected $fillable = [
        'name'
    ];
}
