<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City // extends Model
{
    public static function all()
    {
        $pharms = PharmacyViewModel::groupBy('city')->get();
    
        return $pharms->map(function($pharm) {
            return ['name' => $pharm['city']];
        });
    }
}
