<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderHistoryView extends Model
{
    protected $table = 'v_order_history';
    public $incrementing = false;
    public $timestamps = false;

    public function scopeNotConfirmed($query)
    {
        return $query->where('need_confirmation', 1)->where('confirmed', 0);
    }
    public function scopeConfirmed($query)
    {
        return $query->where('confirmed', 1);
    }
}
