<?php

namespace App\Observers;


use App\Order;
use App\OrderHistory;

class OrderObserver
{
    /**
     * @return void
     */
    public function updated(Order $order)
    {
        OrderHistory::where('order_id', $order->id)
            ->update([
                'need_confirmation' => $order->need_confirmation,
                'confirmed' => $order->confirmed,
            ]);
    }

    /**
     * @return void
     */
    public function deleting(Order $order)
    {
        //
        OrderHistory::where('order_id', $order->id)
            ->delete();
    }
}