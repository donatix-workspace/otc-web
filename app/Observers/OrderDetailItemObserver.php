<?php

namespace App\Observers;


use App\Jobs\PopulateOrderHistoryJob;
use App\OrderDetailItem;
use Illuminate\Bus\Dispatcher;

class OrderDetailItemObserver
{
    /**
     * @param OrderDetailItem $item
     *
     * @return void
     */
    public function created(OrderDetailItem $item)
    {
        app(Dispatcher::class)->dispatch(new PopulateOrderHistoryJob($item));
    }
}