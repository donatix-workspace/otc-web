<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderHistory extends Model
{
    protected $fillable = [
        'order_id'
        ,'order_date'
        ,'order_type'
        ,'city'
        ,'bulstat'
        ,'pharmacy_id'
        ,'pharmacy_name'
        ,'chain_id'
        ,'chain_name'
        ,'item_type'
        ,'item_id'
        ,'item_name'
        ,'package_id'
        ,'quantity'
        ,'rabat'
        ,'user_id'
        ,'user_name'
        ,'reseller_id'
        ,'reseller_name'
        ,'need_confirmation'
        ,'confirmed'
    ];


    public function scopeMadeBy($query,$user_id)
    {
        return $query->where('user_id', '=', $user_id);
    }

    public function scopeFilter($query, $filters)
    {
        return $filters->apply($query);
    }

    public function scopeNotEmpty($query)
    {
        return $query->where('quantity','!=',0)->orWhere('rabat','!=' ,0);
    }
}
