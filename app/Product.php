<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'promotion_id', 'product_id', 'quantity','rabat',
    ];

    public function promotion()
    {
    	return $this->belongsTo(Promotion::class);
    }

    public function productView()
    {
        return $this->belongsTo(ProductViewModel::class, 'product_id');
    }
}
