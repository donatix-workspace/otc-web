<?php

namespace App;


use Carbon\Carbon;

class OrderHistoryFilters extends QueryFilters
{
    public function venueType($type)
    {
        if ($type != 'chain' && $type != 'single') {
            return $this->builder;
        }


        return $this->builder->where('order_type', $type);
    }

    public function venueName($name)
    {
        $name = trim($name);

        if($this->filters('venue_type') == 'chain'){
            return $this->builder->where('chain_name','like', '%'.$name.'%');
        }
        else{
            return $this->builder->where('pharmacy_name','like', '%'.$name.'%');
        }

    }
    public function venueId($id)
    {
        if(!is_array($id)) $id=array(mb_strtolower($id));

        if($this->filters('venue_type') == 'chain'){
            return $this->builder->whereIn('chain_id', $id);
        }
        else{
            return $this->builder->whereIn('pharmacy_id', $id);
        }
    }

    public function dateFrom($date)
    {
        $date=Carbon::createFromFormat('Y-m-d', $date)->startOfDay()->toDateTimeString();
        return $this->builder->where('order_date', '>', formatDateReverse($date));
    }

    public function dateTo($date)
    {
        $date=Carbon::createFromFormat('Y-m-d', $date)->endOfDay()->toDateTimeString();
        return $this->builder->where('order_date', '<=',formatDateReverse($date));
    }

    public function bulstat($bulstat)
    {
        $bulstat=trim($bulstat);

        return $this->builder->where('bulstat', $bulstat);
    }
    public function sellerName($sellerName)
    {
        $sellerName=trim($sellerName);

        return $this->builder->where('reseller_name','like', '%'.$sellerName.'%');
    }
    public function sellerId($sellerId)
    {
        return $this->builder->where('reseller_id', $sellerId);
    }

    public function cityId($cityId)
    {
        $city = (new CitiesResponse())->getCity($cityId);

        if (is_null($city)) return;

        return $this->builder->where('city', $city->name);
    }

    public function promotionId($id)
    {
        $against = Promotion::find($id)->is_package == '1' ? 'package_id' : 'item_id';

        return $this->builder->where($against, $id);

    }
    public function productId($id)
    {
        return $this->builder->where('item_id', $id);

    }
    public function promoType($type)
    {
        if ($type != 'promotion' && $type != 'product' && $type != 'package') {
            return $this->builder;
        }

        return $this->builder->where('item_type',$type);
    }
    public function promoName($name)
    {
        $name=trim($name);

        return $this->builder->where('item_name','like', '%'.$name.'%');
    }
    public function user($name)
    {
        $name=trim($name);

        return $this->builder->where('user_name', $name);
    }
    public function userId($id)
    {
        return $this->builder->where('user_id', $id);
    }

    public function sortBy($criteria)
    {
        $criterias = [
            'date_asc' => 'sortByDateAsc',
            'date_desc' => 'sortByDateDesc',
            'seller' => 'sortBySeller',
            'venue' => 'sortByVenue',
            'city' => 'sortByCity',
        ];

        if ( ! in_array($criteria, array_keys($criterias))) return;
        return $this->{$criterias[$criteria]}();
    }

    private function sortByDateAsc()
    {
        return $this->builder->orderBy('order_date', 'asc');
    }

    private function sortByDateDesc()
    {
        return $this->builder->orderBy('order_date', 'desc');
    }

    private function sortBySeller()
    {
        return $this->builder->orderBy('reseller_name', 'asc');
    }

    private function sortByVenue()
    {
        if($this->filters('venue_type') == 'chain'){
            return $this->builder->orderBy('chain_name', 'asc');
        }
        else{
            return $this->builder->orderBy('pharmacy_name', 'asc');
        }
    }

    private function sortByCity()
    {
        return $this->builder->orderBy('city', 'asc');
    }

    public function state($state)
    {
        $states = [
            'confirmed' => 'filterConfirmed',
            'declined' => 'filterDeclined',
            'waiting' => 'filterWaiting',
        ];
        if ( ! in_array($state, array_keys($states))) return;
        return $this->{$states[$state]}();

    }
    private function filterConfirmed()
    {
        return $this->builder->where('confirmed', 1);
    }
    private function filterDeclined()
    {
        return $this->builder->where('confirmed', 0)->where('need_confirmation', 0);
    }
    private function filterWaiting()
    {
        return $this->builder->where('confirmed', 0)->where('need_confirmation', 1);
    }
}