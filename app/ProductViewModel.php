<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductViewModel extends Model
{
    protected $table = 'v_bayer_otc_products';
    public $incrementing = false;

    protected $guarded = ['id'];

    public $timestamps = false;

    public function __construct()
    {
        $this->connection = config('app.dbviewconnections');
    }
}
