<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
	protected $table = 'order_details';
    protected $fillable = [
        'order_id','pharm_id' ,'chain_id'
    ];

    public function order()
    {
        return $this->belongsTo('App\Order');
    }

    public function items()
    {
        return $this->hasMany('App\OrderDetailItem');
    }
    public function pharmacy()
    {   
        return $this->belongsTo('App\PharmacyViewModel','pharm_id','id');
    }
    public function chain()
    {   
        return $this->belongsTo('App\ChainViewModel','chain_id','id');
    }
    protected $dates = ['created_at', 'updated_at'];
}
