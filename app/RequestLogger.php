<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestLogger extends Model
{

	protected $table = 'requests';
   	protected $fillable = [
        'user_id','action','method','raw'
    ];
}
