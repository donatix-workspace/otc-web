<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderNotify extends Model
{
    protected $table = 'order_notify';
    
    protected $fillable = [
        'order_id'
    ];
}
