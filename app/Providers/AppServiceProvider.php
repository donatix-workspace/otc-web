<?php

namespace App\Providers;

use App\Observers\OrderDetailItemObserver;
use App\Observers\OrderObserver;
use App\Order;
use App\OrderDetailItem;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        OrderDetailItem::observe(OrderDetailItemObserver::class);
        Order::observe(OrderObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
