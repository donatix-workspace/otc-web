<?php

namespace App;

use App\PharmacyViewModel;
use Illuminate\Database\Eloquent\Model;

class ChainViewModel extends Model
{
    protected $table = 'v_bayer_otc_chains';
    
    public $incrementing = false;

    protected $guarded = ['id'];

    public $timestamps = false;

    public function __construct()
    {
        $this->connection = config('app.dbviewconnections');
    }

    public function pharmacies()
    {
        return $this->hasMany(PharmacyViewModel::class, 'chain_id', 'id');
    }
}
