<?php
namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

abstract class QueryFilters
{
    /**
     * The request object.
     *
     * @var Request
     */
    protected $request;
    /**
     * The builder instance.
     *
     * @var Builder
     */
    protected $builder;
    /**
     * Create a new QueryFilters instance.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }
    /**
     * Apply the filters to the builder.
     *
     * @param  Builder $builder
     * @return Builder
     */
    public function apply(Builder $builder)
    {
        $this->builder = $builder;

        $this->filters()->each(function($value, $name) {
            $name = $this->toMethodName($name);
            if ( ! method_exists($this, $name)) return;

            if ( ! trim($value)) return;

            return $this->$name($value);
        });

        return $this->builder;
    }
    private function toMethodName($string)
    {
        return preg_replace_callback('/_([a-z])/', function($matches) {
            return strtoupper($matches[1]);
        }, $string);
    }

    /**
     * Get all request filters data.
     *
     * @return array
     */
    public function filters($prop = null)
    {
        if ( ! is_null($prop)) {
            return $this->request->get($prop);
        }

        return collect($this->request->all());
    }
}
