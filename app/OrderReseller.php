<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderReseller extends Model
{
    protected $table = 'order_resellers';
    protected $fillable = [
        'order_id','reseller_id' ,'comment','is_global'
    ];

    public function order()
    {
        return $this->belongsTo('App\Order');
    }
    
    public function reseller()
    {
        return $this->belongsTo('App\Reseller', 'reseller_id');
    }
}
	