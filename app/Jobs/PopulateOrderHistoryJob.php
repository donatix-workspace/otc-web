<?php

namespace App\Jobs;


use App\OrderDetailItem;
use App\OrderHistory;
use Illuminate\Support\Facades\Log;

class PopulateOrderHistoryJob extends Job
{
    private $orderDetailItem;

    /**
     * Create a new job instance.
     *
     * @param OrderDetailItem $orderDetailItem
     * @return PopulateOrderHistoryJob
     */
    public function __construct(OrderDetailItem $orderDetailItem)
    {
        $this->orderDetailItem = $orderDetailItem;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->orderDetailItem->load('product','reseller','orderDetail.pharmacy','orderDetail.chain','orderDetail.order.user');

        $itemName = NULL;

        if($this->orderDetailItem == NULL) dd('problem: '.$this->orderDetailItem->id);

        if($this->orderDetailItem->item_type == 'packadge') {
            $this->orderDetailItem->load('packadge');
            $itemName = $this->orderDetailItem->package->name;
        }
        else if($this->orderDetailItem->item_type == 'promotion') {
            $this->orderDetailItem->load('promotion');
            $itemName = $this->orderDetailItem->promotion->name;
        }
        else if($this->orderDetailItem->item_type == 'product' && $this->orderDetailItem->product != NULL) {
            $itemName = $this->orderDetailItem->product->name;
        }

        $parameterMap = collect([
            'order_id' => $this->orderDetailItem->orderDetail->order_id,
            'order_date' => $this->orderDetailItem->orderDetail->order->send_date ?: $this->orderDetailItem->orderDetail->order->created_at,
            'order_type' => $this->orderDetailItem->orderDetail->order->type,
            'city' => $this->orderDetailItem->orderDetail->pharmacy->city,
            'bulstat' => $this->orderDetailItem->orderDetail->pharmacy->bulstat,
            'pharmacy_id' => $this->orderDetailItem->orderDetail->pharmacy->id,
            'pharmacy_name' => $this->orderDetailItem->orderDetail->pharmacy->name,
            'item_type' => $this->orderDetailItem->item_type,
            'item_id' => $this->orderDetailItem->item_id,
            'item_name' => $itemName,
            'quantity' => $this->orderDetailItem->quantity,
            'rabat' => $this->orderDetailItem->rabat,
            'user_id' => $this->orderDetailItem->orderDetail->order->user->id,
            'user_name' => $this->orderDetailItem->orderDetail->order->user->name,
            'reseller_id' => $this->orderDetailItem->reseller->id,
            'reseller_name' => $this->orderDetailItem->reseller->name,
            'need_confirmation' => $this->orderDetailItem->orderDetail->order->need_confirmation,
            'confirmed' => $this->orderDetailItem->orderDetail->order->confirmed,
        ]);

        if ($this->orderDetailItem->orderDetail->chain != NULL) {
            $parameterMap->put('chain_id', $this->orderDetailItem->orderDetail->chain->id);
            $parameterMap->put('chain_name', $this->orderDetailItem->orderDetail->chain->name);
        }

        if ($this->orderDetailItem->package_id != NULL) {
            $parameterMap->put('package_id', $this->orderDetailItem->package_id);
        }
        OrderHistory::create($parameterMap->toArray());

    }
}
