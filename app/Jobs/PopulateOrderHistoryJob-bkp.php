<?php

namespace App\Jobs;


use App\OrderDetailItem;
use App\OrderHistory;
use Illuminate\Support\Facades\Log;

class PopulateOrderHistoryJob extends Job
{
    private $orderDetailItem;

    /**
     * Create a new job instance.
     *
     * @param OrderDetailItem $orderDetailItem
     * @return PopulateOrderHistoryJob
     */
    public function __construct(OrderDetailItem $orderDetailItem)
    {
        $this->orderDetailItem = $orderDetailItem;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $orderDetailItemId = $this->orderDetailItem->id;

        $orderDetailItem = $this
            ->orderDetailItem
            ->with([
                'promotion' => function($query) {
                    $query
                        ->withTrashed();
                },
                'package' => function($query) {
                    $query
                        ->withTrashed();
                },
                'product',
                'reseller',
                'orderDetail' => function($query) {
                    $query
                        ->with([
                            'pharmacy',
                            'chain',
                            'order' => function($query) {
                                $query
                                    ->with(['user']);
                            }
                        ]);
                }
            ])
            ->find($orderDetailItemId);

        $itemName = NULL;

        if($orderDetailItem == NULL) dd('problem: '.$orderDetailItemId);

        if($orderDetailItem->item_type == 'packadge') {
            $itemName = $orderDetailItem->package->name;
        }
        else if($orderDetailItem->item_type == 'promotion') {
            $itemName = $orderDetailItem->promotion->name;
        }
        else if($orderDetailItem->item_type == 'product' && $orderDetailItem->product != NULL) {
            $itemName = $orderDetailItem->product->name;
        }


        $parameterMap = collect([
            'order_id' => $orderDetailItem->orderDetail->order->id,
            'order_date' => $orderDetailItem->orderDetail->order->send_date ?: $orderDetailItem->orderDetail->order->created_at,
            'order_type' => $orderDetailItem->orderDetail->order->type,
            'city' => $orderDetailItem->orderDetail->pharmacy ? $orderDetailItem->orderDetail->pharmacy->city : '',
            'bulstat' => $orderDetailItem->orderDetail->pharmacy ? $orderDetailItem->orderDetail->pharmacy->bulstat : '',
            'pharmacy_id' => $orderDetailItem->orderDetail->pharmacy ? $orderDetailItem->orderDetail->pharmacy->id : '',
            'pharmacy_name' => $orderDetailItem->orderDetail->pharmacy ? $orderDetailItem->orderDetail->pharmacy->name : '',
            'item_type' => $orderDetailItem->item_type,
            'item_id' => $orderDetailItem->item_id,
            'item_name' => $itemName,
            'quantity' => $orderDetailItem->quantity,
            'rabat' => $orderDetailItem->rabat,
            'user_id' => $orderDetailItem->orderDetail->order->user->id,
            'user_name' => $orderDetailItem->orderDetail->order->user->name,
            'reseller_id' => $orderDetailItem->reseller->id,
            'reseller_name' => $orderDetailItem->reseller->name,
            'need_confirmation' => $orderDetailItem->orderDetail->order->need_confirmation,
            'confirmed' => $orderDetailItem->orderDetail->order->confirmed,
        ]);

        if ($orderDetailItem->orderDetail->chain != NULL) {
            $parameterMap->put('chain_id', $orderDetailItem->orderDetail->chain->id);
            $parameterMap->put('chain_name', $orderDetailItem->orderDetail->chain->name);
        }

        if ($orderDetailItem->package_id != NULL) {
            $parameterMap->put('package_id', $orderDetailItem->package_id);
        }

        OrderHistory::create($parameterMap->toArray());
    }
}