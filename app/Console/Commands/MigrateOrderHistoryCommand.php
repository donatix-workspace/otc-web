<?php

namespace App\Console\Commands;


use App\Jobs\PopulateOrderHistoryJob;
use App\Order;
use App\OrderDetailItem;
use App\OrderHistory;
use Illuminate\Bus\Dispatcher;
use Illuminate\Console\Command;

class MigrateOrderHistoryCommand  extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'orders:migrate';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate orders from the Order table to the OrderHistory table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $alreadyCached = OrderHistory::select(['order_id'])
            ->groupBy('order_id')
            ->get()
            ->pluck('order_id')
            ->toArray();

        $orderItems = OrderDetailItem::query()
            ->leftJoin('order_details', 'order_detail_items.order_detail_id','=', 'order_details.id')
            ->whereNotIn('order_details.order_id', $alreadyCached)
            ->select('order_detail_items.*')
            ->get();

        foreach ($orderItems as $orderItem) {
            app(Dispatcher::class)->dispatchNow(new PopulateOrderHistoryJob($orderItem));
        }
        dump('Migration successful!');
    }
}