<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Mail;
use App\User;
use App\OrderNotify;

class NotificationOrderCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'orders:notify';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Notification Mail to Promotion Approvers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $orders=OrderNotify::all();

        if ($orders->count()>0) {
           
            $promoApprovers=User::with('roles')->get();

            $promoApprovers=$promoApprovers->map(function($user){
                if ($user->hasRole('promotion_approver')) {
                    return $user;
                }
            });
            $promoApprovers = $promoApprovers->filter(function ($item) {
                if ($item != NULL) {
                    return $item;
                }
            });

        }
        if(!$promoApprovers) return ;
        
        Mail::send('emails.notifications', compact('orders'), function($m) use ($promoApprovers) {
            $m->from(env('MAIL_FROM'), env('MAIL_FROM_NAME'));
            $m->subject('New Waiting Orders');
            foreach ($promoApprovers as $approver) {
                $m->to($approver->email);
            }

        });
        $orderIds=$orders->map(function($order){
            return $order->order_id;
        })->toArray();

        if (count(Mail::failures()) == 0) {
             OrderNotify::wherein('order_id',$orderIds)->delete();
         } 

        
    }
}
