<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Mail;
use Excel;
use Log;
use App\Promotion;
use App\OrderQueue;

class SubmitOrderCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'orders:submit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate and Send Orders to Resellers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {   
        $resellers = OrderQueue::ready()->groupBy(['reseller_id','email'])->get(['reseller_id','email']);
        foreach ($resellers as $reseller) {

            $orders = OrderQueue::ready()->forReseller($reseller->reseller_id)->get();

            
            $orders = $orders->groupBy('type')->map(function($type) {
                return $type->groupBy(function ($item, $key) {
                    return $item->item_type == 'package' ? 'package' : 'promotion';
                });
            });
            $groupedOrders = [];


            if ($orders->get('single')) {
                $groupedOrders['totalsPromoForSinglePharms'] = $orders->get('single')->get('promotion') ?: collect([]);
                $groupedOrders['totalsPackageForSinglePharms'] = $orders->get('single')->get('package') ?: collect([]);
            }


            if ($orders->get('chain')) {
               $groupedOrders['totalsPromoForChains'] = $orders->get('chain')->get('promotion') ?: collect([]);
                $groupedOrders['totalsPackageForChains'] = $orders->get('chain')->get('package') ?: collect([]);
            }
            

            $packages = OrderQueue::distinct()->ready()->forReseller($reseller->reseller_id)
                            ->where('package_id','!=',0)->groupBy('package_id')->get(['package_id']);
           

            $packageIds = $packages->pluck('package_id');
            $groupedOrders['packagesArray'] = Promotion::whereIn('id', $packageIds)->with('products')->get();
            // dd($groupedOrders);
            $exportName = uniqid();
            Excel::create($exportName, function($excel) use ($groupedOrders) {
                $excel->sheet('Daily Orders', function($sheet) use ($groupedOrders) {
                    $sheet->loadView('emails.orders', compact('groupedOrders'));
                });
            })->store('xlsx');
            //Log::debug($this->send($groupedOrders, $from, $fromName, $to));
            // if ($this->send($groupedOrders, env('MAIL_FROM'), env('MAIL_FROM_NAME'), $reseller->email)) {
            //     OrderQueue::ready()->forReseller($reseller->reseller_id)->delete();
            // }
            $mailSent = $this->sendOrderMail(
                storage_path('exports\\'.$exportName.'.xlsx'), 
                env('MAIL_FROM'), env('MAIL_FROM_NAME'), $reseller->email
            );

            if ($mailSent) {
                OrderQueue::ready()->forReseller($reseller->reseller_id)->delete();
            }
        }
    }

    
    public function sendOrderMail($pathToFile, $from, $fromName, $to)
    {
        Mail::send('emails.test', [], function($m) use ($pathToFile, $from, $fromName, $to) {
            $m->from($from, $fromName);
            $m->to($to)->cc(env('MAIL_FROM'))->subject('Байер България ЕООД - Заявка поръчки');
            $m->attach($pathToFile);
        });

        return count(Mail::failures()) == 0;
    }
}
