<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PharmacyViewModel extends Model
{
    protected $table = 'v_bayer_otc_pharmacies';

    public $incrementing = false;
    protected $guarded = ['id'];

    public $timestamps = false;

    public function __construct()
    {
        $this->connection = config('app.dbviewconnections');
    }

    public function scopeAoDeleted($query)
    {
        return $query->where('ao_deleted', '!=',1);
    }
    public function setUserId($userId)
    {
        $this->user_id = $userId;
        return $this;
    }
    public function setCwidId($cwid)
    {
        $this->cwid = $cwid;
        return $this;
    }
}
