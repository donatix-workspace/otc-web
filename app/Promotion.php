<?php

namespace App;

use App\Product;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Promotion extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name','is_package', 'start_at', 'end_at','is_private'
    ];

    protected $dates = ['start_at', 'end_at','deleted_at'];

    public function setStartAtAttribute($date){

        $this->attributes['start_at']= Carbon::createFromFormat('Y-m-d H:i:s',$date);
    }
    public function setEndAtAttribute($date){

        $this->attributes['end_at']= Carbon::createFromFormat('Y-m-d H:i:s',$date);
    }
    public function products()
    {
        return $this->hasMany('App\Product');
    }
    public function scopeAfter($query,$lastUpdate)
    {
        return $query->where('updated_at', '>', formatDateReverse($lastUpdate));
    }
    public function scopeActive($query)
    {
        $now=formatDateReverse(Carbon::now());
        return $query->where('start_at', '<', $now)->where('end_at', '>', $now);
    }
    public function scopeIsPromo($query)
    {
        return $query->where('is_package',0);
    }
    public function scopeIsPackage($query)
    {
        return $query->where('is_package',1);
    }

    public function saveProducts($products)
    {
        $products = collect($products)->map(function($product, $index) {
            return new Product([
                'product_id' => $index,
                'quantity' => $product['quantity'],
                'rabat' => $product['rabat'],
            ]);
        });

        $this->products()->saveMany($products);
    }

    public function setQuantity()
    {
        $this->product_quantity = $this->products[0]['quantity'];
        return $this;
    }

    public function setRabat()
    {
        $this->rabat = $this->products[0]['rabat'];
        return $this;
    }
    public function users()
    {
        return $this->belongsToMany('App\User');

    }
    

}
