<?php 

namespace App\Http\Transformers	;

abstract class Transformer 
{
    /**
     * summary
     */
    public function transformCollection($items)
    {
        if (is_array($items)) {
            return array_map([$this,'transform'], $items);
        }

        return $items->map([$this, 'transform']);
    }

    public abstract function transform($item);
}