<?php 

namespace App\Http\Transformers	;

class ProductTransformer extends Transformer
{
	
	public function transform($product)
    {
		return [
			'id' => $product['id'],
            'name' =>  $product['name'],
            'deleted' => (boolean) $product['is_deleted']
		];
	}
}