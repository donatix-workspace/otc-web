<?php 

namespace App\Http\Transformers	;

class ResellerTransformer extends Transformer
{

	public function transform($reseller)
    {
		return [
			'id' =>(int) $reseller['id'],
            'name' =>  $reseller['name'],
            'email' =>  $reseller['email'],
            'photo'=> url('/') . env('FILE_UPLOAD_DIRECTORY', '/uploaded/images/') . $reseller['image'],
            'deleted' => ! is_null($reseller['deleted_at'])
		];
	}
}
