<?php 

namespace App\Http\Transformers	;

class PharmacyTransformer extends Transformer
{

    public function transform($pharmacy)
    {
        
        return [
            'id' => $pharmacy['id'],
            'name' => isset($pharmacy['name'])?$pharmacy['name']:'',
            'user_id' => $pharmacy['user_id'],
            // 'is_owned'=>isset($pharmacy['cwid']),
            'is_owned'=>$pharmacy['owner_id']==$pharmacy['cwid'],
            'city' => isset($pharmacy['city']) ? $pharmacy['city'] : '',
            'address' => $pharmacy['address'],
            'pid' => isset($pharmacy['bulstat']) ? $pharmacy['bulstat'] : '',
            'deleted' => (boolean) $pharmacy['is_deleted'],
        ];
    }
}