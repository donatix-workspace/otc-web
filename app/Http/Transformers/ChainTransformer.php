<?php 

namespace App\Http\Transformers	;

class ChainTransformer extends Transformer
{
	
	public function transform($chain)
    {
    	$pharms = array_filter($chain['pharmacies'],function($pharm){
    		if ($pharm['ao_deleted'] != 1) {
            			return $pharm['id'];
            		}
    	});
		return [
			'id' => $chain['id'],
            'name' => $chain['name'],
            'deleted' => !(boolean) $chain['is_deleted'],
            'pharmacies' => array_values(array_unique(array_map(function($pharm){

            		return $pharm['id'];
                    /*if ($pharm['ao_deleted'] != 1) {
            			return $pharm['id'];
            		}
            		else{
            			unset($pharm['id']);
            		}*/
                }, $pharms)))
		];
	}
}