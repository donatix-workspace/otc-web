<?php 

namespace App\Http\Transformers	;

class PackageGuiTransformer extends Transformer
{
    
    public function transform($package)
    {
        return [
            'id' => (int) $package['id'],
            'name' => $package['name'],
            'start_at' => $package['start_at'],
            'end_at' => $package['end_at'],
            'deleted' => ! is_null($package['deleted_at']),
            'is_private' => $package['is_private'],
            'products' => (new PackagesProductTransformer)->transformCollection($package['products']),
            'users' => (new PackagesUserTransformer)->transformCollection($package['users']),
        ];
    }
}