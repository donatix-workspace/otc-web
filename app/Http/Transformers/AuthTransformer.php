<?php 

namespace App\Http\Transformers;

class AuthTransformer extends Transformer
{
	public function transform($user)
    {
        return [
            'token' => $user['token'],
            'user' => [
                'fullname' => $user['name'],
                'id' => $user['id'],
                'email' => $user['email']
            ],
       ];
    }
}