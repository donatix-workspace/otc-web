<?php 

namespace App\Http\Transformers	;

class CityTransformer extends Transformer
{

    public function transform($city)
    {
        return [
            'id' => (int) $city['id'],
            'name' => $city['name'],
        ];
    }
}