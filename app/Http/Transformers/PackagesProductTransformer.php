<?php 

namespace App\Http\Transformers	;

class PackagesProductTransformer extends Transformer
{

	public function transform($product)	
    {
		return [
			'id' => $product['product_id'],
            'name' => $product['product_view']['name'],
            'rabat'=> (int) $product['rabat'],
            'product_quantity' => (int) $product['quantity'],
            'deleted' => false,
		];
	}
}
