<?php 

namespace App\Http\Transformers ;

class SingleOrderTransformer extends Transformer
{

    public function transform($order)
    {
        $totals = $order->totals->groupBy('reseller_id')->map(function($reseller) {
            return $reseller->groupBy('type');
        });

        $resellers = $order->resellers->map(function($reseller) use ($totals) {
            $groups = $totals[$reseller['reseller_id']];

            return [
                'seller_name' => $reseller['reseller']['name'],
                'seller_id' => $reseller['reseller_id'],
                'comment' => $reseller['comment'],

                'products' => $this->extractProducts($groups->get('product')),
                'packages' => $this->extractPackages($groups->get('package')),
                'promotions' => $this->extractPromotions($groups->get('promotion')),
            ];
        })->toArray();
            
        return [
            'venue_name' => $order['venue_name'],
            'is_chain' => $order['type'] == 'chain',
            'sellers' => $resellers,
        ];
    }

    private function extractProducts($products)
    {
        if (is_null($products)) return [];

        return $products->map(function($product) {
            return [
                'name' => $product['productView']['name'],
                'quantity' => (int) $product['quantity'], 
                'rabat' => (int) $product['rabat'], 
            ];
        });
    }

    private function extractPackages($packages)
    {
        if (is_null($packages)) return [];

        return $packages->map(function($package) {
            return [
                'name' => $package['package']['name'], 
                'quantity' => (int) $package['package_quantity'], 
            ];
        });
    }

    private function extractPromotions($promotions)
    {
        if (is_null($promotions)) return [];

        return $promotions->map(function($promotion) {

            $productQuantity = (int)$promotion['promotion']['products'][0]['quantity'];
            if ($productQuantity == 0   ) {
                $productQuantity = 1;
            }
            $quantity=((int) $promotion['quantity'] )/ $productQuantity;
            return [

                'name' => $promotion['promotion']['name'], 
                'quantity' => (int) $quantity, 
                'rabat' => (int) $promotion['rabat'], 
            ];
        });
    }
}
