<?php 

namespace App\Http\Transformers	;

class PendingOrderTransformer extends Transformer
{

    public function transform($order)
    {

        $pharm_name=$this->getPharmName($order['details']);

        if (is_null($pharm_name)) {
            return '';
        }
        return [
            'order_id'  =>  $order['id'],
            'type' => $order['type'],
            'pharm_name' =>$pharm_name,
            'date_created' => date('Y-m-d H:i:s O', strtotime($order['created_at'])),
        ];
    }
    
    private function getPharmName($details)
    {
        if (is_null($details)) {
            return '';
        } 

        if (count($details) > 1) {
            return $details[0]['chain']['name']?$details[0]['chain']['name']:'';
        }

        return $details[0]['pharmacy']['name']?$details[0]['pharmacy']['name']:'';
    }
}
