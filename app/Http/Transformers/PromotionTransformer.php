<?php 

namespace App\Http\Transformers ;

class PromotionTransformer extends Transformer
{

    public function transform($promo)
    {
        return [
            'id' => '' . $promo['id'],
            'name' => $promo['name'],
            'rabat' => (int) $promo['rabat'],
            'product_quantity' => (int) $promo['product_quantity'],
            'deleted' => (! is_null($promo['deleted_at'])) || (!isPromoActive($promo['start_at'],$promo['end_at'])),
            //'deleted' => ! is_null($promo['deleted_at']),
        ];
    }
}
