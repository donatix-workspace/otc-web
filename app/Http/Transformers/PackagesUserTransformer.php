<?php 

namespace App\Http\Transformers ;

class PackagesUserTransformer extends Transformer
{

    public function transform($user)    
    {
        return [
            'id' => $user['id'],
            'name' => $user['name'],
        ];
    }
}
