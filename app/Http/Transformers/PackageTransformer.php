<?php 

namespace App\Http\Transformers	;

class PackageTransformer extends Transformer
{

    public function transform($package)
    {
        return [
            'id' => (int)$package['id'],
            'name' =>  $package['name'],
            //'deleted' => ! is_null($package['deleted_at']),
            'deleted' => (! is_null($package['deleted_at'])) || (!isPromoActive($package['start_at'],$package['end_at'])),
            'products'=> (new PackagesProductTransformer)->transformCollection($package['products']),
        ];
    }
}