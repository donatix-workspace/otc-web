<?php 

namespace App\Http\Handlers;

use App\Order;
use App\OrderDetail;
use App\OrderDetailItem;
use App\OrderQueue;
use App\OrderPriority;
use App\OrderReseller;
use App\OrderTotal;
use App\OrderNotify;
use App\PharmacyViewModel;
use App\Product;
use App\Promotion;
use Mail;
use Excel;

class OrderHandler
{
	private $resellers;
	private $signature;
	private $user_id;
	private $pharmacies;
	private $total;
	public $is_priority;

	private $order;
	private $confirmation;
	public $need_confirmation;
	private $globalResellerId;
	
	
	function __construct($jsonObject)
	{	

		$this->resellers=$jsonObject->sellers;
		$this->signature=isset($jsonObject->signature)?$jsonObject->signature:'';
		// $this->signature=$jsonObject->signature;
		$this->user_id=$jsonObject->user_id;
		$this->pharmacies=$jsonObject->pharmacies;
		$this->total=$jsonObject->total;
		$this->is_priority=false;
		if (isset($jsonObject->is_priority)) {
			$this->is_priority=$jsonObject->is_priority;
		}
		
	}
	public function deleteOrder(){

		$this->order->delete();
		
	}
	public function createOrder(){


		$createParams = array(
			'user_id'=>$this->user_id,
			'type'=>$this->getType(),
            'is_priority'=>$this->is_priority,
            'need_confirmation'=>0,
            'confirmed'=>0,
            'global_reseller_id'=>$this->getGlobalReseller(),
            'signiture'=>isset($this->signature)?str_replace(PHP_EOL, '', $this->signature):'',
        );
		$this->order=Order::create($createParams);
		
	}

	public function createOrderDetails(){

		foreach ($this->pharmacies as $pharmacy) {
			$orderDetail = OrderDetail::create(
					array(
				'order_id'=>$this->order->id,
				'pharm_id'=>$pharmacy->pharmacy_id,
				'chain_id'=>isset($pharmacy->chain_id)?$pharmacy->chain_id:'',
			));
			$orderDetail->items()->saveMany($this->createOrderDetailItemsParams($pharmacy->items));
		}

	}
	public function createOrderTotals(){

		$totals=array_merge($this->generateProductTotals(),$this->generatePromoTotals(),$this->generatePackageTotals());
		$this->order->totals()->saveMany($totals);

	}

	public function generateProductTotals(){

		$productsArray=[];
		if (!isset($this->total->products)) {
			return $productsArray;
		}

		foreach ($this->total->products as $product) {
			$productsArray[]=new OrderTotal([
				'type'=>'product',
				'package_id'=>0,
				'package_quantity'=>0 ,
				'item_id'=>$product->id,
				'quantity'=>$product->quantity,
				'rabat'=>$product->rabat,
				'reseller_id'=>isset($product->seller_id)?$product->seller_id:$this->globalResellerId,

			]);
		}
		return $productsArray;
	}

	public function generatePromoTotals(){
			$promoArray=[];
		if (!isset($this->total->promotions)) {
			return $promoArray;
		}
		foreach ($this->total->promotions as $promotion) {
			$promoArray[]=new OrderTotal([
				'type'=>'promotion',
				'package_id'=>0,
				'package_quantity'=>0 ,
				'item_id'=>$promotion->id,
				'quantity'=>$promotion->quantity,
				'rabat'=>$promotion->rabat,
				'reseller_id'=>isset($promotion->seller_id)?$promotion->seller_id:$this->globalResellerId,

			]);
		}
		return $promoArray;
	}
	public function generatePackageTotals(){

		$packageArray=[];
		if (!isset($this->total->packadges) ) {
			return $packageArray;
		}
		foreach ($this->total->packadges as $packadge) {
			if (!isset($packadge->products)) {
			return $packageArray;
		}
			foreach ($packadge->products as $product) {
				$packageArray[]=new OrderTotal([
				'type'=>'packadge',
				'package_id'=>$packadge->id,
				'package_quantity'=>$packadge->quantity ,
				'item_id'=>$product->id,
				'quantity'=>$product->quantity,
				'rabat'=>$product->rabat,
				'reseller_id'=>isset($packadge->seller_id)?$packadge->seller_id:$this->globalResellerId,

				]);
			}
			
		}
		return $packageArray;
	}

	
	public function createOrderDetailItemsParams($items){

		$createParams=array();
		foreach ($items->products as $product) {
			$createParams[]=$this->generateOrderDetails('product',$product);
		}
		foreach ($items->promotions as $promotion) {
			$createParams[]=$this->generateOrderDetails('promotion',$promotion);
		}
		$createPackageParams=[];
		foreach ($items->packadges as $packadge) {
			$createPackageParams=$this->generatePackageOrderDetails('packadge',$packadge);
		}
		return array_merge($createParams,$createPackageParams);
	}
	
    public function createOrderResellers()
    {
		$resellers = array_map(function($reseller) {
            return new OrderReseller([
                'reseller_id' => $reseller->seller_id,
                'comment' => isset($reseller->comment) ? $reseller->comment : '',
                'is_global' => $reseller->is_global
            ]);
        }, $this->resellers);
		
        $this->order->resellers()->saveMany($resellers);
	}

	public function generateOrderDetails($type,$item){

		$params = array(
			'reseller_id' => $this->getItemReseller($item), 
			'item_type' => $type, 
			'item_id' => $item->id, 
			'package_id' => 0, 
			'quantity' => $item->quantity, 
			'rabat' => isset($item->rabat)?$item->rabat:0, 
		);
		return new OrderDetailItem($params);
	}
	public function generatePackageOrderDetails($type,$package){
		$params=array();
		if (!isset($package->products)) {
			return $params;
		}
		foreach ($package->products as $product) {
			$params[]=new OrderDetailItem(
				array(
					'reseller_id' => $this->getItemReseller($package), 
					'item_type' => $type, 
					'item_id' => $product->id, 
					'package_id' => $package->id, 
					'quantity' => $product->quantity, 
					'rabat' => isset($product->rabat)?$product->rabat:0, 
				));
		}
		return $params;
	}


	public function getItemReseller($item){

		if (!isset($item->seller_id)) {
			return $this->getGlobalReseller();
		}
		return $item->seller_id;
		
	}

	public function getGlobalReseller(){

		foreach ($this->resellers as $reseller) {
			if ($reseller->is_global) {
				$this->globalResellerId=$reseller->seller_id;
				return $reseller->seller_id;
			}
		}
	}
	public function getType(){

		if (count($this->pharmacies)==1) {
			return 'single';
		}
		return 'chain';
	
	}


	public function updateConfirmation(){

		$this->need_confirmation=$this->checkConfirmation();
		$confirmed=1;

		if ($this->need_confirmation) {
			$confirmed=0;
		}
		

		$this->order->need_confirmation=$this->need_confirmation;
		$this->order->confirmed=$confirmed;
		$this->order->save();


            
	}
	public function checkConfirmation(){

		$totals=OrderTotal::where('order_id',$this->order->id)->get();

		foreach ($totals as $total) {
            if ($total->type == 'product') {
                if ($total->rabat != 0) {
                    return true;
                }
            } else {
                $id = $total->type == 'promotion' ? $total->item_id : $total->package_id;
                $entity = Promotion::with('products')->findOrFail($id);

                foreach ($entity->products as $product) {
                	if ($product->quantity != 0 && $product->rabat !=0 ) {
	                	if (($total->quantity % $product->quantity != 0) ||($total->rabat % $product->rabat != 0)) {
	                       return true;
	                    }
                	}
                    
                    if ($total->type=='packadge') {
                    	if (($total->package_quantity*$product->quantity != $total->quantity) ||  ($total->package_quantity*$product->rabat != $total->rabat)) {
                    		return true;
                    	}
                    }
                    
                }
			}
		}

		return false;
		
	}
	public function moveToPriorityOrders(){

		$priorityArray=[];
		foreach ($this->order->details as $detail) {
			foreach ($detail->items as $item ) {

		
				$pharm = PharmacyViewModel::find($detail->pharm_id);
				
                $priorityArray[]=new OrderPriority([

					'reseller_id'=>$item->reseller_id,
					'order_id'=>$this->order->id,
					'created_at'=>$this->order->created_at,
					'manufacturer'=>'Bayer',
					'city'=>$pharm['city'],
					'client'=>$pharm['name'],
					'address'=>$pharm['address'],
					'bulstat'=>$pharm['bulstat'],
					'item_type'=>$item->item_type,
					'type'=>$this->order->type,
					'package_id'=>$item->package_id,	
					'item'=>$item->item_id,
					'quantity'=>$item->quantity,
					'rabat'=>$item->rabat,
					'comment'=>getComment($this->order->id,$item->reseller_id),
					'email'=>getEmail($item->reseller_id),
					'ready_for_send'=>$this->order->confirmed]


				);
			}
		}
		$this->order->priority()->SaveMany($priorityArray);
	}



	public function sendPriority(){

		$resellers = OrderPriority::ready()->groupBy(['reseller_id','email'])->get(['reseller_id','email']);
        foreach ($resellers as $reseller) {

            $orders = OrderPriority::ready()->forReseller($reseller->reseller_id)->get();

            
            $orders = $orders->groupBy('type')->map(function($type) {
                return $type->groupBy(function ($item, $key) {
                    return $item->item_type == 'package' ? 'package' : 'promotion';
                });
            });
            $groupedOrders = [];


            if ($orders->get('single')) {
                $groupedOrders['totalsPromoForSinglePharms'] = $orders->get('single')->get('promotion') ?: collect([]);
                $groupedOrders['totalsPackageForSinglePharms'] = $orders->get('single')->get('package') ?: collect([]);
            }


            if ($orders->get('chain')) {
               $groupedOrders['totalsPromoForChains'] = $orders->get('chain')->get('promotion') ?: collect([]);
                $groupedOrders['totalsPackageForChains'] = $orders->get('chain')->get('package') ?: collect([]);
            }
            

            $packages = OrderPriority::ready()->forReseller($reseller->reseller_id)
                            ->where('package_id','!=',0)->groupBy('package_id')->get(['package_id']);
           

            $packageIds = $packages->pluck('package_id');
            $groupedOrders['packagesArray'] = Promotion::whereIn('id', $packageIds)->with('products')->get();
            // dd($groupedOrders);
            $exportName = 'priority_'.uniqid();
            Excel::create($exportName, function($excel) use ($groupedOrders) {
                $excel->sheet('Daily Orders', function($sheet) use ($groupedOrders) {
                    $sheet->loadView('emails.orders', compact('groupedOrders'));
                });
            })->store('xlsx');
            
            $mailSent = $this->sendOrderMail(
                storage_path('exports/'.$exportName.'.xlsx'), 
                env('MAIL_FROM'), env('MAIL_FROM_NAME'), $reseller->email
            );

            if ($mailSent) {
                OrderPriority::ready()->forReseller($reseller->reseller_id)->delete();
            }
        }

		
	}

	public function sendOrderMail($pathToFile, $from, $fromName, $to)
    {
        Mail::send('emails.test', [], function($m) use ($pathToFile, $from, $fromName, $to) {
            $m->from($from, $fromName);
            $m->to($to)->subject('Test Mail');
            $m->attach($pathToFile);
        });

        return count(Mail::failures()) == 0;
    }

	public function moveToQueue(){

		$queueArray=[];
		foreach ($this->order->details as $detail) {
			foreach ($detail->items as $item ) {
				$pharm = PharmacyViewModel::find($detail->pharm_id);
				
                $queueArray[]=new OrderQueue([

					'reseller_id'=>$item->reseller_id,
					'order_id'=>$this->order->id,
					'created_at'=>$this->order->created_at,
					'manufacturer'=>'Bayer',
					'city'=>$pharm['city'],
					'client'=>$pharm['name'],
					'address'=>$pharm['address'],
					'bulstat'=>$pharm['bulstat'],
					'item_type'=>$item->item_type,
					'type'=>$this->order->type,
					'package_id'=>$item->package_id,	
					'item'=>$item->item_id,
					'quantity'=>$item->quantity,
					'rabat'=>$item->rabat,
					'comment'=>getComment($this->order->id,$item->reseller_id),
					'email'=>getEmail($item->reseller_id),
					'ready_for_send'=>$this->order->confirmed]


				);
			}
		}

		$this->order->queue()->SaveMany($queueArray);
		
	
	}

	public function moveToNotifyQueue(){


		$orderNotifyArray=[
			'order_id'=>$this->order->id,
		];

		OrderNotify::create($orderNotifyArray);
		
	
	}



	
}