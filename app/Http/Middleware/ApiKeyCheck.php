<?php

namespace App\Http\Middleware;

use Closure;

class ApiKeyCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
     public function handle($request, Closure $next)
    {

        if ($request->header('ApiKey')===md5(env(API_KEY))) {
            return $next($request);
        }
        return response()->json(['message'=>'Unauthorized'], 401);
    }
}
