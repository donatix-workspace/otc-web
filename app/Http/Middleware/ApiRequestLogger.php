<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use App\RequestLogger;

// use Illuminate\Database\Eloquent\ModelNotFoundException;

class ApiRequestLogger
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( ! $request->hasHeader('Auth')) {

            RequestLogger::create([
            'user_id' =>0,
            'action' => $request->route()->uri(),
            'method' => $request->method(),
            'raw' => json_encode($request->input()),
        ]);


            return $next($request);
        }
        
        $user = User::where('token', $request->header('Auth'))->first();
        if (is_null($user)) {
            return response()->json(['message'=>'Unauthorized'], 401);
        }

        RequestLogger::create([
            'user_id' =>$user->id,
            'action' => $request->route()->uri(),
            'method' => $request->method(),
            'raw' => json_encode($request->input()),
        ]);

        return $next($request);
    }


    // public function handle($request, Closure $next)
    // {
       
    //     $action=$request->route()->uri();
    //     $method=$request->method();
    //     $raw=((json_encode($request->input()))) ;
    //     $user_id=null;
    //     if (!empty($request->header('Auth'))) {
    //         try {
    //             $user_id=User::where('token', $request->header('Auth'))->first()->id;
    //         } catch (ModelNotFoundException $e) {
    //             RequestLogger::create([
    //                 'user_id' =>null,
    //                 'action' => $action,
    //                 'method' => $method,
    //                 'raw' => ($request->header())." Body: ".$raw,
    //             ]);
    //            return response()->json(['message'=>'Unauthorized'], 401);
    //         }
           
        
    //     RequestLogger::create([
    //         'user_id' =>$user_id,
    //         'action' => $action,
    //         'method' => $method,
    //         'raw' => $raw,
    //     ]);
    //     }
    //     return $next($request);
    // }
}
