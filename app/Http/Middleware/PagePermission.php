<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use App\User;

use Closure;

class PagePermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$role1='',$role2='',$role3='')
    {   
        $roles=[$role1,$role2,$role3];

        if (!$request->user()) {
            return redirect('no-permissions');
        }
        foreach ($roles as $role) {
            if ($request->user()->hasRole($role)) {
                return $next($request);
            }
        }
        
        return redirect('no-permissions');
        
    }
}
