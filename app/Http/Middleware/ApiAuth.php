<?php

namespace App\Http\Middleware;

use Closure;
use App\User;

class ApiAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = User::active()->where('token', $request->header('Auth'))->first();
        // $user = User::where('token', $request->header('Auth'))->first();
        
        if (is_null($user)) {
            return response()->json(['message' => 'Unauthorized'], 401);
        }

        $request->user = $user;
        
        return $next($request);
    }
}
