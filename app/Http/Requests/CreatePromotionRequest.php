<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreatePromotionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'start_at'=>'required',
            'end_at'=>'required',
            'products'=>'required|min:1',
            'products.*.quantity'=>'required|min:1',
            'products.*.rabat'=>'required|min:1',
        ];
    }
}
