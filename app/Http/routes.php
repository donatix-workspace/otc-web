<?php

/*
API endpoints
*/
Route::get('t','DummyController@order'); 
Route::get('no-permissions',function(){
    return view('nopermissions');
}


); 
Route::group(['prefix'=>'api/v1','as'=>'Api/v1::'],function() {      
    Route::post('auth','Api\AuthController@auth');
    Route::post('sync','Api\SyncController@index');
    Route::post('push','Api\PushController@push');
    Route::get('reports','Api\ReportControllerer@index'); 
    Route::post('report','Api\ReportControllerer@generate'); 

    Route::resource('orders','Api\OrderController');
    Route::resource('logs','Api\LogController');
});

/*
Web endpoints
*/
Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('/', 'OrderController@index');
    Route::get('orders/{id}/confirmation','OrderController@confirmation');
    Route::get('orders/{id}/decline','OrderController@decline');
    Route::resource('/resellers','ResellerController');
    Route::resource('/resellers/groups','ResellersGroupController');
    Route::resource('/promotions','PromotionController');
    Route::resource('/packages','PromotionController');
    Route::resource('/users','UserController');
    Route::resource('/orders','OrderController');
    Route::resource('/reports','ReportController');
    Route::post('reports/export','ReportController@export');

    Route::get('orders','OrderController@create');
    Route::post('orders','OrderController@store');
});

Route::get('/check-api-orders/{user}', function($user){
	

	
	$todayOrders = App\Order::with(['globalReseller', 'details.pharmacy', 'details.chain'])
                        ->madeBy($user)->fromToday()->get();
      
        $pendingOrders = App\Order::with(['details', 'details.pharmacy', 'details.chain'])
                        ->madeBy($user)->notConfirmed()->get();
        
        return response()->json([
                'today_orders' => (new App\Http\Transformers\OrderTransformer)->transformCollection($todayOrders->toArray()),
                'proccessing_orders' => (new App\Http\Transformers\PendingOrderTransformer)->transformCollection($pendingOrders->toArray()),
            ], 200
        );
	
});

Route::get('/check-api-pharms/{cwid}', function($cwid){
	
	
	$chains = App\ChainViewModel::with('pharmacies')->get();

        $ownedPharmacies = App\PharmacyViewModel::where('owner_id', $cwid)->get();
        $ownedPharmsIds=$ownedPharmacies->map(function($pharm){
            return $pharm->id;
        });

        $chainPharmacies = App\PharmacyViewModel::whereNotIn('id', $ownedPharmsIds)->where('chain_id', '!=', NULL)->get();
        $pharmacies=$ownedPharmacies->merge($chainPharmacies);
		
	return $pharmacies;
	
	
});