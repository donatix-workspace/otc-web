<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\CreatePromotionRequest;
use App\Http\Transformers\PackageGuiTransformer;
use App\Product;
use App\ProductViewModel;
use App\Promotion;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\User;


class PromotionController extends Controller
{
    public function index(Request $request)
    {
        if ($request->is('promotions')) {
            $promotions = Promotion::isPromo()->get();
        } else {
            $promotions = Promotion::isPackage()->get();
        }
        
        return view('promotions.index', compact('promotions'));
    }

    public function show($id)
    {
        try {
            $promotion = Promotion::with(['products.productView','users'])->findOrFail($id);
        }
        catch(ModelNotFoundException $e) {
            return view('errors.4000')->with('errCode', 4001);
        }
        $promo = (new PackageGuiTransformer)->transform($promotion->toArray());

        return view('promotions.show',compact('promo'));
    }

    public function create()
    {
        $products = ProductViewModel::where('status',1)->get();
        $users=User::all();

        return view('promotions.create', compact('products','users'));
    }

    public function store(CreatePromotionRequest $request)
    {   


        $isPackage = count($request->products) > 1;
        $users=$request->users;
        $isPrivate=isset($request->is_private);

        $start_at=Carbon::createFromFormat('Y-m-d', $request->start_at)->startOfDay()->toDateTimeString();
        $end_at=Carbon::createFromFormat('Y-m-d', $request->end_at)->endOfDay()->toDateTimeString();

        $promotion = Promotion::create([
            'name' => $request->name,
            'is_package'=>$isPackage,
            'start_at' => $start_at,
            'end_at' => $end_at,
            'is_private' =>  $isPrivate,
        ]);

        $promotion->saveProducts($request->products);
        if ($isPrivate) {
            if (isset($users)) {
                foreach ($users as $userId) {
                    $promotion->users()->save(User::findOrFail($userId));
                }
            }
        }
        
        return redirect()->to($isPackage ? 'packages' : 'promotions');  
    }
    public function edit($id){

        try {
            $promotion = Promotion::with(['products.productView','users'])->findOrFail($id);
        }
        catch(ModelNotFoundException $e) {
            return view('errors.4000')->with('errCode', 4001);
        }

        $promo = (new PackageGuiTransformer)->transform($promotion->toArray());
        $users=User::all();
        $assignedUsers=$promotion->users->map(function($user){
            return $user->id;
        });

        return view('promotions.edit',compact('promo','users','assignedUsers'));
    }
    public function update($id,Request $request ){

        $promo=Promotion::findOrFail($id);
        
        $end_at=Carbon::createFromFormat('Y-m-d', $request->end_at)->endOfDay()->toDateTimeString();
        
        $promo->update([
            'name' => $request->name,
            'end_at' => $end_at,
            'is_private'=>isset($request->is_private),
        ]);
        $promo->users()->detach();

        if (isset($request->users)) {
            foreach ($request->users as $userId) {
                $promo->users()->save(User::findOrFail($userId));
            }
        }
        return back();
    }
    public function destroy(Promotion $promotion, $id)
    {
        $promotion->find($id)->delete();

        return back();
    }
}
