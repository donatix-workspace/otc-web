<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Push;

class PushController extends Controller
{
    public function __construct()
    {
       
        $this->middleware('api.auth', ['on' => 'push']);
        if (env('API_LOG', false)) {
            $this->middleware('api.logger', ['on' => 'push']);
        }
         
    }
    protected function push(Request $request)
    {	
    	$pushArray=[
    		'user_id'=>$request->user->id,
    		'version'=>$request->version,
    		'device'=>$request->device,
    		'platform'=>$request->platform
    	];
    	if(Push::create($pushArray)){
    		return response()->json(['message'=>'Success'], 200);
    	}
    	return response()->json(['message'=>'Unauthorized'], 401);
        
            
        
    }
}
