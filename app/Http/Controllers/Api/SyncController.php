<?php

namespace App\Http\Controllers\Api;

use App\ChainViewModel;
use App\City;
use App\Http\Controllers\Controller;
use App\Http\Responses\CitiesResponse;
use App\Http\Transformers\ChainTransformer;
use App\Http\Transformers\PackageTransformer;
use App\Http\Transformers\PharmacyTransformer;
use App\Http\Transformers\ProductTransformer;
use App\Http\Transformers\PromotionTransformer;
use App\Http\Transformers\ResellerTransformer;
use App\PharmacyViewModel;
use App\ProductViewModel;
use App\Promotion;
use App\Reseller;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Log;

class SyncController extends Controller
{   
    private $defaultDate = '2016-01-01 12:00:00';

    public function __construct()
    {
        date_default_timezone_set('Europe/Sofia');
        $this->middleware('api.auth', ['on' => 'push']);
        if (env('API_LOG', false)) {
            $this->middleware('api.logger', ['on' => 'push']);
        }
    }

    public function index(Request $request)
    {   

        $date = $this->getDate($request->get('since'));
        
        $products = ProductViewModel::all();
       // $cities = City::all();
        $resellers = Reseller::whereHas('users', function($query) use ($request) {
            return $query->where('id', $request->user->id);
        })->get();
        $chains = ChainViewModel::with('pharmacies')->get();

        $ownedPharmacies = PharmacyViewModel::where('owner_id', $request->user->cwid)->get();
        $ownedPharmsIds=$ownedPharmacies->map(function($pharm){
            return $pharm->id;
        });

        $chainPharmacies = PharmacyViewModel::whereNotIn('id', $ownedPharmsIds)->where('chain_id', '!=', NULL)->get();
        $pharmacies=$ownedPharmacies->merge($chainPharmacies);

        $promotions = Promotion::withTrashed()->where('is_private',NULL)->orWhere('is_private', 0)->with('products.productView')->isPromo()->get();
        $privatePromotions = Promotion::withTrashed()
            ->where('is_private',1)
            ->with(['products.productView','users'])
            ->isPromo()
            ->whereHas('users',function($query)use ($request){
                $query->where('user_id',$request->user->id);
            })
            ->get();

        $promotions=$promotions->merge($privatePromotions);

        $packages = Promotion::withTrashed()->where('is_private',NULL)->orWhere('is_private', 0)->with('products.productView')->isPackage()->get();
        $privatePackages = Promotion::withTrashed()
            ->where('is_private',1)
            ->with(['products.productView','users'])
            ->isPackage()
            ->whereHas('users',function($query)use ($request){
                $query->where('user_id',$request->user->id);
            })
            ->get();
        $packages=$packages->merge($privatePackages);

        $response = [
            'cities' => [],//$cities,
            'chains' => (new ChainTransformer)->transformCollection($chains->toArray()),
            'packages' => (new PackageTransformer)->transformCollection($packages->toArray()),
            'products' => (new ProductTransformer)->transformCollection($products->toArray()),
            'sellers' => (new ResellerTransformer)->transformCollection($resellers->toArray()),
            'pharmacies' => (new PharmacyTransformer)->transformCollection(
                $this->transformPharmacies($pharmacies, $request->user->id,$request->user->cwid)
            ),
            'promotions' => (new PromotionTransformer)->transformCollection(
                $this->transformPromotions($promotions)        
            ),
        ];
        // return $response['promotions'];
        return response()->json($response, 200);
    }

    private function getDate($date)
    {
        if (is_null($date)) {
            return $this->defaultDate;
        }
        $date=Carbon::createFromFormat('Y-m-d H:i:s +P', $date)
                ->timezone('Europe/Sofia')->toDateTimeString();

        // Log::debug($date);
        return $date;
    }

    private function transformPromotions($promotions)
    {
        return $promotions->map(function($promo) {
            return $promo->setQuantity()->setRabat();
        })->toArray();
    }

    private function transformPharmacies($pharmacies, $userId,$cwid)
    {
        return $pharmacies->map(function($pharmacy) use ($userId,$cwid) {
            return $pharmacy->setUserId($userId)->setCwidId($cwid);
        })->toArray();
    }
}
