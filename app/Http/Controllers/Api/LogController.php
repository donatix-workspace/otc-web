<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Log;

class LogController extends Controller
{
    public function store(Request $request)
    {	
       Log::create([
    		'endpoint'=>$request->endpoint,
    		'code'=>$request->code,
    		'request'=>$request->http_request,
    		'response'=>$request->http_response,
    	]);
        return response()->json(['message'=>'Success'], 200);
    }
}
