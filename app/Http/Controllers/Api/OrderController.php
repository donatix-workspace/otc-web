<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Order;
use Log;
use File;
use App\Http\Handlers\OrderHandler;
use App\Http\Controllers\Controller;
use App\Http\Transformers\OrderTransformer;
use App\Http\Transformers\PendingOrderTransformer;
use App\Http\Transformers\SingleOrderTransformer;

class OrderController extends Controller
{
    public function __construct()
    {
        date_default_timezone_set('Europe/Sofia');
        $this->middleware('api.auth');
        if (env('API_LOG', false)) {
            $this->middleware('api.logger');
        }
    }

    public function index(Request $request)
    {
        $todayOrders = Order::with(['globalReseller', 'details.pharmacy', 'details.chain'])
                        ->madeBy($request->user->id)->fromToday()->get();
        
        $pendingOrders = Order::with(['details', 'details.pharmacy', 'details.chain'])
                        ->madeBy($request->user->id)->notConfirmed()->get();
        
        return response()->json([
                'today_orders' => (new OrderTransformer)->transformCollection($todayOrders->toArray()),
                'proccessing_orders' => (new PendingOrderTransformer)->transformCollection($pendingOrders->toArray()),
            ], 200
        );
    }

    public function store(Request $request)
    {
        //TODO path change delete file
        if ($request->hasFile('orders')) {
            $jsonFile=$request->file('orders')->move(public_path(env('FILE_UPLOAD_DIRECTORY', 'uploaded/images')));
            $file=File::get($jsonFile);
            Log::debug($file);
            $jsonObjectArray=json_decode($file);
        }
        else{
            Log::debug($request->orders);
           $jsonObjectArray=json_decode(stripcslashes ($request->orders));
            
        }
        if (!$jsonObjectArray) {
                return response()->json(['message'=>'Error'], 500);
        }

        foreach ($jsonObjectArray as $jsonObject) {
            try {
                $orderHandler = new OrderHandler($jsonObject);
                $orderHandler->createOrder();
                $orderHandler->createOrderDetails();
                $orderHandler->createOrderTotals();
                $orderHandler->updateConfirmation();
                $orderHandler->createOrderResellers();
                if ($orderHandler->is_priority && !$orderHandler->need_confirmation) {
                    $orderHandler->moveToPriorityOrders();
                }
                else{
                    $orderHandler->moveToQueue();
                    if ($orderHandler->need_confirmation) {
                       $orderHandler->moveToNotifyQueue();
                    }
                    
                }
            } catch (Exception $e) {
                Log::debug($e);
                $orderHandler->deleteOrder();
            }
            
            
        }

        return response()->json(['message'=>'Success'], 200);
    }

    public function show($id, Request $request)
    {
        $order = Order::with([
            'details.items', 'details.pharmacy', 'details.chain', 'resellers.reseller',
            'totals.promotion.products', 'totals.package', 'totals.productView'
        ])->where('id', $id)->madeBy($request->user->id)->first();
        
        if ( ! $order) return response()->json([], 404);

        return response()->json(
            (new SingleOrderTransformer)->transform($order->setVenueName()), 200
        );
    }
}
