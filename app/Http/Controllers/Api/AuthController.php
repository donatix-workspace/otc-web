<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Auth;


use App\Http\Transformers\AuthTransformer;

class AuthController extends Controller
{
    public function __construct()
    {
       
        if (env('API_LOG', false)) {
            $this->middleware('api.logger', ['on' => 'push']);
        }
         
    }
    protected function auth(Request $request)
    {

        $credentials=$request->only('email','password');
        
        if (Auth::attempt($credentials))
        {
            $user= Auth::user();
            //Update toke after every login
            $user->token=uniqid('ios');
            $user->save();
   
            $userResponse=new AuthTransformer();
            $response=$userResponse->transform($user->toArray());
                return $response;
            }
            
        return response()->json(['message'=>'Unauthorized'], 401);
    }
}
