<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\ReportRequest;
use App\Order;
use App\OrderDetailItem;
use App\OrderFilters;
use App\OrderHistory;
use App\OrderHistoryFilters;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Log;
use Maatwebsite\Excel\Facades\Excel;

class ReportControllerer extends Controller
{
    public function __construct()
    {
    	date_default_timezone_set('Europe/Sofia');
        $this->middleware('api.auth');
        if (env('API_LOG', false)) {
            $this->middleware('api.logger');
        }
    }

    public function index(OrderHistoryFilters $filters, Request $request)
    {
        Log::debug($request);
        
        $view='reports.tables.api.bytypepharm';

        $viewMap=[
            'seller'=>'reports.tables.api.byreseller',
            'city'=>'reports.tables.api.bycity',
            'state'=>'reports.tables.api.bystate',
            'chain'=>'reports.tables.api.bytypechain',
            'pharm'=>'reports.tables.api.bytypepharm',
            'promo'=>'reports.tables.api.bypromo',
            'user'=>'reports.tables.api.byuser',
        ];

        if(isset($request->sort_by) && !empty($viewMap[$request->sort_by])){
          $view=$viewMap[$request->sort_by];
        }

        $chunks = OrderHistory::query()
            ->madeBy($request->user->id)
            ->filter($filters)
            ->get()
            ->chunk(8);

        Log::debug($chunks->toArray());
        $path='/pdf/reports/'.uniqid().'.pdf';
        $fullpath=public_path().$path;
        Log::debug($fullpath);
        $pdf = \PDF::loadView('reports.api.index',compact('chunks','view'));
        Log::debug('pdf');
        $pdf->save($fullpath);

        return response()->json(['url'=>url($path)], 200);
    }
}
