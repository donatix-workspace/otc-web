<?php

namespace App\Http\Controllers;

use App\Http\Handlers\OrderHandler;
use App\Order;
use App\OrderDetail;
use App\OrderHistoryView;
use App\OrderQueue;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Log;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        if (Auth::guest()) {
            return redirect('no-permissions');
        }
        if (isRepresentative(Auth::user())) {

            $userId = Auth::user()->id;

            $ordersForConfirm = OrderHistoryView::where('user_id', $userId)->notConfirmed()->orderBy('id', 'desc')->get();
            $orders = OrderHistoryView::where('user_id', $userId)->confirmed()->orderBy('id', 'desc')->take(500)->get();
            return view('orders.index', compact('ordersForConfirm', 'orders'));

        }
        $ordersForConfirm = OrderHistoryView::notConfirmed()->orderBy('id', 'desc')->get();
        $orders = OrderHistoryView::confirmed()->orderBy('id', 'desc')->take(500)->get();
        return view('orders.index', compact('ordersForConfirm', 'orders'));
    }

    public function show($id)
    {
        $order = Order::with('details.chain')->findOrFail($id);
        $user = User::findOrFail($order->user_id);

        return view('orders.show', compact('order', 'user'));
    }

    public function create()
    {
        return view('orders.create');
    }

    public function store(Request $request)
    {
        // return $request->orders;

        // Log::debug($request->orders);
        $jsonObjectArray = json_decode(stripcslashes($request->orders));

        if (!$jsonObjectArray) {
            return response()->json(['message' => 'Error'], 500);
        }
        foreach ($jsonObjectArray as $jsonObject) {
            try {
                $orderHandler = new OrderHandler($jsonObject);
                $orderHandler->createOrder();
                $orderHandler->createOrderDetails();
                $orderHandler->createOrderTotals();
                $orderHandler->updateConfirmation();
                $orderHandler->createOrderResellers();
                if ($orderHandler->is_priority && !$orderHandler->need_confirmation) {
                    $orderHandler->moveToPriorityOrders();
                } else {
                    $orderHandler->moveToQueue();
                    if ($orderHandler->need_confirmation) {
                        $orderHandler->moveToNotifyQueue();
                    }

                }
            } catch (Exception $e) {
                Log::debug($e);
                $orderHandler->deleteOrder();
            }


        }

        return response()->json(['message' => 'Success'], 200);
    }

    public function getOrderDetails($id)
    {
        $orderDetails = OrderDetail::where('order_id', '=', $id)->get(['id', 'pharm_id'])->toArray();
        return $orderDetails;
    }

    public function confirmation($id, Request $request)
    {
        $order = Order::findOrFail($id);
        $order->confirmed = 1;
        $order->save();
        $orderQueue = OrderQueue::where('order_id', $id)->update(['ready_for_send' => 1]);

        return redirect()->action('OrderController@index');
    }

    public function decline($id, Request $request)
    {
        $order = Order::findOrFail($id);
        $order->need_confirmation = 0;
        $order->save();
        $orderQueue = OrderQueue::where('order_id', $id)->delete();

        return redirect()->action('OrderController@index');
    }

    public function destroy(Request $order, $id)
    {
        Order::find($id)->delete();

        return redirect()->action('OrderController@index');
    }
}
