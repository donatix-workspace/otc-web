<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Order;
use App\OrderFilters;
use App\OrderHistory;
use App\OrderHistoryFilters;
use App\User;
use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;


use App\OrderDetailItem;
use App\OrderDetail;


use Excel;

class ReportController extends Controller
{

    public function __construct()
    {
        //$this->middleware('page.permission:super_admin');   
    }
    
    public function index(OrderHistoryFilters $filters, Request $request)
    {   
        if (Auth::guest()) {
           return redirect('no-permissions');
        }

        $view='reports.tables.bytypepharm';
        
        $viewMap=[
            'seller'=>'reports.tables.byreseller',
            'city'=>'reports.tables.bycity',
            'state'=>'reports.tables.bystate',
            'chain'=>'reports.tables.bytypechain',
            'pharm'=>'reports.tables.bytypepharm',
            'promo'=>'reports.tables.bypromo',
            'user'=>'reports.tables.byuser',
        ];

        if(isset($request->sortBy) && !empty($viewMap[$request->sortBy])){
          $view=$viewMap[$request->sortBy];
        }
        if (isRepresentative(Auth::user())) {
            
            $userId=Auth::user()->id;

            $orders=OrderHistory::query()
                ->notEmpty()
                ->madeBy($userId)
                ->filter($filters)
                ->get();

            return view('reports.index', compact('orders','view'));
        }

        $orders=OrderHistory::query()
            ->notEmpty()
            ->filter($filters)
            ->get();

        return view('reports.index', compact('orders','view'));
        
    }

    public function export(Request $request)
    {
        $orders=json_decode($request->orders,1);

        $view=str_replace('tables', 'exports', $request->view);
        // return view('reports.export', compact('view','orders'));
        $exportName = Carbon::now();
        $orders = collect($orders)->map(function ($el) {
            return (object) $el;
        });
        Excel::create($exportName, function($excel) use ($view,$orders) {
            $excel->sheet('Export', function($sheet) use ($view,$orders) {
                $sheet->setfitToWidth(true);
                $sheet->loadView('reports.export', compact('view','orders'));
            });
        })->download('xlsx');
    	
    }
}
