<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateUserRequest;
use App\User;
use App\Role;

class UserController extends Controller
{
    public function index() {
    	$users = User::all();
    	return view('users.index',compact('users'));
    }

    public function create() {
    	$roles = Role::all();
    	return view('users.create',compact('roles'));
    }

    public function store(CreateUserRequest $request) {
    	$user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'cwid' => $request->cwid,
            'token' => uniqid('ios'),
            'password' => $request->password,
        ]);
        $user->assignRole($request->role_name);
        
        return redirect()->to('users');	
    }

    public function edit(User $user) 
    {
        $user->load('roles');
        $roles = Role::all();

        return view('users.show', compact('user', 'roles'));
    }

    public function update(User $user, Request $request)
    {
        $this->validate($request, ['password' => 'min:4|confirmed']);

        $values = array_filter($request->input());
        if (!$request->is_active) {
            $values['is_active']=0;
        }

        $user->update($values);
        $user->assignRole($values['role_name']);

        return redirect('users');
    }
}
