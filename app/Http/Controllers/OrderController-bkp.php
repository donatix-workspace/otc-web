<?php

namespace App\Http\Controllers;

use App\OrderHistory;
use Illuminate\Http\Request;

use App\User;
use Auth;
use App\Order;
use App\OrderQueue;
use App\OrderDetail;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        if (Auth::guest()) {
           return redirect('no-permissions');
        }
        if (isRepresentative(Auth::user())) {
            
            $userId=Auth::user()->id;

            $ordersForConfirm = OrderHistory::query()->where('user_id',$userId)->groupBy('order_id')->notConfirmed()->get();
            $orders = OrderHistory::query()->where('user_id',$userId)->groupBy('order_id')->orderBy('order_id', 'desc')->get();

//            $orders=Order::with(['user','details' => function($query) { $query->with(['pharmacy','chain']); }])->where('user_id',$userId)->orderBy('id', 'desc')->get();
//            $ordersForConfirm=Order::with(['user','details' => function($query) { $query->with(['pharmacy','chain']); }])->where('user_id',$userId)->notConfirmed()->get();$orders=Order::with(['user','details' => function($query) { $query->with(['pharmacy','chain']); }])->where('user_id',$userId)->orderBy('id', 'desc')->get();
            return view('orders.index',compact('ordersForConfirm','orders'));

        }
//        $ordersForConfirm=Order::with(['user','details' => function($query) { $query->with(['pharmacy','chain']); }])->notConfirmed()->get();
//        $orders=Order::with(['user','details' => function($query) { $query->with(['pharmacy','chain']); }])->orderBy('id', 'desc')->get();

        $ordersForConfirm = OrderHistory::query()->select(['order_id','chain_id','chain_name','pharmacy_id','pharmacy_name','user_name','created_at','order_date','need_confirmation','confirmed'])->groupBy(['order_id','chain_id','chain_name','pharmacy_id','pharmacy_name','user_name','created_at','order_date','need_confirmation','confirmed'])->orderBy('order_id','chain_id','chain_name','pharmacy_id','pharmacy_name','user_name','created_at','order_date','need_confirmation','confirmed')->get();
        $orders = OrderHistory::query()->select(['order_id','chain_id','chain_name','pharmacy_id','pharmacy_name','user_name','created_at','order_date','need_confirmation','confirmed'])->groupBy(['order_id','chain_id','chain_name','pharmacy_id','pharmacy_name','user_name','created_at','order_date','need_confirmation','confirmed'])->orderBy('order_id','chain_id','chain_name','pharmacy_id','pharmacy_name','user_name','created_at','order_date','need_confirmation','confirmed')->get();
        return view('orders.index',compact('ordersForConfirm','orders'));
    }

    public function show($id)
    {
        $order=Order::with('details.chain')->findOrFail($id);

        $user= User::findOrFail($order->user_id);

        return view('orders.show',compact('order','user'));
    }

    public function getOrderDetails($id)
    {
        $orderDetails=OrderDetail::where('order_id','=',$id)->get(['id','pharm_id'])->toArray();
        return $orderDetails;
    }

    public function confirmation($id,Request $request)
    {
        $order=Order::findOrFail($id);
        $order->confirmed=1;
        $order->save();
        $orderQueue=OrderQueue::where('order_id',$id)->update(['ready_for_send' => 1]);

        return redirect()->action('OrderController@index');
    }

    public function decline($id,Request $request)
    {
        $order=Order::findOrFail($id);
        $order->need_confirmation=0;
        $order->save();
        $orderQueue=OrderQueue::where('order_id',$id)->delete();

        return redirect()->action('OrderController@index');
    }
     public function destroy(Request $order, $id)
    {
        Order::find($id)->delete();

        return redirect()->action('OrderController@index');
    }
}
