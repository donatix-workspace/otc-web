<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Mail;
use Excel;
use Log;
use App\Promotion;
use App\OrderQueue;

class DummyController extends Controller
{

    public function order()
    {
        $resellers = OrderQueue::ready()->groupBy(['reseller_id','email'])->get(['reseller_id','email']);
           
        foreach ($resellers as $reseller) {

            $orders = OrderQueue::with('order.user')->ready()->forReseller($reseller->reseller_id)->get();

            
            $orders = $orders->groupBy('type')->map(function($type) {
                return $type->groupBy(function ($item, $key) {
                    return $item->item_type == 'package' ? 'package' : 'promotion';
                });
            });
            $groupedOrders = [];


            if ($orders->get('single')) {
                $groupedOrders['totalsPromoForSinglePharms'] = $orders->get('single')->get('promotion') ?: collect([]);
                $groupedOrders['totalsPackageForSinglePharms'] = $orders->get('single')->get('package') ?: collect([]);
            }


            if ($orders->get('chain')) {
               $groupedOrders['totalsPromoForChains'] = $orders->get('chain')->get('promotion') ?: collect([]);
                $groupedOrders['totalsPackageForChains'] = $orders->get('chain')->get('package') ?: collect([]);
            }
            

            $packages = OrderQueue::distinct()->ready()->forReseller($reseller->reseller_id)
                            ->where('package_id','!=',0)->groupBy('package_id')->get(['package_id']);
           
            $packageIds = $packages->pluck('package_id');
            if ($packageIds->count()) {
                $groupedOrders['packagesArray'] = Promotion::whereIn('id', $packageIds)->with('products')->get();
            }
            //return $groupedOrders;
            return view('emails.orders', compact('groupedOrders'));
            //dd($groupOrders);
            /*$exportName = uniqid();
            Excel::create($exportName, function($excel) use ($groupedOrders) {
                $excel->sheet('Daily Orders', function($sheet) use ($groupedOrders) {
                    $sheet->loadView('emails.orders', compact('groupedOrders'));
                });
            })->store('xlsx');*/
            //Log::debug($this->send($groupedOrders, $from, $fromName, $to));
            // if ($this->send($groupedOrders, env('MAIL_FROM'), env('MAIL_FROM_NAME'), $reseller->email)) {
            //     OrderQueue::ready()->forReseller($reseller->reseller_id)->delete();
            // }
            /*$mailSent = $this->sendOrderMail(
                storage_path('exports\\'.$exportName.'.xlsx'), 
                env('MAIL_FROM'), env('MAIL_FROM_NAME'), $reseller->email
            );

            if ($mailSent) {
                OrderQueue::ready()->forReseller($reseller->reseller_id)->delete();
            }*/
        }
    }
}
