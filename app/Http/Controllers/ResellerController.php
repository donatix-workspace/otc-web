<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\CreateResellerRequest;
use App\Http\Controllers\Controller;
use App\User;
use App\Reseller;
use App\ResellerGroup;


use Illuminate\Database\Eloquent\ModelNotFoundException;

class ResellerController extends Controller
{
    protected $_imagePath;
    public function __construct()
    {
        $this->_imagePath=env('FILE_UPLOAD_DIRECTORY', 'uploaded/images');
    }
    public function index(){
        $resellers=Reseller::with('group')->get();
        return view('resellers.index',compact('resellers'));
    }
    public function create(){
        $groups=ResellerGroup::all();
        $users=User::all();
        return view('resellers.create',compact('groups','users'));
    }
    public function store(CreateResellerRequest $request){
        
        $filename='';//public_path();
        if($request->hasFile('image')){
            $file = $request->file('image');
            if($file->isValid()){
                $filename.='reseller_'.time().'.'.$file->guessClientExtension();
                $file->move(public_path($this->_imagePath),$filename);
            }   
        }   
        else {
            $filename.='reseller_default.jpeg';
        }
        $reseller=Reseller::create([
            'name' => $request->name,
            'group_id' => $request->group_id,
            'email' => $request->email,
            'image' => $filename,
            'description' => $request->description,
        ]);
        if (isset($request->users)) {
            foreach ($request->users as $userId) {
                $reseller->users()->save(User::findOrFail($userId));
            }
        }
        
        
        return redirect()->to('resellers'); 
    }
    public function edit($id){

        try
        {
            $reseller=Reseller::with('users')->findOrFail($id);
        }
        catch(ModelNotFoundException $e)
        {
            $errCode=4002;
            return view('errors.4000',compact('errCode'));
        }
        
        $groups=ResellerGroup::all();
        $users=User::all();

        $assignedUsers=$reseller->users->map(function($user){
            return $user->id;
        });
        return view('resellers.edit',compact('reseller','groups','users','assignedUsers'));
    }
    public function update($id,Request $request ){
        $filename=null;//public_path();
        if($request->hasFile('image')){
            $file = $request->file('image');
            if($file->isValid()){
                $filename='reseller_'.time().'.'.$file->guessClientExtension();
                $file->move(public_path($this->_imagePath),$filename);
            }   
        }
        $reseller=Reseller::findOrFail($id);
        
        $filename=isset($filename)?$filename:$reseller->image;

        $reseller->update([
            'name' => $request->name,
            'group_id' => $request->group_id,
            'email' => $request->email,
            'image' =>$filename ,
            'description' => $request->description,
        ]);
        if (isset($request->users)) {
            $reseller->users()->detach();
            foreach ($request->users as $userId) {
                $reseller->users()->save(User::findOrFail($userId));
            }
        }
        return redirect()->to('resellers'); 
    }

    public function destroy($id,Request $request ){

        
        $reseller=Reseller::findOrFail($id);

        $reseller->delete();
        return redirect()->to('resellers'); 
    }
}
