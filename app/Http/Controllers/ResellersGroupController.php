<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateResellersGroupRequest;
use App\ResellerGroup;

class ResellersGroupController extends Controller
{
    public function create(){
        $groups=ResellerGroup::all();
    	return view('resellers.groups.create',compact('groups'));
    }
    public function store(CreateResellersGroupRequest $request){
    	
	
    	ResellerGroup::create([
            'name' => $request->name,
        ]);
    	
    	return redirect()->to('resellers');	
    }
}
