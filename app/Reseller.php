<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reseller extends Model
{
	use SoftDeletes;

    protected $fillable = [
        'name','group_id' ,'email','image','description'
    ];
    

    protected $dates = ['deleted_at'];

    public function scopeAfter($query,$lastUpdate)
    {
        return $query->where('updated_at', '>', $lastUpdate);
    }
    public function users()
    {
        return $this->belongsToMany('App\User');

    }
    public function group()
    {
        return $this->belongsTo('App\ResellerGroup');

    }
    /*public function response()
    {
    	$is_deleted = false;

    	if ($this->deleted_at) {
    		$is_deleted = true;
    	}
        return [
        	'id' => $this->id,
			'name' => $this->name,
			'photo' => $this->image,
			'deleted' => $is_deleted
        ];
    }
*/
}
