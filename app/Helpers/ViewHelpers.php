<?php 

use App\OrderQueue;
use App\OrderView;
use App\OrderReseller;
use App\PharmacyViewModel;
use App\ProductViewModel;
use App\Promotion;
use App\Reseller;
use App\User;
use Carbon\Carbon;


function getPromoTypeName($type)
{
	if ($type=='promotion') {
		return trans('reports.product-type-promo');
	}elseif ($type=='packadge') {
		return trans('reports.product-type-package');
	}
	return trans('reports.product-type-single');
}
function getState($state){

	if ($state) {
		
		return trans('reports.state-confirmed'); 
	}
	return trans('reports.state-not-confirmed'); 
	
}


function productName($type,$id)
{
	if ($type=='promotion') {
		return Promotion::withTrashed()->findOrFail($id)->name;
	}
	
	return ProductViewModel::findOrFail($id)->name;
}

function promoName($promoId)
{
	return Promotion::findOrFail($promoId)->name;
}

function packageName($packageId)
{
	if ($packageId == 0) return '';

    return Promotion::findOrFail($packageId)->name;
}

function pharmName($pharmId)
{
    $pharm = PharmacyViewModel::find($pharmId);
	return $pharm?$pharm->name:'';
}

function getPharm($pharmId)
{
    return PharmacyViewModel::findOrFail($pharmId);
}

function resellerName($resellerId)
{
	return Reseller::findOrFail($resellerId)->name;
}

function resellerEmail($resellerId)
{
	return Reseller::findOrFail($resellerId)->email;
}

function username($id)
{
	return User::findOrFail($id)->name;
}

function getEmail($reseller_id)
{	
	$reseller=Reseller::where('id',$reseller_id)->first();
	return $reseller->email;
}
function getComment($order_id,$reseller_id)
{	
	$reseller=OrderReseller::where('order_id',$order_id)->where('reseller_id',$reseller_id)->first();
	return $reseller->comment;
}
function formatDate($date)
{	


	$date=new Carbon($date);
	return $date->format('Y-m-d');
	
}
function formatDateReverse($date)
{	

	$date=new Carbon($date);
	return $date->format('Y-m-d H:i:s').config('app.timezonepostfix');

}
function getOrderState($order)
{	

	if (!$order->need_confirmation && !$order->confirmed) {
        
		return 'fa fa-times';

	}
	if ($order->confirmed) {
		return 'fa fa-check';
	}
	return 'fa fa-refresh';
	
}
function getStateByOrder($order)
{	
	if (!$order->need_confirmation && !$order->confirmed) {
        
		return 'declined';

	}
	if ($order->confirmed) {
		return 'confirmed';
	}
	return 'waiting';
	
}
function getStateByOrderArray($order)
{	
	if (!$order['need_confirmation'] && !$order['confirmed']) {
        
		return 'declined';

	}
	if ($order['confirmed']) {
		return 'confirmed';
	}
	return 'waiting';
	
}
function isRepresentative($user)
{	
	return $user->hasRole('sales_representative ');
	
}
function isPromoActive($startDate,$endDate){

	$now=formatDateReverse(Carbon::now());

    return $startDate<=$now && $now<=$endDate;

}
function sendDate($sendDateInput){

	if (isset($sendDateInput)) {

		$date=new Carbon($sendDateInput);

		return $date->format('Y-m-d H:i:s').config('app.timezonepostfix');
	}
	if (Carbon::now()->hour >= env("CRON_TIME_HOUR")) {
		return Carbon::tomorrow()->format('Y-m-d H:i:s').config('app.timezonepostfix') ;
	}
	return Carbon::now()->format('Y-m-d H:i:s').config('app.timezonepostfix');

}
function isPromoPrivate($is_private){
	return $is_private?trans('promotions.private'):trans('promotions.public');
}
function getDateBeforeDays($days){
    return Carbon::now()->subDays($days)->format('Y-m-d');
}