<?php

namespace App;

use App\Reseller;
use DB;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'user_id','type','is_priority' ,'need_confirmation','confirmed','global_reseller_id','signiture','send_date'
    ];

    public function scopeMadeBy($query,$user_id)
    {
        return $query->where('user_id', '=', $user_id);
    }
    public function details()
    {
        return $this->hasMany('App\OrderDetail');
    }
    public function totals()
    {
        return $this->hasMany('App\OrderTotal');
    }
    public function resellers()
    {
        return $this->hasMany('App\OrderReseller');
    }
    public function queue()
    {
        return $this->hasMany('App\OrderQueue');
    }
    public function priority()
    {
        return $this->hasMany('App\OrderPriority');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function globalReseller()
    {
        return $this->belongsTo(Reseller::class, 'global_reseller_id', 'id');
    }

    public function scopeNotConfirmed($query)
    {
        return $query->where('need_confirmation', 1)->where('confirmed', 0);
    }
    public function scopeConfirmed($query)
    {
        return $query->where('confirmed', 1);
    }
    public function scopeFromToday($query)
    {
        //SELECT    FROM [BayerOTCOrder].[dbo].[orders]
        if (env('ENV')=='dev') {
            return $query->where(DB::raw('DAY(created_at)'),'=', date('d'));
        }
        
        
        return $query->where(DB::raw("replace(convert(NVARCHAR, created_at, 105), ' ', '-')"),'=', date('d-m-Y'));
    }
    public function scopeChains($query)
    {
        return $query->where('type','chain');
    }
    public function scopePharms($query)
    {
        return $query->where('type','single');
    }

    public function scopeFilter($query, $filters)
    {
        return $filters->apply($query);
    }

    public function setVenueName()
    {
        $this->venue_name = $this->getPharmName($this->details);

        return $this;
    }

    public function getPharmName($details)
    {
       if (is_null($details)) {
            return '';
        } 

        if (count($details) > 1) {
            return $details[0]['chain']['name'];
        }

        return $details[0]['pharmacy']['name'];
    }
}
