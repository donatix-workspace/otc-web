<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Push extends Model
{
	protected $table = 'push';
     protected $fillable = [
        'user_id','version', 'device', 'platform',
    ];
}
